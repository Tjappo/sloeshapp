# Todo

- Implement car type delete
- Implement advertisement delete

# Comments

Form
 - Layout van de knoppen, wanneer niet aan validatie wordt voldaan (disabled)?
    - toon campagnes bijv?
 - Validaties
   - van kenteken platen en postcodes (format)
   - Porten van form validaties

Pagina's
 - Campagne aanbieden
 - Bestikkeraars gezocht
 - Privacy
 - Disclaimer
 - Cookie overlay
 - Update user details view
 - Update user cars view 
   - Navigatie knop naar user cars pagina?
   - Confirmation status van een kenteken?
 
Hoofdpagina
  - Wanneer er maar 1 nieuwste campagne is, position?
  - Position van wolken op verschillende schermen (responsive)
    - iphone 5 vs iphone x bijv

Advertisements
  - Niet ingelogd verschil in view?
  - Verschil in filters?
    - Geschikt voor mijn auto's -> false
      - Aanmelden knop weghalen?
  - Filters
    - Stel dat de minimum compensation en maximum hetzelfde is, slider dan weghalen?
  - Koppelen van postcode en kenteken aan te laten zien advertenties?
  - Bijzonderheden toevoegen aan model?
  - Auto types
  - Koppeling met advertentie badges (100% elektrisch)?

Advertisement Detail page
  - Geen tussenvoegsel
  - Geen stad
  - Koppeling api met startdatum en adres
  - Verschil in foto advertentie en banner?

Algemeen
  - Nalopen positioning banners op tablet en mobile view qua positioning.
  - Animates?
  - Migration to nuxt.js, for a better development environment?
  - Wat laat je zien als er geen verbinding is met de api?
