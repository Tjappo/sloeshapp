const express = require('express');
const path = require('path');
const app = express();

const port = process.env.PORT || 80;

// Add headers
app.use(function (req, res, next) {

	// Website you wish to allow to connect
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:80');
	res.setHeader('Access-Control-Allow-Origin', 'http://35.177.207.38:3000');
  res.setHeader('Access-Control-Allow-Origin', 'http://ariseit.ddns.net:3000');

	// Request methods you wish to allow
	 res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	// res.setHeader('Access-Control-Allow-Credentials', true);

	// Pass to next layer of middleware
	next();
});

app.use(express.static(path.join(__dirname, 'public')));
app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'public/index.html')));

const server = app.listen(port, function () {
	console.log('Listening on port ' + port);
});

