export const navbarText = {
  en: {
    user: 'User',
    profile: 'Profile',
    cars: 'Cars',
    signOut: 'Sign out',
    signIn: 'Sign in',
    register: 'Register',
    adminDashboard: 'Admin dashboard'
  },
  nl: {
    user: 'Gebruiker',
    profile: 'Profiel',
    cars: 'Auto\'s',
    signOut: 'Uitloggen',
    signIn: 'Inloggen',
    register: 'Gratis registeren',
    adminDashboard: 'Admin dashboard'
  }
}
