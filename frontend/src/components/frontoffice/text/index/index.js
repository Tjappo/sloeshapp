export const indexText = {
  en: {
    title: 'We pay you for driving your car',
    subtitle: 'Find nearby Sloesh advertisements',
    zipCode: 'Zip code',
    licensePlate: 'License *',
    search: 'Show advertisements'
  },
  nl: {
    title: 'Wij betalen jou voor autorijden',
    subtitle: 'Zoek Sloesh campagnes in de buurt',
    zipCode: 'Postcode',
    licensePlate: 'Kenteken *',
    search: 'Toon campagnes'
  }
}
