export const advertisementIndexText = {
  en: {
    compensation: 'compensation per week',
    unavailableAdvertisements: 'There are no available advertisements'
  },
  nl: {
    compensation: 'per week vergoeding',
    unavailableAdvertisements: 'Er zijn geen advertenties beschikbaar'
  }
}

export const advertisementDetailText = {
  en: {
    ...(advertisementIndexText['en']),
    licensePlate: 'License Plate',
    subscribe: 'Subscribe',
    notLoggedIn: 'You have to be logged in to subscribe'
  },
  nl: {
    ...(advertisementIndexText['nl']),
    licensePlate: 'Kentekennummer',
    subscribe: 'Aanmelden',
    notLoggedIn: 'U moet ingelogd zijn om u aan te melden'
  }
}
