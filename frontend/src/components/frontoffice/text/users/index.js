export const userDetailsText = {
  en: {
    exordium: 'Exordium',
    firstName: 'First name',
    middleName: 'Middle name',
    lastName: 'Last name',
    email: 'Email',
    address: 'Address',
    addressLineTwo: 'Address line two',
    zipCode: 'Zip code',
    city: 'City',
    country: 'Country',
    iban: 'IBAN',
    cardHolderName: 'Card holder\'s name',
    activeSince: 'Active since',
    password: 'Password',
    changePasswordText: 'Click here to change your password',
    back: 'Back',
    update: 'Update',
    accountDetails: 'Account details',
    errorMessages: {
      nonEmpty: 'Field has to be non-empty',
      invalidEmail: 'Field does not contain a valid email address'
    }
  },
  nl: {
    exordium: 'Aanhef',
    firstName: 'Voornaam',
    middleName: 'Tussenvoegsel',
    lastName: 'Achternaam',
    email: 'Email',
    address: 'Adres',
    addressLineTwo: 'Adresregel twee',
    zipCode: 'Postcode',
    city: 'Stad',
    country: 'Lang',
    iban: 'IBAN',
    cardHolderName: 'Naam van kaarthouder',
    activeSince: 'Actief sinds',
    password: 'Wachtwoord',
    changePasswordText: 'Klik hier om uw wachtwoord te veranderen',
    back: 'Terug',
    update: 'Bijwerken',
    accountDetails: 'Gebruikersgegevens',
    errorMessages: {
      nonEmpty: 'Veld mag niet leeg zijn',
      invalidEmail: 'Veld bevat geen geldig mailadres'
    }
  }
}

export const userCarsText = {
  en: {
    userCars: 'Cars of the user',
    cars: 'Cars',
    add: 'Add',
    cancel: 'Cancel',
    back: 'Back',
    update: 'Update',
    errorMessages: {
      notLoggedIn: 'Not Logged in',
      somethingWentWrong: 'Something went wrong, please try again later',
      itemAlreadyExists: 'Item already exists',
      itemEmpty: 'Item cannot be empty'
    }
  },
  nl: {
    cars: 'Auto\'s',
    add: 'Toevoegen',
    cancel: 'Annuleren',
    back: 'Terug',
    update: 'Bijwerken',
    errorMessages: {
      notLoggedIn: 'Niet ingelogd',
      somethingWentWrong: 'Er is iets verkeerd gegaan, probeer het later opnieuw',
      itemAlreadyExists: 'Object bestaat al',
      itemEmpty: 'Object can niet leeg zijn'
    }
  }
}

export const resetPasswordText = {
  en: {
    resetPassword: 'Reset Password',
    title: 'Are you sure you want to reset your password?',
    subtitle: 'If you click submit, you will receive an email to reset your password.',
    submit: 'Submit'
  },
  nl: {
    resetPassword: 'Reset Wachtwoord',
    title: 'Weet u zeker dat u uw wachtwoord opnieuw wilt instellen?',
    subtitle: 'Als u op Bevestig klikt, zult u een mail ontvangen om uw wachtwoord opnieuw in te stellen.',
    submit: 'Bevestig'
  }
}
