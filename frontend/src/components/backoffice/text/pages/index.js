export const confirmPageText = {
  en: {
    title: 'Confirm your',
    subtitle: 'You have received a message in the form of an email to confirm your account.'
  },
  nl: {
    title: 'Bevestig uw',
    subtitle: 'U heeft een email ontvangen waarin u uw account kan bevestigen'
  }
}

export const forgotPasswordText = {
  en: {
    title: 'Forgot your password?',
    subtitle: 'Enter your mail to send a mail to reset your password',
    submit: 'Send Mail'
  },
  nl: {
    title: 'Wachtwoord vergeten?',
    subtitle: 'Voer uw emailadres in om een mail te sturen zodat u uw wachtwoord opnieuw kan instellen',
    submit: 'Verstuur mail'
  }
}

export const loginText = {
  en: {
    title: 'Login',
    subtitle: 'Sign In to your account',
    submit: 'Login',
    forgotPassword: 'Forgot password?',
    signup: 'Sign up',
    signupDescription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    signupSubmit: 'Register Now!',
    sessionExpired: 'Your session has expired.'
  },
  nl: {
    title: 'Log in',
    subtitle: 'Log in op uw account',
    submit: 'Log in',
    forgotPassword: 'Wachtwoord vergeten?',
    signup: 'Registreer',
    signupDescription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    signupSubmit: 'Registreer Nu!',
    sessionExpired: 'Uw sessie is verlopen.'
  }
}

export const page404Text = {
  en: {
    title: 'Oops! You\'re lost.',
    subtitle: 'The page you are looking for was not found.'
  },
  nl: {
    title: 'Oops! U bent verdwaald.',
    subtitle: 'De pagina die u probeert te vinden bestaat niet.'
  }
}

export const page500Text = {
  en: {
    title: 'Houston, we have a problem!',
    subtitle: 'The page you are looking for is temporarily unavailable.'
  },
  nl: {
    title: 'Houston, we hebben een probleem!',
    subtitle: 'De pagina die u probeert te vinden is tijdelijk onbereikbaar.'
  }
}

export const registerText = {
  en: {
    title: 'Register',
    subtitle: 'Create your account',
    submit: 'Create Account'
  },
  nl: {
    title: 'Registeren',
    subtitle: 'Maak uw account aan',
    submit: 'Registreer'
  }
}

export const resetPasswordText = {
  en: {
    title: 'Reset password',
    subtitle: 'Enter your new password',
    submit: 'Confirm new password'
  },
  nl: {
    title: 'Wachtwoord opnieuw instellen',
    subtitle: 'Voer uw nieuwe wachtwoord in',
    submit: 'Bevestig uw nieuwe wachtwoord'
  }
}
