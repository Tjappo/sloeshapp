export const passwordText = {
  en: {
    invalidLength: {
      start: 'Password must be between',
      and: 'and',
      end: 'characters long.'
    },
    invalidFormat: 'Password must contain at least 1 upper-case letter, 1 number and 1 special character.'
  },
  nl: {
    invalidLength: {
      start: 'Wachtwoord moet tussen',
      and: 'en',
      end: 'karakters lang zijn.'
    },
    invalidFormat: 'Wachtwoord moet op zijn minst 1 hoofdletter, 1 nummer en 1 speciale karakter bevatten.'
  }
}

export const confirmPasswordValidationText = {
  en: {
    ...(passwordText['en']),
    noMatch: 'Passwords do not match.'
  },
  nl: {
    ...(passwordText['nl']),
    noMatch: 'Wachtwoorden zijn niet gelijk'
  }
}

export const afterTodayText = {
  en: {
    invalid: 'Date must be after today.'
  },
  nl: {
    invalid: 'Datum moet na vandaag zijn.'
  }
}

export const dateCheckText = {
  en: {
    invalid: 'Date must be after start date.'
  },
  nl: {
    invalid: 'Datum moet na startdatum zijn.'
  }
}

export const dateText = {
  en: {
    invalid: 'Field does not contain a correct date time format.'
  },
  nl: {
    invalid: 'Veld bevat geen correct datum formaat.'
  }
}

export const displayNameText = {
  en: {
    invalid: 'The field must start with an uppercase and end with a lowercase character.'
  },
  nl: {
    invalid: 'Het veld moet starten met een hoofdletter en eindigen met een kleine letter.'
  }
}

export const imageText = {
  en: {
    invalidFile: 'An image is required',
    invalidExtension: 'Only .jpg, .jpeg, and .png images are allowed',
    invalidSize: 'File is too large (max 5MiB)'
  },
  nl: {
    invalidFile: 'Een afbeelding is verplicht',
    invalidExtension: 'Alleen .jpg, .jpeg en .png afbeeldingen zijn toegestaan',
    invalidSize: 'Bestand is te groot (max 5MiB)'
  }
}

export const numberText = {
  en: {
    start: 'The input must be greater or equal than',
    middle: 'smaller or equal than',
    end: 'and have a step size of'
  },
  nl: {
    start: 'De invoer moet groter of gelijk zijn aan',
    middle: 'kleiner of gelijk zijn aan',
    end: 'en een stapgrootte hebben van'
  }
}

export const requiredText = {
  en: {
    invalid: 'This field is required.'
  },
  nl: {
    invalid: 'Dit veld is verplicht.'
  }
}

export const validEmailText = {
  en: {
    invalid: 'This is not a valid email address'
  },
  nl: {
    invalid: 'Dit is geen geldig emailadres'
  }
}
