export const defaultHeaderDropdownAccntText = {
  en: {
    settings: 'Settings',
    profile: 'Profile',
    logout: 'Logout'
  },
  nl: {
    settings: 'Instellingen',
    profile: 'Profiel',
    logout: 'Uitloggen'
  }
}
