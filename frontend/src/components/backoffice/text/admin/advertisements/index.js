import {tableText} from '../default'

export const advertisementIndexText = {
  en: {
    ...(tableText['en']),
    create: 'Create',
    advertisement: 'Advertisement',
    title: 'Title',
    compensation: 'Compensation in euros',
    duration: 'Duration in weeks',
    start: 'Start date',
    end: 'End date'
  },
  nl: {
    ...(tableText['nl']),
    create: 'Maak',
    advertisement: 'Advertentie',
    title: 'Titel',
    compensation: 'Compensatie in euro\'s',
    duration: 'Duur in weken',
    start: 'Begindatum',
    end: 'Einddatum'
  }
}

export const advertisementEditText = {
  en: {
    edit: 'Edit',
    advertisement: 'Advertisement',
    subscribedCars: 'Subscribed cars',
    title: 'Title',
    image: 'Image',
    logo: 'Logo',
    imagePlaceholder: 'Upload image of the advertisement',
    logoPlaceholder: 'Upload logo of the advertisement',
    compensation: 'Compensation',
    duration: 'Duration',
    start: 'Start date',
    end: 'End date',
    update: 'Update',
    carTypes: 'Car Types',
    errorMessages: {
      invalidTitle: 'The input must start with an uppercase and end with a lowercase character.',
      invalidCompensation: 'The input must be greater or equal than 0 and have a step size of 0.01.',
      invalidDuration: 'The input must be greater or equal than 1, smaller or equal than 4 and have a step size of 1.',
      invalidStartDate: 'Start date has to be a valid date and after today.',
      invalidEndDate: 'End date has to be a valid date and after the start date.'
    }
  },
  nl: {
    edit: 'Wijzig',
    advertisement: 'Advertentie',
    subscribedCars: 'Geabbonneerde auto\'s',
    title: 'Titel',
    image: 'Afbeelding',
    logo: 'Logo',
    imagePlaceholder: 'Upload afbeelding van de advertentie',
    logoPlaceholder: 'Upload logo van de advertentie',
    compensation: 'Compensatie in euro\'s',
    duration: 'Duur in weken',
    start: 'Begindatum',
    end: 'Einddatum',
    update: 'Update',
    carTypes: 'Auto Types',
    errorMessages: {
      invalidTitle: 'De invoer moet starten met een hoofdletter en eindigen met een kleine letter.',
      invalidCompensation: {
        start: 'De invoer moet groter zijn dan',
        end: 'and have a step size of '
      },
      invalidDuration: {
        start: 'The input must be greater or equal than',
        middle: ', smaller or equal than',
        end: ', and have a step size of'
      },
      invalidStartDate: 'Start date has to be a valid date and after today.',
      invalidEndDate: 'End date has to be a valid date and after the start date.'
    }
  }
}

export const advertisementCreateText = {
  en: {
    imagePlaceholder: 'Upload image of the advertisement',
    logoPlaceholder: 'Upload logo of the advertisement',
    create: 'Create',
    advertisement: 'Advertisement',
    carTypes: 'Car Types'
  },
  nl: {
    imagePlaceholder: 'Upload afbeelding van de advertentie',
    logoPlaceholder: 'Upload logo van de advertentie',
    create: 'Maak',
    advertisement: 'Advertentie',
    carTypes: 'Auto Types'
  }
}

export const historyActionsTableText = {
  en: {
    ...(tableText['en']),
    historyActions: 'History Actions',
    displayName: 'Display name',
    action: 'Action',
    createdAt: 'Created at'
  },
  nl: {
    ...(tableText['nl']),
    historyActions: 'Geschiedenis van acties',
    displayName: 'Weergavenaam',
    action: 'Actie',
    createdAt: 'Aangemaakt op'
  }
}
