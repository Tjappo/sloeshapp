import {userDetailsText} from '../../../../frontoffice/text/users'

export const editText = {
  en: {
    ...(userDetailsText['en']),
    user: 'User',
    edit: 'Edit',
    isLockedOut: 'Is locked out',
    emailConfirmed: 'Is email confirmed'
  },
  nl: {
    ...(userDetailsText['nl']),
    user: 'Gebruiker',
    edit: 'Wijzig',
    isLockedOut: 'Is vergrendeld',
    emailConfirmed: 'Is email bevestigd'
  }
}
