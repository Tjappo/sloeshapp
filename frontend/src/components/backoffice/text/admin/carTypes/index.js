import {tableText} from '../default'

export const carTypeIndexText = {
  en: {
    ...(tableText['en']),
    create: 'Create',
    carType: 'Car Type',
    name: 'Name',
    description: 'Description'
  },
  nl: {
    ...(tableText['nl']),
    create: 'Maak',
    carType: 'Auto type',
    name: 'Naam',
    description: 'Beschrijving'
  }
}

export const carTypeEditText = {
  en: {
    edit: 'Edit',
    carType: 'Car Type',
    name: 'Name',
    description: 'Description',
    update: 'Update',
    errorMessages: {
      invalidName: 'The input must start with an uppercase and end with a lowercase character.'
    }
  },
  nl: {
    edit: 'Wijzig',
    carType: 'Advertentie',
    name: 'Naam',
    description: 'Beschrijving',
    update: 'Update',
    errorMessages: {
      invalidName: 'De invoer moet starten met een hoofdletter en eindigen met een kleine letter.'
    }
  }
}

export const carTypeCreateText = {
  en: {
    create: 'Create',
    carType: 'Car Type'
  },
  nl: {
    create: 'Maak',
    carType: 'Auto type'
  }
}
