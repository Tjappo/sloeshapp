export const tableText = {
  en: {
    captionPrepend: 'Overview of all ',
    users: 'Users',
    searchPlaceholder: 'Type to Search',
    clear: 'Clear',
    none: 'none',
    asc: 'Asc',
    desc: 'Desc',
    last: 'Last',
    sort: 'Sort',
    perPage: 'Per page',
    sortDirection: 'Sort direction',
    filter: 'Filter'
  },
  nl: {
    captionPrepend: 'Overzicht van alle ',
    users: 'Gebruikers',
    searchPlaceholder: 'Typ om te zoeken',
    clear: 'Verwijderen',
    none: 'leeg',
    asc: 'Asc',
    desc: 'Desc',
    last: 'Laatste',
    sort: 'Sorteer',
    perPage: 'Per pagina',
    sortDirection: 'Sorteerrichting',
    filter: 'Filter'
  }
}
