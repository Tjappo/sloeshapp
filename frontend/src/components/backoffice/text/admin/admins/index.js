import {tableText} from '../default'

export const adminCreateText = {
  en: {
    create: 'Create',
    admin: 'Admin'
  },
  nl: {
    create: 'Maak',
    admin: 'Administrator'
  }
}

export const adminIndexText = {
  en: {
    ...(tableText['en']),
    create: 'Create',
    admin: 'admin',
    userName: 'User name',
    displayName: 'Display name',
    isLockedOut: 'Is locked out'
  },
  nl: {
    ...(tableText['nl']),
    create: 'Maak',
    admin: 'administrator',
    userName: 'Gebruikersnaam',
    displayName: 'Weergavenaam',
    isLockedOut: 'Is vergrendeld'
  }
}
