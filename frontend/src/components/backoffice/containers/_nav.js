export default {
  items: {
    'admin_access': [
      {
        name: 'Admin Dashboard',
        url: '/admin/dashboard',
        icon: 'icon-speedometer'
      },
      {
        name: 'Admins',
        url: '/admin/admins',
        icon: 'icon-user'
      },
      {
        name: 'Users',
        url: '/admin/users',
        icon: 'icon-user'
      },
      {
        name: 'Advertisements',
        url: '/admin/advertisements',
        icon: 'icon-social-youtube'
      },
      {
        name: 'Car Types',
        url: '/admin/carTypes',
        icon: 'icon-badge'
      }
    ]
  }
}
