import {ApiCallData, ApiCallMessages} from '../models/apiCallData'
import {axiosGetCall} from './apiCalls'

/**
 * Fetches the users
 * @returns {*|Promise<any>} undefined if invalid, else the users
 */
export const fetchUsers = async () => {
  const apiCallMessages = new ApiCallMessages(
    'Fetching users...',
    'Fetched Data!',
    'Failed fetching users, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'adminView/getUsers'
  )
  return axiosGetCall(apiCallData)
}

/**
 * Fetches the admin
 * @returns {*|Promise<any>} undefined if invalid, else the admin
 */
export const fetchAdmins = async () => {
  const apiCallMessages = new ApiCallMessages(
    'Fetching admin...',
    'Fetched Data!',
    'Failed fetching admin, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'adminView/getAdmins'
  )
  return axiosGetCall(apiCallData)
}
