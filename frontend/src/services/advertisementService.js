import {ApiCallData, ApiCallMessages} from '../models/apiCallData'
import {axiosGetCall, axiosGetCallNoValidation, axiosPostCall} from './apiCalls'

/**
 * Fetch from database: Available advertisements
 * @returns {Promise<void>}
 */
export const fetchAdvertisements = async () => {
  const apiCallMessages = new ApiCallMessages(
    'Fetching advertisements...',
    undefined,
    'Failed fetching advertisements, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'advertisement/GetAvailableAdvertisements'
  )
  return axiosGetCall(apiCallData)
}

/**
 * Fetch from database: Newest advertisements
 * without userId
 * @returns {Promise<void>}
 */
export const fetchNewestAdvertisements = async params => {
  const apiCallMessages = new ApiCallMessages(
    'Fetching advertisements...',
    undefined,
    'Failed fetching advertisements, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'advertisement/GetNewestAvailableAdvertisements',
    params
  )
  return axiosGetCallNoValidation(apiCallData)
}

/**
 * Fetch from database: Available advertisements
 * without userId
 * @returns {Promise<void>}
 */
export const fetchAdvertisementsNoValidation = async params => {
  const apiCallMessages = new ApiCallMessages(
    'Fetching advertisements...',
    undefined,
    'Failed fetching advertisements, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'advertisement/GetAvailableAdvertisementsNoValidation',
    params
  )
  return axiosGetCallNoValidation(apiCallData)
}

/**
 * Submit to database: subscribe user to advertisement
 * @returns {Promise<void>}
 */
export const subscribeAdvertisement = async (params, callback, errorCallback) => {
  const apiCallMessages = new ApiCallMessages(
    'Subscribing to advertisement...',
    'Submitted!',
    'Failed subscribing to advertisement, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'advertisement/SubscribeAdvertisement',
    params,
    callback,
    errorCallback
  )
  await axiosPostCall(apiCallData)
}
