import { ApiCallData, ApiCallMessages } from '../models/apiCallData'
import { axiosGetCall, axiosPutCall } from './apiCalls'

/**
 * Fetches the account details of a user
 * @returns {*|Promise<any>} undefined if invalid, else the account details of the user
 */
export const fetchAccountDetails = async () => {
  const apiCallMessages = new ApiCallMessages(
    'Loading account info...',
    'Fetched Data!',
    'Failed fetching account info, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'customer/get'
  )
  return axiosGetCall(apiCallData)
}

export const updateCustomerData = async (params) => {
  const apiCallMessages = new ApiCallMessages(
    'Updating account info...',
    'Updated account info!',
    'Failed updating account info, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'customer/UpdateCustomer',
    params
  )
  await axiosPutCall(apiCallData)
}

/**
 * Resets the password of the user
 * @param {Function} callback function to execute when the request has been finished
 */
export const resetPassword = async (callback) => {
  const apiCallMessages = new ApiCallMessages(
    'Resetting password...',
    undefined,
    'Failed resetting password, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'user/ResetPassword',
    {},
    callback
  )
  await axiosGetCall(apiCallData)
}
