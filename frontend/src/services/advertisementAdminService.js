import {ApiCallData, ApiCallMessages} from '../models/apiCallData'
import {axiosGetCall, axiosPostCall, axiosPutCall} from './apiCalls'

/**
 * Fetch from database: All advertisements
 * @returns {Promise<void>}
 */
export const fetchAdvertisements = async () => {
  const apiCallMessages = new ApiCallMessages(
    'Fetching advertisements...',
    'Fetched advertisements!',
    'Failed fetching advertisements, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'advertisement/GetAdvertisements'
  )
  return axiosGetCall(apiCallData)
}

/**
 * Fetch from database: Detailed advertisement
 * @returns {Promise<void>}
 */
export const fetchAdvertisementDetail = async (params) => {
  const apiCallMessages = new ApiCallMessages(
    'Fetching advertisement...',
    'Fetched advertisement!',
    'Failed fetching advertisement, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'advertisement/GetAdvertisement',
    params
  )
  return axiosGetCall(apiCallData)
}

/**
 * Fetch from database: Available advertisements
 * without userId
 * @returns {Promise<void>}
 */
export const createAdvertisement = async (params, callback) => {
  const apiCallMessages = new ApiCallMessages(
    'Creating advertisement...',
    'Created advertisement!',
    'Failed creating advertisement, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'advertisement/CreateAdvertisement',
    params,
    callback
  )
  await axiosPostCall(apiCallData)
}

/**
 * Submit to database: subscribe user to advertisement
 * @returns {Promise<void>}
 */
export const updateAdvertisement = async (params, callback) => {
  const apiCallMessages = new ApiCallMessages(
    'Updating advertisement...',
    'Updated advertisement!',
    'Failed updating advertisement, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'advertisement/UpdateAdvertisement',
    params,
    callback
  )
  await axiosPutCall(apiCallData)
}
