import {ApiCallData, ApiCallMessages} from '../models/apiCallData'
import {axiosGetCall, axiosPostCall, axiosPutCall} from './apiCalls'

/**
 * Fetch from database: All car types
 * @returns {Promise<void>}
 */
export const fetchCarTypes = async (params) => {
  const apiCallMessages = new ApiCallMessages(
    'Fetching car types...',
    'Fetched car types!',
    'Failed fetching car types, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'carType/GetCarTypes',
    params
  )
  return axiosGetCall(apiCallData)
}

/**
 * Fetch from database: Available car types
 * without userId
 * @returns {Promise<void>}
 */
export const createCarType = async (params, callback) => {
  const apiCallMessages = new ApiCallMessages(
    'Creating car type...',
    'Created car type!',
    'Failed creating car type, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'carType/CreateCarType',
    params,
    callback
  )
  await axiosPostCall(apiCallData)
}

/**
 * Submit to database: subscribe user to car type
 * @returns {Promise<void>}
 */
export const updateCarType = async (params, callback) => {
  const apiCallMessages = new ApiCallMessages(
    'Updating to car type...',
    'Updated car type!',
    'Failed updating car type, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'carType/UpdateCarType',
    params,
    callback
  )
  await axiosPutCall(apiCallData)
}
