import {ApiCallData, ApiCallMessages} from '../models/apiCallData'
import {axiosPostCallNoValidation} from './apiCalls'

/**
 * Logs a user in
 * @param {Object}    params   post body
 * @param {Function}  callback function to execute when the call has been finished
 * @param {Function}  errorCallback function to execute when the call has encountered an error
 */
export const loginUser = async (params, callback, errorCallback) => {
  const apiCallMessages = new ApiCallMessages(
    'Logging in...',
    'Logged in!',
    'Failed logged in..., please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'auth/login',
    params,
    callback,
    errorCallback
  )
  await axiosPostCallNoValidation(apiCallData)
}
