import { ApiCallData, ApiCallMessages } from '../models/apiCallData'
import { axiosGetCall, axiosPostCall, axiosDeleteCall } from './apiCalls'

/**
 * Fetches the cars of a user
 * @returns {*|Promise<any>} undefined if invalid, else the account details of the user
 */
export const fetchCars = async () => {
  const apiCallMessages = new ApiCallMessages(
    'Fetching cars of user...',
    'Fetched data!',
    'Failed fetching cars, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'car/GetCars'
  )
  return axiosGetCall(apiCallData)
}

/**
 * Fetches the supported cars of a user for an advertisement
 * @returns {*|Promise<any>} undefined if invalid, else the account details of the user
 */
export const fetchSupportedCars = async (params) => {
  const apiCallMessages = new ApiCallMessages(
    'Fetching cars of user...',
    'Fetched data!',
    'Failed fetching cars, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'car/GetSupportedCars',
    params
  )
  return axiosGetCall(apiCallData)
}

export const addCar = async (params, callback, errorCallback) => {
  const apiCallMessages = new ApiCallMessages(
    'Adding car...',
    'Added car!',
    'Failed adding car, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'car/addCar',
    params,
    callback,
    errorCallback
  )
  await axiosPostCall(apiCallData)
}

export const deleteCar = async (params, callback, errorCallback) => {
  const apiCallMessages = new ApiCallMessages(
    'Deleting car...',
    'Deleted car!',
    'Failed deleting car, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'car/deleteCar',
    params,
    callback,
    errorCallback
  )
  await axiosDeleteCall(apiCallData)
}
