import {ApiCallData, ApiCallMessages} from '../models/apiCallData'
import {axiosPostCall, axiosPutCall} from './apiCalls'

/**
 * Updates the user
 * @param {Object}    params   post body
 * @param {Function}  callback function to execute when the call has been finished
 */
export const updateUser = async (params, callback) => {
  const apiCallMessages = new ApiCallMessages(
    'Updating user info...',
    'User updated!',
    'Failed updating user, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'admin/UpdateUser',
    params,
    callback
  )
  await axiosPutCall(apiCallData)
}

/**
 * Updates the admin
 * @param {Object}    params   post body
 * @param {Function}  callback function to execute when the call has been finished
 */
export const updateAdmin = async (params, callback) => {
  const apiCallMessages = new ApiCallMessages(
    'Updating admin info...',
    'Admin updated!',
    'Failed updating admin, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'admin/UpdateAdmin',
    params,
    callback
  )
  await axiosPutCall(apiCallData)
}

/**
 * Creates an admin
 * @param {Object}    params   post body
 * @param {Function}  callback function to execute when the call has been finished
 */
export const createAdmin = async (params, callback) => {
  const apiCallMessages = new ApiCallMessages(
    'Creating admin...',
    'Admin created!',
    'Failed creating admin, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'admin/CreateAdmin',
    params,
    callback
  )
  await axiosPostCall(apiCallData)
}
