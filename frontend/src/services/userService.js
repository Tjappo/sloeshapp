import {ApiCallData, ApiCallMessages} from '../models/apiCallData'
import { axiosPostCall, axiosPostCallNoValidation, axiosPutCallNoValidation } from './apiCalls'

/**
 * Registers a user
 * @param {Object}    params   post body
 * @param {Function}  callback function to execute when the call has been finished
 */
export const registerUser = async (params, callback) => {
  const apiCallMessages = new ApiCallMessages(
    'Registering user...',
    'User registered!',
    'Failed registering user, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'user/register',
    params,
    callback
  )
  await axiosPostCallNoValidation(apiCallData)
}

/**
 * Updates the email of the user
 * @param {Object}    params   post body
 * @param {Function}  callback function to execute when the call has been finished
 */
export const updateEmail = async (params, callback) => {
  const apiCallMessages = new ApiCallMessages(
    'Resetting email address...',
    undefined,
    'Failed updating email address, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'user/updateEmail',
    params,
    callback
  )
  await axiosPostCall(apiCallData)
}

/**
 * Resets the password of the user
 * @param {Object}    params   post body
 * @param {Function}  callback function to execute when the call has been finished
 */
export const resetPassword = async (params, callback) => {
  const apiCallMessages = new ApiCallMessages(
    'Resetting password...',
    'Password has been reset!',
    'Failed resetting password, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'user/resetPasswordText',
    params,
    callback
  )
  await axiosPutCallNoValidation(apiCallData)
}

/**
 * Sends a mail to reset the password of the user
 * @param {Object}  params  post body
 */
export const forgotPassword = async (params) => {
  const apiCallMessages = new ApiCallMessages(
    'Sending email to reset password...',
    'Email has been sent!',
    'Failed sending email to reset password, please try again later!'
  )
  const apiCallData = new ApiCallData(
    apiCallMessages,
    'user/forgotPassword',
    params
  )
  await axiosPostCallNoValidation(apiCallData)
}
