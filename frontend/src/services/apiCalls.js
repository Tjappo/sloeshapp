import store from '../store/index'

/**
 * Gets the user id from the store and validates the store data
 * @returns {*}
 */
const getUserId = () => {
  if (!store.getters['auth/isAuthenticated']) {
    VueEventListener.fire('errorMessage', 'Invalid access')
    VueEventListener.fire('stopLoading')
    return
  }

  return store.getters['auth/userId']
}

/**
 * Places an api call to get data
 * @param {ApiCallData} apiCallData the data to use to place the api call
 * @returns {*|Promise<any>} undefined if invalid, else the data of the api call
 */
export const axiosGetCall = (apiCallData) => {
  apiCallData.loadingMessage()
  const userId = getUserId()

  if (!userId) {
    return
  }

  return axios.get('/api/' + apiCallData.route, {
    params: {
      userId,
      ...(apiCallData.params || {})
    }
  })
    .then((res) => responseSuccessHandler(apiCallData, res.data))
    .catch((error) => responseErrorHandler(apiCallData, (error.response || {}).data))
    .finally(() => VueEventListener.fire('stopLoading'))
}

/**
 * Places an api call to post data
 * @param {ApiCallData} apiCallData the data to use to place the api call
 * @returns {*|Promise<any>} undefined if invalid, else the data of the api call
 */
export const axiosPostCall = (apiCallData) => {
  apiCallData.loadingMessage()
  const userId = getUserId()

  if (!userId) {
    return
  }

  return axios.post('/api/' + apiCallData.route, {
    userId,
    ...(apiCallData.params || {})
  })
    .then((res) => responseSuccessHandler(apiCallData, res.data))
    .catch((error) => responseErrorHandler(apiCallData, (error.response || {}).data))
    .finally(() => VueEventListener.fire('stopLoading'))
}

/**
 * Places an api call to put data
 * @param {ApiCallData} apiCallData the data to use to place the api call
 * @returns {*|Promise<any>} undefined if invalid, else the data of the api call
 */
export const axiosPutCall = (apiCallData) => {
  apiCallData.loadingMessage()
  const userId = getUserId()

  if (!userId) {
    return
  }

  return axios.put('/api/' + apiCallData.route, {
    userId,
    ...(apiCallData.params || {})
  })
    .then((res) => responseSuccessHandler(apiCallData, res.data))
    .catch((error) => responseErrorHandler(apiCallData, (error.response || {}).data))
    .finally(() => VueEventListener.fire('stopLoading'))
}

/**
 * Places an api call to delete data
 * @param {ApiCallData} apiCallData the data to use to place the api call
 * @returns {*|Promise<any>} undefined if invalid, else the data of the api call
 */
export const axiosDeleteCall = (apiCallData) => {
  apiCallData.loadingMessage()
  const userId = getUserId()

  if (!userId) {
    return
  }

  return axios.delete('/api/' + apiCallData.route, {
    data: {
      userId,
      ...(apiCallData.params || {})
    }
  })
    .then((res) => responseSuccessHandler(apiCallData, res.data))
    .catch((error) => responseErrorHandler(apiCallData, (error.response || {}).data))
    .finally(() => VueEventListener.fire('stopLoading'))
}

/**
 * Places an api call to get data
 * Without validation of the user id
 * @param {ApiCallData} apiCallData the data to use to place the api call
 * @returns {*|Promise<any>} undefined if invalid, else the data of the api call
 */
export const axiosGetCallNoValidation = (apiCallData) => {
  apiCallData.loadingMessage()

  return axios.get('/api/' + apiCallData.route, {
    params: apiCallData.params || {}
  })
    .then((res) => responseSuccessHandler(apiCallData, res.data))
    .catch((error) => responseErrorHandler(apiCallData, (error.response || {}).data))
    .finally(() => VueEventListener.fire('stopLoading'))
}

/**
 * Places an api get call to post data
 * Without validation of the user id
 * @param {ApiCallData} apiCallData the data to use to place the api call
 * @returns {*|Promise<any>} undefined if invalid, else the data of the api call
 */
export const axiosPostCallNoValidation = (apiCallData) => {
  apiCallData.loadingMessage()

  return axios.post('/api/' + apiCallData.route, apiCallData.params || {})
    .then((res) => responseSuccessHandler(apiCallData, res.data))
    .catch((error) => responseErrorHandler(apiCallData, (error.response || {}).data))
    .finally(() => VueEventListener.fire('stopLoading'))
}

/**
 * Places an api put call to put data
 * Without validation of the user id
 * @param {ApiCallData} apiCallData the data to use to place the api call
 * @returns {*|Promise<any>} undefined if invalid, else the data of the api call
 */
export const axiosPutCallNoValidation = (apiCallData) => {
  apiCallData.loadingMessage()

  return axios.put('/api/' + apiCallData.route, apiCallData.params || {})
    .then((res) => responseSuccessHandler(apiCallData, res.data))
    .catch((error) => responseErrorHandler(apiCallData, (error.response || {}).data))
    .finally(() => VueEventListener.fire('stopLoading'))
}

/**
 * Handles a successful response
 * @param {ApiCallData}  apiCallData  the class that contains the methods to execute
 * @param {object}       data         data of the response
 * @returns {object} data that was given
 */
const responseSuccessHandler = (apiCallData, data) => {
  apiCallData.successMessage()
  apiCallData.successCallbackIfExists(data)
  return data
}

/**
 * Handles an error response
 * @param {ApiCallData}  apiCallData  the class that contains the methods to execute
 * @param {object}       error        error of the response
 * @returns {Promise<void>} of the function
 */
const responseErrorHandler = async (apiCallData, error) => {
  apiCallData.errorCallbackIfExists(error)
  apiCallData.errorMessage()
}
