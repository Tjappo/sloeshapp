const AdvertisementIndex = () => import('../../../components/backoffice/admin/advertisements/Index.vue')
const AdvertisementEdit = () => import('../../../components/backoffice/admin/advertisements/Edit.vue')
const AdvertisementCreate = () => import('../../../components/backoffice/admin/advertisements/Create.vue')

export default {
  path: 'advertisements',
  component: {
    render (c) {
      return c('router-view')
    }
  },
  children: [
    {
      path: '',
      component: AdvertisementIndex,
      name: 'AdvertisementIndex',
      meta: {label: 'Advertisement Overview'}
    },
    {
      path: 'edit',
      component: AdvertisementEdit,
      name: 'AdvertisementEdit',
      meta: {label: 'Advertisement Edit'},
      props: true
    },
    {
      path: 'create',
      component: AdvertisementCreate,
      name: 'AdvertisementCreate',
      meta: {label: 'Advertisement Create'}
    }
  ]
}
