// Helper functions
import {beforeEnterAllowedRoutes, beforeEnterAuthCheck} from '../../helpers/authenticationHelpers'
// Views
import UserViews from './user'
import AdminViews from './admin'
import AdvertisementViews from './advertisements'
import CarTypeViews from './carTypes'

// Container
const AdminContainer = () => import('../../components/backoffice/containers/AdminContainer.vue')

// Dashboard
const AdminDashboard = () => import('../../components/backoffice/admin/Dashboard.vue')

export default [
  {
    path: '/admin',
    component: AdminContainer,
    beforeEnter: (to, from, next) => beforeEnterAuthCheck(to, from, next),
    children: [
      {
        path: '',
        component: {
          render (c) {
            return c('router-view')
          }
        },
        redirect: 'dashboard',
        beforeEnter: (to, from, next) => beforeEnterAllowedRoutes(to, from, next, ['admin'], '/500'),
        children: [
          {
            path: 'dashboard',
            component: AdminDashboard,
            name: 'AdminDashboard',
            meta: {label: 'Admin Dashboard'}
          },
          UserViews,
          AdminViews,
          AdvertisementViews,
          CarTypeViews
        ]
      }
    ]
  }
]
