const UserIndex = () => import('../../../components/backoffice/admin/users/Index.vue')
const UserEdit = () => import('../../../components/backoffice/admin/users/Edit.vue')

export default {
  path: 'users',
  component: {
    render (c) {
      return c('router-view')
    }
  },
  children: [
    {
      path: '',
      component: UserIndex,
      name: 'AdminUserIndex',
      meta: {label: 'User Overview'}
    },
    {
      path: 'edit',
      component: UserEdit,
      name: 'AdminUserEdit',
      meta: {label: 'User Edit'},
      props: true
    }
  ]
}
