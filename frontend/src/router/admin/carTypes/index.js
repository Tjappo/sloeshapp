const CarTypeIndex = () => import('../../../components/backoffice/admin/carTypes/Index.vue')
const CarTypeEdit = () => import('../../../components/backoffice/admin/carTypes/Edit.vue')
const CarTypeCreate = () => import('../../../components/backoffice/admin/carTypes/Create.vue')

export default {
  path: 'carTypes',
  component: {
    render (c) {
      return c('router-view')
    }
  },
  children: [
    {
      path: '',
      component: CarTypeIndex,
      name: 'CarTypeIndex',
      meta: {label: 'CarType Overview'}
    },
    {
      path: 'edit',
      component: CarTypeEdit,
      name: 'CarTypeEdit',
      meta: {label: 'CarType Edit'},
      props: true
    },
    {
      path: 'create',
      component: CarTypeCreate,
      name: 'CarTypeCreate',
      meta: {label: 'CarType Create'}
    }
  ]
}
