const AdminIndex = () => import('../../../components/backoffice/admin/admins/Index.vue')
const AdminEdit = () => import('../../../components/backoffice/admin/admins/Edit.vue')
const AdminCreate = () => import('../../../components/backoffice/admin/admins/Create.vue')

export default {
  path: 'admins',
  component: {
    render (c) {
      return c('router-view')
    }
  },
  children: [
    {
      path: '',
      component: AdminIndex,
      name: 'AdminIndex',
      meta: {label: 'Admin Overview'}
    },
    {
      path: 'edit',
      component: AdminEdit,
      name: 'AdminEdit',
      meta: {label: 'Admin Edit'},
      props: true
    },
    {
      path: 'create',
      component: AdminCreate,
      name: 'AdminCreate',
      meta: {label: 'Admin Create'}
    }
  ]
}
