const UserResetPassword = () => import('../../../components/frontoffice/pages/users/edit/UserResetPassword.vue')

export default [
  {
    path: 'edit',
    redirect: '/user/details',
    component: {
      render (c) {
        return c('router-view')
      }
    },
    children: [
      {
        path: 'resetPasswordText',
        name: 'UserResetPassword',
        meta: {label: 'Reset Password'},
        component: UserResetPassword
      }
    ]
  }
]
