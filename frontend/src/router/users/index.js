// Helper functions
import {beforeEnterAllowedRoutes, beforeEnterAuthCheck} from '../../helpers/authenticationHelpers'
// User Edit
import UserEditRoutes from './edit'

// Container
const DefaultContainer = () => import('../../components/frontoffice/containers/DefaultContainer.vue')

// Views
const UserDetails = () => import('../../components/frontoffice/pages/users/UserDetails.vue')
const UserCars = () => import('../../components/frontoffice/pages/users/UserCars.vue')

export default [
  {
    path: '/user',
    component: DefaultContainer,
    beforeEnter: (to, from, next) => beforeEnterAuthCheck(to, from, next),
    children: [
      {
        path: '',
        beforeEnter: (to, from, next) => beforeEnterAllowedRoutes(to, from, next, ['api'], '/404'),
        component: {
          render (c) {
            return c('router-view')
          }
        },
        children: [
          {
            path: '',
            name: 'UserDetails',
            meta: {label: 'User Details'},
            component: UserDetails
          },
          {
            path: 'cars',
            name: 'UserCars',
            meta: {label: 'User Cars'},
            component: UserCars
          },
          ...UserEditRoutes
        ]
      }
    ]
  }
]
