const AdvertisementOverview = () => import('../../components/frontoffice/pages/advertisement/Index.vue')
const AdvertisementDetail = () => import('../../components/frontoffice/pages/advertisement/Detail.vue')
const DefaultContainer = () => import('../../components/frontoffice/containers/DefaultContainer.vue')

export default [
  {
    path: '/advertisements',
    component: DefaultContainer,
    children: [
      {
        path: '',
        name: 'AdvertisementOverview',
        meta: {label: 'Advertisement Overview'},
        component: AdvertisementOverview,
        props: true
      },
      {
        path: 'detailed',
        name: 'AdvertisementDetail',
        meta: {label: 'Advertisement Detailed'},
        component: AdvertisementDetail,
        props: true
      }
    ]
  }
]
