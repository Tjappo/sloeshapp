// User routes
import UserRoutes from './users/index'
// Admin routes
import AdminRoutes from './admin/index'
// Advertisement routes
import AdvertisementRoutes from './advertisement/index'
// Default routes
import DefaultRoutes from './default/index'

export const routes = [
  ...UserRoutes,
  ...AdminRoutes,
  ...AdvertisementRoutes,
  ...DefaultRoutes
]
