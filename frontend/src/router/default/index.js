const Page404 = () => import('../../components/backoffice/pages/Page404.vue')
const Page500 = () => import('../../components/backoffice/pages/Page500.vue')
const Login = () => import('../../components/frontoffice/pages/actions/Login.vue')
const Logout = () => import('../../components/frontoffice/pages/actions/Logout.vue')
const Register = () => import('../../components/frontoffice/pages/actions/Register.vue')
const ResetPassword = () => import('../../components/frontoffice/pages/actions/ResetPassword.vue')
const ConfirmPage = () => import('../../components/frontoffice/pages/actions/ConfirmPage.vue')
const Index = () => import('../../components/frontoffice/pages/index/Index.vue')
const DefaultFrontOfficeContainer = () => import('../../components/frontoffice/containers/DefaultContainer.vue')

export default [
  {
    path: '/',
    component: DefaultFrontOfficeContainer,
    children: [
      {
        path: '',
        name: 'Index',
        component: Index
      },
      {
        path: 'login',
        component: Login,
        name: 'Login',
        props: true
      },
      {
        path: 'logout',
        component: Logout,
        name: 'Logout'
      },
      {
        path: 'register',
        component: Register,
        name: 'Register'
      },
      {
        path: 'resetPassword',
        component: ResetPassword,
        name: 'ResetPassword'
      },
      {
        path: '500',
        component: Page500,
        name: 'Page500'
      },
      {
        path: '404',
        component: Page404,
        name: 'Page404'
      },
      {
        path: 'confirmPage',
        component: ConfirmPage,
        name: 'ConfirmPage',
        props: true
      }
    ]
  },
  {
    path: '*',
    redirect: '/404'
  }
]
