import 'core-js/es6/promise'
import 'core-js/es6/string'
import 'core-js/es7/array'

import Vue from 'vue'
import moment from 'moment'
import BootstrapVue from 'bootstrap-vue'
import axios from './helpers/axios-auth'
import MultiLangMixin from './helpers/MultiLangMixin'
import router from './router/index'
import store from './store/index'
import {sync} from 'vuex-router-sync'
import App from '@/components/backoffice/app-root'
import Toastr from 'vue-toastr'

import '@/assets/scss/backoffice/vendors/toastr.scss'
// CoreUI Icons Set
import '@coreui/icons/css/coreui-icons.min.css'
/* Import Simple Line Icons Set */
import 'simple-line-icons/scss/simple-line-icons.scss'
/* Import Flag Icons Set */
import 'flag-icon-css/css/flag-icon.min.css'
/* Import Bootstrap Vue Styles */
import 'bootstrap-vue/dist/bootstrap-vue.css'

import '@/assets/scss/flags.scss'

require('./helpers/VueEventListener')

moment.locale('nl-nl')
window.moment = moment
window.axios = axios

Vue.use(BootstrapVue)
Vue.use(Toastr)

Vue.mixin(MultiLangMixin)

sync(store, router)

export const app = new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})
