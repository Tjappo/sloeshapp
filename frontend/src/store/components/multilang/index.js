const state = {
  activeLang: 'nl',
  fallbackLang: 'en'
}

const mutations = {
  changeLang (state, lang) {
    state.activeLang = lang
  }
}

const actions = {
  changeLang ({commit, dispatch}, lang) {
    commit('changeLang', lang)
  }
}

const getters = {
  activeLang (state) {
    return state.activeLang
  },
  fallbackLang (state) {
    return state.fallbackLang
  }
}

export const multilang = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
