import router from '../../../router/index'
import { getLoginRedirect, redirectNotLoggedIn } from '../../../helpers/authenticationHelpers'
import { loginUser } from '../../../services/authenticationService'

const cacheHelpers = {
  calculateExpirationDate (expiresIn) {
    return moment().add(expiresIn, 's')
  },
  registerUser (userId, displayName, token, expiresIn, roles) {
    localStorage.setItem('token', token)
    localStorage.setItem('userId', userId)
    localStorage.setItem('displayName', displayName)
    localStorage.setItem('expirationDate', cacheHelpers.calculateExpirationDate(expiresIn).toISOString())
    localStorage.setItem('roles', roles)
  },
  clearUserInfo () {
    localStorage.removeItem('expirationDate')
    localStorage.removeItem('token')
    localStorage.removeItem('userId')
    localStorage.removeItem('displayName')
    localStorage.removeItem('roles')
  }
}

const state = {
  token: null,
  userId: null,
  displayName: null,
  roles: null
}

const mutations = {
  authUser (state, userData) {
    state.token = userData.auth_token
    state.userId = userData.id
    state.displayName = userData.displayName
    state.roles = userData.roles
  },
  clearAuthData (state) {
    state.token = null
    state.userId = null
    state.displayName = null
    state.roles = null
  }
}

const actions = {
  setLoginValues ({ commit, dispatch }, values) {
    cacheHelpers.registerUser(values.id, values.displayName, values.auth_token, values.expires_in, values.roles)
    dispatch('setLogoutTimer', { expiresIn: values.expires_in })
    commit('authUser', values)
  },
  async login ({ commit, dispatch }, [formData, successCallback, errorCallback]) {
    await loginUser(formData, (values) => {
      dispatch('setLoginValues', values)
      if (successCallback) {
        successCallback()
      } else {
        router.push({ name: getLoginRedirect() })
      }
    }, errorCallback)
  },
  setLogoutTimer ({ commit, dispatch }, { expiresIn }) {
    setTimeout(() => dispatch('logout'), expiresIn * 1000)
  },
  async tryAutoLogin ({ commit, dispatch }) {
    const token = localStorage.getItem('token')
    const expirationDate = localStorage.getItem('expirationDate')
    const userId = localStorage.getItem('userId')
    const roles = localStorage.getItem('roles')
    const displayName = localStorage.getItem('displayName')
    if (!token || !expirationDate || !userId || !roles) {
      dispatch('logout', true)
      return
    }

    if (moment().isAfter(moment(expirationDate))) {
      dispatch('logout', true)
      return
      // Replace the line above with the line below for refreshing the access token upon page reload
      // await refreshAccessToken({ refreshToken }, (values) => dispatch('setLoginValues', values), () => dispatch('logout'))
    }

    commit('authUser', {
      displayName: displayName,
      auth_token: token,
      id: userId,
      roles: roles
    })
    // router.push({ name: getLoginRedirect() })
  },
  logout ({ commit }, sessionExpired) {
    commit('clearAuthData')
    cacheHelpers.clearUserInfo()
    redirectNotLoggedIn(sessionExpired)
  }
}

const getters = {
  userId (state) {
    return state.userId
  },
  roles (state) {
    return state.roles
  },
  displayName (state) {
    return state.displayName
  },
  token (state) {
    return state.token
  },
  isAuthenticated (state) {
    return state.token !== null
  }
}

export const auth = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
