import Vue from 'vue'
import Vuex from 'vuex'

import {auth} from './components/auth'
import {multilang} from './components/multilang'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    multilang
  }
})
