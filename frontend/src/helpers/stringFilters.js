/**
 * Converts a camelcase string to a capitalized space separated string
 * @param   {string} value  to convert
 * @returns {string} converted string
 */
export const camelcaseToSpaceSeparated = (value) => {
  if (!value) {
    return ''
  }

  return value.toString()
    .replace(/([A-Z])/g, (x) => {
      return ' ' + x.charAt(0).toLowerCase() + x.slice(1)
    })
}

/**
 * Converts a string to uppercase
 * @param   {string} value  string to convert to uppercase
 * @returns {string} uppercase string
 */
export const toUpperCase = (value) => {
  if (!value) {
    return ''
  }

  value = value.toString().trim()

  return value.toUpperCase()
}

/**
 * Capitalizes a string
 * @param   {string} value  string to capitalize
 * @returns {string} capitalized string
 */
export const capitalize = (value) => {
  if (!value) {
    return ''
  }

  value = value.toString().trim()

  return value.charAt(0).toUpperCase() + value.slice(1)
}

/**
 * Converts an array to a comma separated string
 * if the list length is greater than two, it will use 'and' before the last element
 * @param   {string[]} value   array to convert
 * @returns {string}   string  representation of the array
 */
export const arrayToEnumeratedString = (value) => {
  if (!Array.isArray(value) || value.length === 0) {
    return ''
  }

  return [value.slice(0, -1).join(', '), value.slice(-1)[0]].join(value.length < 2 ? '' : ' and ')
}

/**
 * Converts the value to a date format DD-MM-YYYY
 * @param {string}  value  to convert
 * @returns {string} formatted string
 */
export const formatDate = (value) => {
  if (!value) {
    return ''
  }

  return moment(value).format('DD-MM-YYYY')
}

/**
 * Converts the value to a datetime format DD-MM-YYYY hh:mm
 * @param {string}  value  to convert
 * @returns {string} formatted string
 */
export const formatDateTime = (value) => {
  if (!value) {
    return ''
  }

  return moment(value).format('DD-MM-YYYY HH:mm')
}
