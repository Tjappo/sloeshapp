export const AuthStatusApproved = 'AuthStatusApproved'
export const AuthStatusInProgress = 'AuthStatusInProgress'

const pwMinLength = 8
const pwMaxLength = 50
const maxFileSize = 5 * Math.pow(2, 20)

const passwordRegex = '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$'
const allowedImageExtensionRegex = /\.(jpg|jpeg|png)$/i
export const validEmailRegex = /[^\s@]+@[^\s@]+\.[^\s@]+/
export const validDisplayNameRegex = /^[A-Z]+[a-zA-Z\s]+[a-zA-Z]*$/

export default {
  pwMinLength: pwMinLength,
  pwMaxLength: pwMaxLength,
  maxFileSize: maxFileSize,
  passwordRegex: passwordRegex,
  allowedImageExtensionRegex: allowedImageExtensionRegex,
  validEmailRegex: validEmailRegex
}
