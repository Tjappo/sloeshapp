import axios from 'axios'
import {apiServer} from '../../config'

/**
 * Created a new axios instance
 * @type {AxiosInstance}
 */
const instance = axios.create({
  baseURL: apiServer
})

/**
 * Adds the authorization header to the axios calls
 */
instance.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem('token')
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

export default instance
