// define a mixin object
export default {
  methods: {
    getText (key) {
      if (!this.textData) {
        return ''
      }

      return this.textMultilang[key] || this.textData[this.$store.getters['multilang/fallbackLang']][key]
    }
  },
  computed: {
    textMultilang () {
      return this.textData[this.$store.getters['multilang/activeLang']] || this.textData[this.$store.getters['multilang/fallbackLang']]
    }
  }
}
