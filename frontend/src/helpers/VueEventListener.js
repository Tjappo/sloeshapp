import Vue from 'vue'

/**
 * VueEventListener class
 */
class VueEventListener {
  constructor () {
    this.vue = new Vue()
  }

  fire (event, data = null) {
    this.vue.$emit(event, data)
  }

  listen (event, callback) {
    this.vue.$on(event, callback)
  }
}

/**
 * Binds a new VueEventListener class to the window
 */
window.VueEventListener = new VueEventListener()
