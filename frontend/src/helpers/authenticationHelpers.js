import store from '../store'
import router from '../router'

/**
 * Validates the current store values
 * @returns {boolean}  whether the values are valid
 */
export const isAuthenticated = () => {
  const roles = localStorage.getItem('roles')
  return !!localStorage.getItem('token') && !!localStorage.getItem('userId') && !!roles &&
    ['api_access', 'admin_access', 'unvalidated_access'].includes(roles) &&
    typeof localStorage.getItem('twoFactorEnabled') !== 'undefined'
}

/**
 * Gets the name of the route to redirect to after the login
 * @returns {string}  name of the route to redirect to after logging in
 */
export const getLoginRedirect = () => {
  if (!isAuthenticated()) {
    return 'Index'
  }

  const role = getRoleName()

  if (role === 'admin') {
    return 'AdminDashboard'
  }

  return 'Index'
}

/**
 * Gets the name of the role
 * @returns {*|string}  undefined if role is invalid or name of the role
 */
export const getRoleName = () => {
  const roles = localStorage.getItem('roles')
  if (!roles) {
    return
  }

  const index = roles.indexOf('_access')
  if (index === -1) {
    return
  }

  return roles.substring(0, index)
}

/**
 * Checks whether you are authenticated before you are redirected
 * @param {string}    to route you want to visit
 * @param {string}    from route you are coming from
 * @param {Function}  next route helper function
 * @returns {*}  redirects you to the login page if you are not authenticated,
 *               otherwise goes to the next route helper function
 */
export const beforeEnterAuthCheck = (to, from, next) => {
  if (!isAuthenticated()) {
    return next('/login')
  }

  next()
}

/**
 * Checks whether you are allowed to visit a specific route
 * @param {string}   to          route you want to visit
 * @param {string}   from        route you are coming from
 * @param {Function} next        route helper function
 * @param {string[]} allowed     array of roles that are allowed
 * @param {string}   redirectTo  route to redirect to if you are not allowed to visit
 * @returns {*}  redirects you to the validation page if you are not validated,
 *               if you are not allowed to visit redirect to the redirectTo route,
 *               otherwise goes to the next route helper function
 */
export const beforeEnterAllowedRoutes = (to, from, next, allowed, redirectTo) => {
  const role = getRoleName()

  if (allowed.includes(role)) {
    return next()
  }

  if (role === 'unvalidated') {
    return next('/confirmPage')
  }

  return next(redirectTo)
}

/**
 * Redirects to the login page if you are not logged in
 * @param {boolean}  sessionExpired  whether the session has expired
 */
export const redirectNotLoggedIn = (sessionExpired) => {
  if (store['state']['route']['path'].indexOf('user') !== -1 || store['state']['route']['path'].indexOf('admin') !== -1) {
    router.push({name: 'Login', params: {loggedOutAutomatically: sessionExpired}})
  }
}
