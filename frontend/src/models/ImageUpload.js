export default {

  getBase64OfImage (file) {
    return new Promise(function (resolve, reject) {
      let reader = new FileReader()
      reader.onload = function () {
        resolve(reader.result)
      }
      reader.onerror = reject
      reader.readAsDataURL(file)
    })
  },
  getImage (event) {
    let files = event.target.files || event.dataTransfer.files
    if (!files.length) {
      console.log('No files detected')
      return undefined
    }
    return files[0]
  }
}
