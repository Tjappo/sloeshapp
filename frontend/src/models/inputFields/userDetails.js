import {FormInput, FormOptions, FormRadio} from '../Form'
import Countries from '../CountryNames'

export default {
  'exordium': new FormRadio(
    'exordium',
    {'Mr': 'Mr', 'Mrs': 'Mrs'},
    ['required']),
  'firstName': new FormInput(
    'firstName',
    'icon-user',
    'text',
    undefined,
    'firstName',
    ['required']),
  'middleName': new FormInput(
    'middleName',
    'icon-user',
    'text',
    undefined,
    'middleName',
    []),
  'lastName': new FormInput(
    'lastName',
    'icon-user',
    'text',
    undefined,
    'lastName',
    ['required']),
  'address': new FormInput(
    'address',
    'icon-user',
    'text',
    undefined,
    'address1',
    ['required']),
  'addressLineTwo': new FormInput(
    'addressLineTwo',
    'icon-user',
    'text',
    undefined,
    'address2',
    []),
  'zipcode': new FormInput(
    'zipcode',
    'icon-user',
    'text',
    undefined,
    'postal-code',
    ['required']),
  'city': new FormInput(
    'city',
    'icon-user',
    'text',
    undefined,
    'city',
    ['required']),
  'countryOfResidence': new FormOptions(
    'countryOfResidence',
    'icon-user',
    Countries,
    ['required']),
  'iban': new FormInput(
    'iban',
    'icon-user',
    'text',
    undefined,
    undefined,
    ['required']),
  'cardHolderName': new FormInput(
    'cardHolderName',
    'icon-user',
    'text',
    undefined,
    undefined,
    ['required']),
  'email': new FormInput(
    'email',
    '@',
    'email',
    undefined,
    'email',
    ['required', 'email'])
}
