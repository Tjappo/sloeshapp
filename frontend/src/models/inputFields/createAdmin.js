import { FormInput, FormCheckbox } from '../Form'

export default [
  new FormInput(
    'displayName',
    'icon-user',
    'text',
    undefined,
    'firstName',
    ['required', 'displayName']),
  new FormInput(
    'userName',
    '@',
    'email',
    undefined,
    'email',
    ['required', 'email']),
  new FormCheckbox(
    'isLockedOut',
    'icon-lock',
    'Is locked out',
    false,
    true,
    []),
  new FormInput(
    'password',
    'icon-lock',
    'password',
    undefined,
    'password',
    ['password', 'required']),
  new FormInput(
    'confirmPassword',
    'icon-lock',
    'password',
    undefined,
    undefined,
    ['confirmPassword', 'required'])
]
