import { FormInput } from '../Form'

export default [
  new FormInput(
    'name',
    'icon-user',
    'text',
    undefined,
    undefined,
    ['required']),
  new FormInput(
    'description',
    'icon-user',
    'text',
    undefined,
    undefined,
    ['required', 'displayName'])
]
