import { FormInput, FormNumber } from '../Form'

export default [
  new FormInput(
    'title',
    'icon-user',
    'text',
    undefined,
    undefined,
    ['required', 'displayName']),
  new FormNumber(
    'compensation',
    '€',
    0.00,
    Number.MAX_VALUE,
    0.01,
    ['required']),
  new FormNumber(
    'duration',
    'icon-hourglass',
    1,
    4,
    1,
    ['required']),
  new FormInput(
    'start',
    'icon-clock',
    'datetime-local',
    undefined,
    undefined,
    ['required', 'date', 'afterToday']),
  new FormInput(
    'end',
    'icon-clock',
    'datetime-local',
    undefined,
    undefined,
    ['required', 'date', 'afterToday', 'dateCheck'])
]
