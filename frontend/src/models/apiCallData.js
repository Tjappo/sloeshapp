/**
 * Api Call Messages class
 */
export class ApiCallMessages {
  constructor (loadingMessage, successMessage, errorMessage) {
    this.loadingMessage = loadingMessage
    this.successMessage = successMessage
    this.errorMessage = errorMessage
  }
}

/**
 * Api Call Data class
 */
export class ApiCallData {
  constructor (apiCallMessages, route, params, successCallback, errorCallback) {
    this.apiCallMessages = apiCallMessages
    this.route = route
    this.params = params
    this.successCallback = successCallback
    this.errorCallback = errorCallback
  }

  loadingMessage () {
    if (ApiCallData._has(this.apiCallMessages.loadingMessage)) {
      VueEventListener.fire('toggleLoading', this.apiCallMessages.loadingMessage)
    }
  }

  successMessage () {
    if (ApiCallData._has(this.apiCallMessages.successMessage)) {
      VueEventListener.fire('successMessage', this.apiCallMessages.successMessage)
    }
  }

  errorMessage () {
    if (ApiCallData._has(this.apiCallMessages.errorMessage)) {
      VueEventListener.fire('errorMessage', this.apiCallMessages.errorMessage)
    }
  }

  successCallbackIfExists (data) {
    if (ApiCallData._has(this.successCallback)) {
      this.successCallback(data)
    }
  }

  errorCallbackIfExists (data) {
    if (ApiCallData._has(this.errorCallback)) {
      this.errorCallback(data)
    }
  }

  static _has (value) {
    return typeof value !== 'undefined'
  }
}
