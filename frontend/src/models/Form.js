export class Form {
  constructor (formData) {
    this.formData = formData
  }
}

export class FormInput {
  constructor (key, icon, type, placeholder, autocomplete, validations) {
    this.key = key
    this.icon = icon
    this.type = type
    this.placeholder = placeholder
    this.autocomplete = autocomplete || ''
    this.validations = validations || []
  }

  setValue (value) {
    this.value = value
  }

  initial () {
    return this.value || ''
  }
}

export class FormOptions {
  constructor (key, icon, options, validations) {
    this.key = key
    this.icon = icon
    this.options = options
    this.validations = validations
  }

  setValue (value) {
    this.value = value
  }

  initial () {
    return this.value || ''
  }
}

export class FormCheckbox {
  constructor (key, icon, label, unchecked, checked, validations) {
    this.key = key
    this.icon = icon
    this.label = label
    this.unchecked = unchecked
    this.checked = checked
    this.validations = validations
  }

  setValue (value) {
    this.value = value
  }

  initial () {
    return this.unchecked
  }
}

export class FormRadio {
  constructor (key, options, validations) {
    this.key = key
    this.options = options
    this.validations = validations
  }

  setValue (value) {
    this.value = value
  }

  initial () {
    return ''
  }
}

export class FormNumber {
  constructor (key, icon, min, max, step, validations) {
    this.key = key
    this.icon = icon
    this.min = min
    this.max = max
    this.step = step
    this.validations = []
  }

  initial () {
    return this.min.toString()
  }
}
