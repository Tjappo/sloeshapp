using sloesh_api.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace sloesh_api.CustomErrors
{
    public class UserAlreadyExistsObjectResult : BadRequestObjectResult
    {
        public UserAlreadyExistsObjectResult() :
            base(Constants.Strings.Errors.UserAlreadyExistsDescription)
        {
        }
    }
}