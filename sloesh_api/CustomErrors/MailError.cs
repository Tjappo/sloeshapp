using sloesh_api.Helpers;

namespace sloesh_api.CustomErrors
{
    public class MailError : Error
    {
        public MailError() : base(Constants.Strings.Errors.MailError,
            Constants.Strings.Errors.SomethingWentWrong)
        {
        }

        public MailError(string description) : base(Constants.Strings.Errors.MailError, description)
        {
        }
    }
}