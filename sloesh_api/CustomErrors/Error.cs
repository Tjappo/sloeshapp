﻿using Microsoft.AspNetCore.Identity;

namespace sloesh_api.CustomErrors
{
    public class Error
    {
        public Error(string code, string description)
        {
            Code = code;
            Description = description;
        }

        public Error(IdentityError identityError)
        {
            Code = identityError.Code;
            Description = identityError.Description;
        }

        public string Code { get; set; }
        public string Description { get; set; }
    }
}