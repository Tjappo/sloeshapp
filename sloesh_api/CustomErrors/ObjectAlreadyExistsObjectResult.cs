using sloesh_api.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace sloesh_api.CustomErrors
{
    public class ObjectAlreadyExistsObjectResult : NotFoundObjectResult
    {
        public ObjectAlreadyExistsObjectResult() :
            base(Constants.Strings.Errors.ObjectAlreadyExistsDescription)
        {
        }
    }
}