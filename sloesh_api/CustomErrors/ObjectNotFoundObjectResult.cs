using sloesh_api.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace sloesh_api.CustomErrors
{
    public class ObjectNotFoundObjectResult : NotFoundObjectResult
    {
        public ObjectNotFoundObjectResult() :
            base(Constants.Strings.Errors.ObjectNotFoundDescription)
        {
        }
    }
}