using sloesh_api.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace sloesh_api.CustomErrors
{
    public class UserNotFoundObjectResult : NotFoundObjectResult
    {
        public UserNotFoundObjectResult() :
            base(Constants.Strings.Errors.UserNotFoundDescription)
        {
        }
    }
}