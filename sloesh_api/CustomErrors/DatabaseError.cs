using sloesh_api.Helpers;

namespace sloesh_api.CustomErrors
{
    public class DatabaseError : Error
    {
        public DatabaseError() : base(Constants.Strings.Errors.DatabaseError,
            Constants.Strings.Errors.SomethingWentWrong)
        {
        }

        public DatabaseError(string description) : base(Constants.Strings.Errors.DatabaseError, description)
        {
        }
    }
}