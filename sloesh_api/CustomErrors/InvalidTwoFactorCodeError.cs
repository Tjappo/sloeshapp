using sloesh_api.Helpers;

namespace sloesh_api.CustomErrors
{
    public class InvalidTwoFactorCodeError : CustomIdentityError
    {
        public InvalidTwoFactorCodeError() :
            base(Constants.Strings.Errors.InvalidTwoFactorCode)
        {
        }
    }
}