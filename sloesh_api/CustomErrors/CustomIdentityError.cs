using sloesh_api.Helpers;

namespace sloesh_api.CustomErrors
{
    public class CustomIdentityError : Error
    {
        public CustomIdentityError() : base(Constants.Strings.Errors.IdentityError,
            Constants.Strings.Errors.SomethingWentWrong)
        {
        }

        public CustomIdentityError(string description) : base(Constants.Strings.Errors.IdentityError, description)
        {
        }
    }
}