﻿using sloesh_api.Models.User;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using sloesh_api.Models.Advertisements;
using sloesh_api.Models.Car;
using sloesh_api.Models.User.Customer;

namespace sloesh_api.Database
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        /// <inheritdoc />
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Admin>();

            modelBuilder.Entity<Customer>()
                .HasOne(u => u.AccountInfo)
                .WithOne(a => a.Customer)
                .HasForeignKey<AccountInfo>(a => a.CustomerId)
                .IsRequired();

            modelBuilder.Entity<Customer>()
                .HasOne(u => u.AddressInfo)
                .WithOne(a => a.Customer)
                .HasForeignKey<AddressInfo>(a => a.CustomerId)
                .IsRequired();

            modelBuilder.Entity<User>()
                .HasMany(u => u.Claims)
                .WithOne()
                .HasForeignKey(uc => uc.UserId)
                .IsRequired();

            modelBuilder.Entity<Customer>()
                .HasMany<Car>()
                .WithOne(c => c.Customer)
                .HasForeignKey(c => c.CustomerId)
                .IsRequired();

            modelBuilder.Entity<Advertisement>()
                .HasMany<HistoryAction>()
                .WithOne(h => h.Advertisement)
                .HasForeignKey(h => h.AdvertisementId)
                .IsRequired();

            modelBuilder.Entity<Admin>()
                .HasMany<HistoryAction>()
                .WithOne(h => h.Admin)
                .HasForeignKey(h => h.AdminId)
                .IsRequired();

            modelBuilder.Entity<CustomerAdvertisement>()
                .HasKey(ca => new {ca.CustomerId, ca.AdvertisementId, ca.CarId});

            modelBuilder.Entity<CustomerAdvertisement>()
                .HasOne(ca => ca.Customer)
                .WithMany(c => c.Advertisements)
                .HasForeignKey(ca => ca.CustomerId);
            
            modelBuilder.Entity<CustomerAdvertisement>()
                .HasOne(ca => ca.Car)
                .WithMany(c => c.CarAdvertisements)
                .HasForeignKey(ca => ca.CarId);

            modelBuilder.Entity<CustomerAdvertisement>()
                .HasOne(ca => ca.Advertisement)
                .WithMany(c => c.CarAdvertisements)
                .HasForeignKey(ca => ca.AdvertisementId)
                .IsRequired();

            modelBuilder.Entity<CarTypeCar>()
                .HasKey(ctc => new {ctc.CarTypeId, ctc.CarId});

            modelBuilder.Entity<CarTypeCar>()
                .HasOne(ctc => ctc.CarType)
                .WithMany(ct => ct.CarTypeCars)
                .HasForeignKey(ctc => ctc.CarTypeId)
                .IsRequired();

            modelBuilder.Entity<CarTypeCar>()
                .HasOne(ctc => ctc.Car)
                .WithMany(c => c.CarTypeCars)
                .HasForeignKey(ctc => ctc.CarId);
            
            modelBuilder.Entity<CarTypeAdvertisement>()
                .HasKey(ctc => new {ctc.CarTypeId, ctc.AdvertisementId});

            modelBuilder.Entity<CarTypeAdvertisement>()
                .HasOne(ctc => ctc.CarType)
                .WithMany(ct => ct.CarTypeAdvertisements)
                .HasForeignKey(ctc => ctc.CarTypeId)
                .IsRequired();

            modelBuilder.Entity<CarTypeAdvertisement>()
                .HasOne(ctc => ctc.Advertisement)
                .WithMany(c => c.CarTypeAdvertisements)
                .HasForeignKey(ctc => ctc.AdvertisementId);
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<CarType> CarTypes { get; set; }
        public DbSet<Advertisement> Advertisements { get; set; }
    }

    
    // @todo Fix the connection string
    public class ApplicationContextDbFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        ApplicationDbContext IDesignTimeDbContextFactory<ApplicationDbContext>.CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseNpgsql<ApplicationDbContext>(
                "Server=localhost;Port=5432;Database=Sloesh;User Id=blockriseserver;Password=##**3388QQPPaall;");

            return new ApplicationDbContext(optionsBuilder.Options);
        }
    }

/*
DROP Table public."IdentificationInfos", public."Ticket", public."Replies", public."AccountInfos", public."AddressInfos", public."AspNetUserClaims", public."AspNetUserLogins", public."AspNetRoleClaims", public."AspNetUserRoles", public."AspNetUserTokens", public."AspNetRoles", public."AspNetUsers", public."__EFMigrationsHistory", public."BankAccounts", public."Tiers"

*/
}