using System;
using System.Collections.Generic;
using sloesh_api.Models.Advertisements;

namespace sloesh_api.Models.User.Customer
{
    public class Customer : User
    {
        /// <summary>
        /// Personal info of the user
        /// </summary>
        public AccountInfo AccountInfo { get; set; }

        /// <summary>
        /// Address info of the user
        /// </summary>
        public AddressInfo AddressInfo { get; set; }

        /// <summary>
        /// Cars of the user
        /// </summary>
        public List<Car.Car> Cars { get; set; }

        /// <summary>
        /// List of advertisements the user has done
        /// </summary>
        public List<CustomerAdvertisement> Advertisements { get; set; }

        /// <summary>
        /// DateTime when the user has been created
        /// </summary>
        public DateTime CreatedAt { get; set; }
    }
}