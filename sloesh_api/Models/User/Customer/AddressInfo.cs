﻿using sloesh_api.Models.Enums.AccountInfo;

namespace sloesh_api.Models.User.Customer
{
    public class AddressInfo
    {
        /// <summary>
        /// Id of the user
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Customer it belongs to
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Foreign key of the customer it belongs to
        /// </summary>
        public string CustomerId { get; set; }
        
        /// <summary>
        /// Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Zipcode
        /// </summary>
        public string Zipcode { get; set; }
    }
}