﻿using System;
using sloesh_api.Models.Enums.AccountInfo;

namespace sloesh_api.Models.User.Customer
{
    /// <summary>
    /// Personal info
    /// </summary>
    public class AccountInfo
    {
        /// <summary>
        /// Id of the Account Info
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Customer it belongs to
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Foreign key of the customer it belongs to
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// First name of the user
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last name of the user
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Iban of the user
        /// </summary>
        public string IBAN { get; set; }

        /// <summary>
        /// DateTime the customer has been created
        /// </summary>
        public DateTime CreatedAt { get; set; }
    }
}