namespace sloesh_api.Models.User
{
    public class Admin : User
    {
        /// <summary>
        /// Name to display in the admin interface
        /// </summary>
        public string DisplayName { get; set; }
    }
}