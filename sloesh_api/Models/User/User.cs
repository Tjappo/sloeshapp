﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace sloesh_api.Models.User
{
    /// <inheritdoc />
    public class User : IdentityUser
    {
        /// <summary>
        /// Claims of the user
        /// </summary>
        public ICollection<IdentityUserClaim<string>> Claims { get; set; }
    }
}