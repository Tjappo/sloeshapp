﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace sloesh_api.Models.Enums.AccountInfo
{
    /// <summary>
    /// The exordiums a user can have
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Exordium
    {
        /// <summary>
        /// Undefined, if the field is invalid
        /// </summary>
        Undefined,

        /// <summary>
        /// Represents the male exordium
        /// </summary>
        Mr,

        /// <summary>
        /// Represents the female exordium
        /// </summary>
        Mrs
    }
}