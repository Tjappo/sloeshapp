using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace sloesh_api.Models.Enums.Advertisement
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Action
    {
        Create,
        Update,
        Delete
    }
}