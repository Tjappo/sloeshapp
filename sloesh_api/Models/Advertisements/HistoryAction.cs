using System;
using System.ComponentModel.DataAnnotations.Schema;
using sloesh_api.Models.User;
using Action = sloesh_api.Models.Enums.Advertisement.Action;

namespace sloesh_api.Models.Advertisements
{
    public class HistoryAction
    {
        /// <summary>
        /// Id of the history action
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Admin that created it
        /// </summary>
        public Admin Admin { get; set; }

        /// <summary>
        /// The foreign key of the admin
        /// </summary>
        public string AdminId { get; set; }

        /// <summary>
        /// Advertisement that created it
        /// </summary>
        public Advertisement Advertisement { get; set; }

        /// <summary>
        /// The foreign key of the advertisement
        /// </summary>
        public int AdvertisementId { get; set; }

        /// <summary>
        /// Enum containing the action
        /// </summary>
        public Action Action { get; set; }

        /// <summary>
        /// Date when the action has been executed
        /// </summary>
        public DateTime CreatedAt { get; set; }
    }
}