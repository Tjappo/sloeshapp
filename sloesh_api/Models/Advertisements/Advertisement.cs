using System;
using System.Collections.Generic;
using sloesh_api.Models.Car;

namespace sloesh_api.Models.Advertisements
{
    public class Advertisement
    {
        /// <summary>
        /// Id of the advertisement
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Title of the advertisement
        /// </summary>
        public string Title { get; set; }
        
        /// <summary>
        /// Logo of the advertisement
        /// </summary>
        public string Logo { get; set; }

        /// <summary>
        /// Image of the advertisement
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// Compensation of the advertisement
        /// </summary>
        public float Compensation { get; set; }

        /// <summary>
        /// Duration in weeks
        /// </summary>
        public int Duration { get; set; }

        /// <summary>
        /// Start date of the advertisement
        /// </summary>
        public DateTime Start { get; set; }

        /// <summary>
        /// End date of the advertisement
        /// </summary>
        public DateTime End { get; set; }
        
        /// <summary>
        /// List of car types the advertisement supports
        /// </summary>
        public List<CarTypeAdvertisement> CarTypeAdvertisements { get; set; }

        /// <summary>
        /// History actions of the advertisement
        /// </summary>
        public List<HistoryAction> History { get; set; }

        /// <summary>
        /// List of cars the advertisement has
        /// </summary>
        public List<CustomerAdvertisement> CarAdvertisements { get; set; }
    }
}