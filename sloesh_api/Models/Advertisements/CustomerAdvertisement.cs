using System;
using sloesh_api.Models.User.Customer;

namespace sloesh_api.Models.Advertisements
{
    public class CustomerAdvertisement
    {
        public string CustomerId { get; set; }
        public Customer Customer { get; set; }

        public int AdvertisementId { get; set; }
        public Advertisement Advertisement { get; set; }
        
        public int CarId { get; set; }
        public Car.Car Car { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}