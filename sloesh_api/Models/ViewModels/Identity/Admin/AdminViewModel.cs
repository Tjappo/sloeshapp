﻿namespace sloesh_api.Models.ViewModels.Identity.Admin
{
    public class AdminViewModel : IsLockedOutViewModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string DisplayName { get; set; }
    }
}