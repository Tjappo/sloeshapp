﻿using System.Collections.Generic;
using sloesh_api.Models.ViewModels.Car;
using sloesh_api.Models.ViewModels.Identity.Customer;

namespace sloesh_api.Models.ViewModels.Identity.Admin
{
    public class CustomerAdminViewModel : CustomerViewModel
    {
        public List<CarViewModel> Cars { get; set; }
        public string UserId { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool IsLockedOut { get; set; }
    }
}