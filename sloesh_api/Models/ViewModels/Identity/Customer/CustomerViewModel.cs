﻿using System;
using sloesh_api.Models.Enums.AccountInfo;

namespace sloesh_api.Models.ViewModels.Identity.Customer
{
    public class CustomerViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public string Address { get; set; }
        public string Zipcode { get; set; }

        public string IBAN { get; set; }

        public DateTime ActiveSince { get; set; }
    }
}