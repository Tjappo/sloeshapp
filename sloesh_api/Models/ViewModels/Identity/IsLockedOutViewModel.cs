﻿namespace sloesh_api.Models.ViewModels.Identity
{
    public abstract class IsLockedOutViewModel
    {
        public bool IsLockedOut { get; set; }
    }
}