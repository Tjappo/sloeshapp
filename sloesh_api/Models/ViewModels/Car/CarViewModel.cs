using System.Collections.Generic;

namespace sloesh_api.Models.ViewModels.Car
{
    public class CarViewModel
    {
        public string LicensePlate { get; set; }

        public List<CarTypeViewModel> CarTypeViewModels { get; set; }
    }
}