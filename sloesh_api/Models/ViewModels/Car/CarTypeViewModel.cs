namespace sloesh_api.Models.ViewModels.Car
{
    public class CarTypeViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}