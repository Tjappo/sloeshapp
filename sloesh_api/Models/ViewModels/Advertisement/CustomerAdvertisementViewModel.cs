using System;

namespace sloesh_api.Models.ViewModels.Advertisement
{
    public class CustomerAdvertisementViewModel
    {
        public string UserName { get; set; }

        public string LicensePlate { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}