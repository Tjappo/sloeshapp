using System.Collections.Generic;
using sloesh_api.Models.ViewModels.Car;

namespace sloesh_api.Models.ViewModels.Advertisement
{
    public class AdvertisementOverviewViewModel
    {
        public List<AdvertisementViewModel> UserAdvertisements { get; set; }
        public List<AdvertisementViewModel> Advertisements { get; set; }
        public List<CarViewModel> Cars { get; set; }
    }
}