using System.Collections.Generic;

namespace sloesh_api.Models.ViewModels.Advertisement
{
    public class AdvertisementDetailViewModel : AdvertisementViewModel
    {
        public IEnumerable<HistoryActionViewModel> History { get; set; }
        public int SubscribedCars { get; set; }
    }
}