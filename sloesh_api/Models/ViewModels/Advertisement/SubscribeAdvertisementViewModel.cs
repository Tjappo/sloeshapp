using System.Collections.Generic;
using sloesh_api.Models.ViewModels.Car;
using sloesh_api.Models.ViewModels.Identity.Customer;

namespace sloesh_api.Models.ViewModels.Advertisement
{
    public class SubscribeAdvertisementViewModel
    {
        public CustomerViewModel CustomerInformation { get; set; }
        public List<CarViewModel> Cars { get; set; }
    }
}