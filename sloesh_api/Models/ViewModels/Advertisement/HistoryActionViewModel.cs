using System;
using Action = sloesh_api.Models.Enums.Advertisement.Action;

namespace sloesh_api.Models.ViewModels.Advertisement
{
    public class HistoryActionViewModel
    {
        public string DisplayName { get; set; }
        public Action Action { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}