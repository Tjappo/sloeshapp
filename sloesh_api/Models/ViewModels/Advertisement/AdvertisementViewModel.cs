using System;
using System.Collections.Generic;
using sloesh_api.Models.ViewModels.Car;

namespace sloesh_api.Models.ViewModels.Advertisement
{
    public class AdvertisementViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Logo { get; set; }
        public string Image { get; set; }
        public decimal Compensation { get; set; }
        public int Duration { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        
        public List<CarTypeViewModel> CarTypes { get; set; }
    }
}