﻿using System.Collections.Generic;

namespace sloesh_api.Models.Car
{
    /// <summary>
    /// The car
    /// </summary>
    public class CarType
    {
        /// <summary>
        /// Id of the Account Info
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the car type
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The description of the car type
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// List of cars the car type has
        /// </summary>
        public ICollection<CarTypeCar> CarTypeCars { get; set; }
        
        /// <summary>
        /// List of advertisements the car type supports
        /// </summary>
        public ICollection<CarTypeAdvertisement> CarTypeAdvertisements { get; set; }
    }
}