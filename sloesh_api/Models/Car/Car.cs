﻿using System.Collections.Generic;
using sloesh_api.Models.Advertisements;
using sloesh_api.Models.User.Customer;

namespace sloesh_api.Models.Car
{
    /// <summary>
    /// The car
    /// </summary>
    public class Car
    {
        /// <summary>
        /// Id of the Account Info
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Customer it belongs to
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Foreign key of the customer it belongs to
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// License plate of the car
        /// </summary>
        public string LicensePlate { get; set; }

        /// <summary>
        /// List of car types the car has
        /// </summary>
        public ICollection<CarTypeCar> CarTypeCars { get; set; }
        
        /// <summary>
        /// List of advertisements the car has
        /// </summary>
        public ICollection<CustomerAdvertisement> CarAdvertisements { get; set; }
    }
}