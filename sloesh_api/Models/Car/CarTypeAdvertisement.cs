using sloesh_api.Models.Advertisements;

namespace sloesh_api.Models.Car
{
    public class CarTypeAdvertisement
    {
        public int CarTypeId { get; set; }
        public CarType CarType { get; set; }

        public int AdvertisementId { get; set; }
        public Advertisement Advertisement { get; set; }
    }
}