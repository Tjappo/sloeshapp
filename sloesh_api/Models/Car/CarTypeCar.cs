namespace sloesh_api.Models.Car
{
    public class CarTypeCar
    {
        public int CarTypeId { get; set; }
        public CarType CarType { get; set; }

        public int CarId { get; set; }
        public Car Car { get; set; }
    }
}