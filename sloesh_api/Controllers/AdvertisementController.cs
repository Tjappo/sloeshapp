﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using sloesh_api.CustomErrors;
using sloesh_api.Helpers;
using sloesh_api.Models.User;
using sloesh_api.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using sloesh_api.Dtos.Advertisement;
using sloesh_api.Dtos.User.Identity;
using sloesh_api.Models.Advertisements;
using sloesh_api.Models.Car;
using sloesh_api.Models.User.Customer;
using sloesh_api.Models.ViewModels.Advertisement;
using sloesh_api.Models.ViewModels.Car;

namespace sloesh_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AdvertisementController : AdminControllerBase
    {
        private readonly IAdminFetchService _adminFetchService;
        private readonly IAdvertisementService _advertisementService;
        private readonly ICustomerFetchService _customerFetchService;
        private readonly ICarTypeService _carTypeService;
        private readonly IMapper _mapper;

        public AdvertisementController(
            IAdminFetchService adminFetchService,
            IAdvertisementService advertisementService,
            ICustomerFetchService customerFetchService,
            ICarTypeService carTypeService,
            UserManager<User> userManager,
            IMapper mapper
        ) : base(userManager)
        {
            _adminFetchService = adminFetchService;
            _advertisementService = advertisementService;
            _customerFetchService = customerFetchService;
            _carTypeService = carTypeService;
            _mapper = mapper;
        }

        #region Customer
        
        /// <summary>
        /// Gets newest available advertisements
        /// </summary>
        ///
        /// <remarks>
        /// GET api/advertisement/GetAvailableAdvertisementsNoValidation
        /// At least one week before end date
        /// Used by an unregistered or api user
        /// Limited by 4
        /// </remarks>
        /// 
        /// <returns>
        ///     <see langword="Ok"/> with a <see langword="List"/> of <see langword="Advertisement"/> if successful,
        ///     <see langword="BadRequest"/> if fails
        /// </returns>
        [HttpGet(nameof(GetNewestAvailableAdvertisements))]
        public async Task<IActionResult> GetNewestAvailableAdvertisements()
        {
            var advertisements = await _advertisementService.GetNewestAvailableAdvertisements();
            var advertisementViewModels =
                _mapper.Map<List<Advertisement>, List<AdvertisementViewModel>>(advertisements);

            return Ok(advertisementViewModels);
        }

        /// <summary>
        /// Gets all available advertisements
        /// </summary>
        ///
        /// <remarks>
        /// GET api/advertisement/GetAvailableAdvertisementsNoValidation
        /// At least one week before end date
        /// Used by an unregistered user
        /// </remarks>
        ///
        /// <param name="fetchAdvertisementsDto"><see langword="FetchAdvertisementsDto"/> containing the parameters to
        /// fetch the advertisements</param>
        /// 
        /// <returns>
        ///     <see langword="Ok"/> with a <see langword="List"/> of <see langword="Advertisement"/> if successful,
        ///     <see langword="BadRequest"/> if fails
        /// </returns>
        [HttpGet(nameof(GetAvailableAdvertisementsNoValidation))]
        public async Task<IActionResult> GetAvailableAdvertisementsNoValidation(
            [FromQuery] FetchAdvertisementsDto fetchAdvertisementsDto)
        {
            // pass postal code and license

            var advertisements = await _advertisementService.GetAllAvailableAdvertisements();
            var advertisementViewModels =
                _mapper.Map<List<Advertisement>, List<AdvertisementViewModel>>(advertisements);

            return Ok(advertisementViewModels);
        }

        /// <summary>
        /// Gets all available advertisements
        /// </summary>
        ///
        /// <remarks>
        /// GET api/advertisement/GetAvailableAdvertisements
        /// At least one week before end date
        /// Used by a registered user
        /// </remarks>
        /// 
        /// <returns>
        ///     <see langword="Ok"/> with a <see langword="List"/> of <see langword="Advertisement"/> if successful,
        ///     <see langword="BadRequest"/> if fails
        /// </returns>
        [Authorize(Policy = Constants.Strings.JwtPolicies.UnvalidatedUser)]
        [HttpGet(nameof(GetAvailableAdvertisements))]
        public async Task<IActionResult> GetAvailableAdvertisements([FromQuery] UserIdDto userIdDto)
        {
            var customer =
                await _customerFetchService.FindByIdWithCarsAdvertisementsAsync(userIdDto.UserId);

            if (customer == null)
                return new UserNotFoundObjectResult();

            // pass postal code and license
            
            var advertisementOverviewViewModel = new AdvertisementOverviewViewModel();

            var advertisements = await _advertisementService.GetAllAvailableAdvertisements();

            var filteredAdvertisements = FilterAdvertisementsForCustomer(advertisements, customer.Cars);

            advertisementOverviewViewModel.Advertisements = 
                _mapper.Map<List<Advertisement>, List<AdvertisementViewModel>>(advertisements);
            advertisementOverviewViewModel.UserAdvertisements = 
                _mapper.Map<List<Advertisement>, List<AdvertisementViewModel>>(filteredAdvertisements);
            advertisementOverviewViewModel.Cars = _mapper.Map<ICollection<Car>, List<CarViewModel>>(customer.Cars);

            return Ok(advertisementOverviewViewModel);
        }

        /// <summary>
        /// Subscribes a customer to an advertisement
        /// </summary>
        ///
        /// <remarks>
        /// POST api/advertisement/SubscribeAdvertisement
        /// </remarks>
        /// 
        /// <param name="subscribeAdvertisementDto"><see langword="SubscribeAdvertisementDto"/> containing
        /// the parameters to subscribe the customer to the advertisement</param>
        /// <returns>
        ///     <see langword="Ok"/> if succeeded,
        ///     <see langword="NotFound"/> if not exists,
        ///     <see langword="BadRequest"/> if failed during validation
        /// </returns>
        [Authorize(Policy = Constants.Strings.JwtPolicies.ApiUser)]
        [HttpPost(nameof(SubscribeAdvertisement))]
        public async Task<IActionResult> SubscribeAdvertisement(
            [FromBody] SubscribeAdvertisementDto subscribeAdvertisementDto)
        {
            var customer =
                await _customerFetchService.FindByIdWithCarsAdvertisementsAsync(subscribeAdvertisementDto.UserId);

            var advertisement = await _advertisementService.FindByIdAsync(subscribeAdvertisementDto.AdvertisementId);

            foreach (var licensePlate in subscribeAdvertisementDto.Cars)
            {
                var car = customer.Cars.FirstOrDefault(c =>
                    string.Equals(c.LicensePlate, licensePlate));

                if (ValidateSubscribeAdvertisement(customer, car, advertisement) is IActionResult validationError)
                    return validationError;

                if (await _advertisementService.SubscribeAdvertisement(customer, car, advertisement) is Error error)
                    return BadRequest(Errors.AddErrorToModelState(error, ModelState));
            }

            return Ok();
        }

        #endregion

        #region Admin

        /// <summary>
        /// Gets all advertisements
        /// </summary>
        ///
        /// <remarks>GET api/advertisement/GetAdvertisements</remarks>
        /// 
        /// <param name="userIdDto"><see langword="UserIdDto"/> containing the user id</param>
        /// <returns>
        ///     <see langword="Ok"/> with a <see langword="List"/> of <see langword="Advertisement"/> if successful,
        ///     <see langword="BadRequest"/> if fails
        /// </returns>
        [Authorize(Policy = Constants.Strings.JwtPolicies.AdminUser)]
        [HttpGet(nameof(GetAdvertisements))]
        public async Task<IActionResult> GetAdvertisements([FromQuery] UserIdDto userIdDto)
        {
            if (ValidateAdmin(await _adminFetchService.FindByIdWithClaimsAsync(userIdDto.UserId)) is
                IActionResult validationError)
                return validationError;

            var advertisements = await _advertisementService.GetAllAdvertisements();
            var advertisementViewModels =
                _mapper.Map<List<Advertisement>, List<AdvertisementViewModel>>(advertisements);

            return Ok(advertisementViewModels);
        }

        /// <summary>
        /// Gets the advertisement in detail
        /// </summary>
        ///
        /// <remarks>GET api/advertisement/GetAdvertisement</remarks>
        /// 
        /// <param name="advertisementDto"><see langword="AdvertisementIdDto"/> containing the user id and advertisement id</param>
        /// <returns>
        ///     <see langword="Ok"/> if succeeded,
        ///     <see langword="NotFound"/> if not exists,
        ///     <see langword="BadRequest"/> if failed during validation
        /// </returns>
        [Authorize(Policy = Constants.Strings.JwtPolicies.AdminUser)]
        [HttpGet(nameof(GetAdvertisement))]
        public async Task<IActionResult> GetAdvertisement([FromQuery] AdvertisementIdDto advertisementDto)
        {
            if (ValidateAdmin(await _adminFetchService.FindByIdWithClaimsAsync(advertisementDto.UserId)) is
                IActionResult validationError)
                return validationError;

            if (await _advertisementService.FindByIdWithRelationsAsync(advertisementDto.Id) is Advertisement
                advertisement)
                return Ok(_mapper.Map<Advertisement, AdvertisementDetailViewModel>(advertisement));

            return NotFound();
        }

        /// <summary>
        /// Creates an advertisement
        /// </summary>
        ///
        /// <remarks>POST api/advertisement/CreateAdvertisement</remarks>
        /// 
        /// <param name="advertisementDto"><see langword="AdvertisementDto"/> containing the parameters to create
        /// the advertisement</param>
        /// <returns><see langword="Ok"/> if successful, otherwise a <see langword="BadRequest"/></returns>
        [Authorize(Policy = Constants.Strings.JwtPolicies.AdminUser)]
        [HttpPost(nameof(CreateAdvertisement))]
        public async Task<IActionResult> CreateAdvertisement([FromBody] AdvertisementDto advertisementDto)
        {
            var admin = await _adminFetchService.FindByIdWithClaimsAsync(advertisementDto.UserId);
            if (ValidateAdmin(admin) is IActionResult validationError)
                return validationError;

            if (advertisementDto.Start > advertisementDto.End)
                return BadRequest("End date must be later than start date");

            var advertisement = _mapper.Map<AdvertisementDto, Advertisement>(advertisementDto);

            if (!await AddCarTypeByIds(advertisementDto.CarTypes, advertisement))
                return NotFound();

            if (await _advertisementService.CreateAdvertisement(admin, advertisement) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));

            return Ok("Advertisement created");
        }

        /// <summary>
        /// Updates the advertisement
        /// </summary>
        ///
        /// <remarks>PUT api/advertisement/UpdateAdvertisement</remarks>
        /// 
        /// <param name="advertisementDto"><see langword="AdvertisementUpdateDto"/> containing the parameters to update
        /// the advertisement</param>
        /// <returns>
        ///     <see langword="Ok"/> if succeeded,
        ///     <see langword="NotFound"/> if not exists,
        ///     <see langword="BadRequest"/> if failed during updating
        /// </returns>
        [Authorize(Policy = Constants.Strings.JwtPolicies.AdminUser)]
        [HttpPut(nameof(UpdateAdvertisement))]
        public async Task<IActionResult> UpdateAdvertisement([FromBody] AdvertisementUpdateDto advertisementDto)
        {
            var admin = await _adminFetchService.FindByIdWithClaimsAsync(advertisementDto.UserId);
            if (ValidateAdmin(admin) is IActionResult validationError)
                return validationError;

            var advertisement = await _advertisementService.FindByIdWithRelationsAsync(advertisementDto.Id);
            if (advertisement == null)
                return NotFound();

            if (!await UpdateCarTypesByIds(advertisementDto.CarTypes, advertisement))
                return NotFound();

            if (await _advertisementService.UpdateAdvertisement(admin, advertisement, advertisementDto) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));

            return Ok("Advertisement updated");
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Filters the advertisements for the customer
        /// </summary>
        /// <param name="advertisements"><see langword="List"/> of <see langword="Advertisement"/> to filter.</param>
        /// <param name="cars"><see langword="List"/> of <see langword="Car"/> to filter it for.</param>
        /// <returns><see langword="List"/> of filtered <see langword="Advertisement"/>.</returns>
        private static List<Advertisement> FilterAdvertisementsForCustomer(List<Advertisement> advertisements,
            IReadOnlyCollection<Car> cars)
        {
            for (var i = 0; i < advertisements.Count; ++i)
            {
                if (!advertisements[i].CarTypeAdvertisements.Any(cta =>
                    cars.Any(c => c.CarTypeCars.Any(ctc => ctc.CarTypeId == cta.CarTypeId))))
                {
                    advertisements.RemoveAt(i);
                }
            }

            return advertisements;
        }

        /// <summary>
        /// Validates the parameters when subscribing to an advertisement
        /// </summary>
        /// <param name="customer"><see langword="Customer"/> that wants to subscribe.</param>
        /// <param name="car"><see langword="Car"/> that is subscribing to the advertisement.</param>
        /// <param name="advertisement"><see langword="Advertisement"/> to subscribe to.</param>
        /// <returns><see langword="null"/> if successful, else <see langword="IActionResult"/></returns>
        private IActionResult ValidateSubscribeAdvertisement(Customer customer, Car car, Advertisement advertisement)
        {
            if (customer == null || car == null || advertisement == null)
                return NotFound();

            if (!car.CarTypeCars
                .Any(ctc =>
                    advertisement.CarTypeAdvertisements
                        .Select(cta =>
                            cta.CarTypeId
                        )
                        .Contains(ctc.CarTypeId)))
                return BadRequest("Car is not eligible for the advertisement");

            return customer.Advertisements.Any(ca =>
                ca.AdvertisementId == advertisement.Id && ca.CarId == car.Id)
                ? BadRequest("Already subscribed to the advertisement")
                : null;
        }


        /// <summary>
        /// Updates the car types of the advertisement with the given list
        /// </summary>
        /// <param name="carTypeIds"><see langword="List"/> of <see langword="int"/> of ids of the car types.</param>
        /// <param name="advertisement"><see langword="Advertisement"/> to update.</param>
        /// <returns><see langword="true"/> if successful, else <see langword="false"/>.</returns>
        private async Task<bool> UpdateCarTypesByIds(ICollection<int> carTypeIds, Advertisement advertisement)
        {
            var toAdd = RemoveCarTypeByIds(carTypeIds, advertisement);

            return await AddCarTypeByIds(toAdd, advertisement);
        }

        /// <summary>
        /// Removes the list of car type ids from the advertisement
        /// </summary>
        /// <param name="carTypeIds"><see langword="List"/> of <see langword="int"/> of ids of the car types.</param>
        /// <param name="advertisement"><see langword="Advertisement"/> to remove it from.</param>
        /// <returns><see langword="List"/> of <see langword="int"/> ids of the car types to add.</returns>
        private static IEnumerable<int> RemoveCarTypeByIds(ICollection<int> carTypeIds, Advertisement advertisement)
        {
            var toAdd = carTypeIds.ToList();
            for (var i = advertisement.CarTypeAdvertisements.Count - 1; i >= 0; --i)
            {
                if (!carTypeIds.Contains(advertisement.CarTypeAdvertisements[i].CarTypeId))
                {
                    advertisement.CarTypeAdvertisements.RemoveAt(i);
                    continue;
                }

                toAdd.RemoveAt(i);
            }

            return toAdd;
        }

        /// <summary>
        /// Adds the list of car type ids to the advertisement
        /// </summary>
        /// <param name="carTypeIds"><see langword="List"/> of <see langword="int"/> of ids of the car types.</param>
        /// <param name="advertisement"><see langword="Advertisement"/> to add it to.</param>
        /// <returns><see langword="true"/> if successful, else <see langword="false"/>.</returns>
        private async Task<bool> AddCarTypeByIds(IEnumerable<int> carTypeIds, Advertisement advertisement)
        {
            foreach (var carTypeId in carTypeIds)
            {
                var carType = await _carTypeService.GetCarTypeById(carTypeId);

                if (carType == null)
                {
                    return false;
                }

                advertisement.CarTypeAdvertisements.Add(new CarTypeAdvertisement
                {
                    Advertisement = advertisement,
                    AdvertisementId = advertisement.Id,
                    CarTypeId = carTypeId,
                    CarType = carType
                });
            }

            return true;
        }

        #endregion
    }
}