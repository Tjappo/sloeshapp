﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using sloesh_api.Auth;
using sloesh_api.CustomErrors;
using sloesh_api.Helpers;
using sloesh_api.Models;
using sloesh_api.Dtos.User.Identity;
using sloesh_api.Models.User;
using sloesh_api.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace sloesh_api.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly IJwtFactory _jwtFactory;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly IAdminFetchService _adminFetchService;
        private readonly ICustomerFetchService _customerFetchService;
        private string _displayName;

        public AuthController(
            UserManager<User> userManager,
            IJwtFactory jwtFactory,
            IOptions<JwtIssuerOptions> jwtOptions,
            IAdminFetchService adminFetchService,
            ICustomerFetchService customerFetchService
        )
        {
            _userManager = userManager;
            _jwtFactory = jwtFactory;
            _jwtOptions = jwtOptions.Value;
            _adminFetchService = adminFetchService;
            _customerFetchService = customerFetchService;
        }

        /// <summary>Logs the user in.</summary>
        ///
        /// <remarks>POST api/auth/login</remarks>
        ///
        /// <param name="credentials">The user credentials <see langword="UserDto"/> from the request body.</param>
        ///
        /// <returns>Json object of a jwt token if successful, else a <see langword="BadRequest"/></returns>
        [HttpPost(nameof(Login))]
        public async Task<IActionResult> Login([FromBody] LoginDto credentials)
        {
            var identity = await GetClaimsIdentity(credentials);

            if (identity == null)
                return BadRequest(ModelState);

            var jwt = await Tokens.GenerateJwt(identity, _jwtFactory, credentials.UserName, _displayName,
                _jwtOptions, new JsonSerializerSettings {Formatting = Formatting.Indented});
            return new OkObjectResult(jwt);
        }

        #region Helpers

        /// <summary>Validates the credentials with the user in the database.</summary>
        ///
        /// <param name="credentials">The credentials <see langword="LoginDto"/> to verify.</param>
        ///
        /// <returns><see langword="ClaimsIdentity"/> if successful, else a <see langword="null"/></returns>
        private async Task<ClaimsIdentity> GetClaimsIdentity(LoginDto credentials)
        {
            var userToVerify = await FindUser(credentials);

            if (userToVerify == null)
                return await Task.FromResult<ClaimsIdentity>(null);

            if (await ValidateUser(userToVerify))
                return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(credentials.UserName, userToVerify.Id,
                    GetRole(userToVerify)));

            return await Task.FromResult<ClaimsIdentity>(null);
        }

        /// <summary>Validates the user, that wants to login.</summary>
        ///
        /// <param name="userToVerify">The user <see langword="User"/> to verify.</param>
        /// 
        /// <returns><see langword="Boolean"/> true if successful, else a <see langword="Boolean"/> false</returns>
        private async Task<bool> ValidateUser(User userToVerify)
        {
            var role = GetRole(userToVerify);

            if (await _userManager.IsLockedOutAsync(userToVerify))
            {
                Errors.AddErrorToModelState(new CustomIdentityError("User locked out."), ModelState);
                return await Task.FromResult(false);
            }

            if (!string.Equals(role, Constants.Strings.JwtClaims.AdminAccess) && 
                !await _userManager.IsEmailConfirmedAsync(userToVerify))
            {
                Errors.AddErrorToModelState(new CustomIdentityError("Email not confirmed."), ModelState);
                return await Task.FromResult(false);
            }

            await _userManager.ResetAccessFailedCountAsync(userToVerify);
            return await Task.FromResult(true);
        }

        /// <summary>Finds the user in the database.</summary>
        ///
        /// <param name="credentials">The credentials <see langword="LoginDto"/> to verify.</param>
        ///
        /// <returns><see langword="User"/> if successful, else a <see langword="null"/></returns>
        private async Task<User> FindUser(LoginDto credentials)
        {
            var userToVerify = await GetUser(credentials.UserName);

            if (userToVerify != null)
            {
                if (await _userManager.CheckPasswordAsync(userToVerify, credentials.Password))
                    return userToVerify;

                await _userManager.AccessFailedAsync(userToVerify);
                Errors.AddErrorToModelState(Constants.Strings.Errors.InvalidAccessAttempts,
                    (await _userManager.GetAccessFailedCountAsync(userToVerify)).ToString(), ModelState);
            }

            Errors.AddErrorToModelState(new CustomIdentityError("Invalid username or password."), ModelState);
            return await Task.FromResult<User>(null);
        }

        /// <summary>
        /// Finds an admin or customer given a username
        /// </summary>
        /// <param name="userName"><see langword="string"/> userName to search by</param>
        /// <returns><see langword="User"/> found user, <see langword="null"/> if not found</returns>
        private async Task<User> GetUser(string userName)
        {
            var userToVerify = await _adminFetchService.FindByNameWithClaimsAsync(userName);

            if (userToVerify == null) return await _customerFetchService.FindByNameWithClaimsAsync(userName);

            _displayName = userToVerify.DisplayName;
            return userToVerify;
        }

        /// <summary> Gets the role of the user </summary>
        ///
        /// <param name="user">The user <see langword="User"/> to get the role from.</param>
        ///
        /// <returns><see langword="string"/> role if successful, else a <see langword="null"/></returns>
        private static string GetRole(User user)
        {
            return user.Claims.First(claim =>
                    string.Equals(claim.ClaimType, Constants.Strings.JwtClaimIdentifiers.Roles))
                ?.ClaimValue;
        }

        #endregion
    }
}