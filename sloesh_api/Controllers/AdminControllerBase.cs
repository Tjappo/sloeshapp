﻿using sloesh_api.Helpers;
using sloesh_api.Models.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace sloesh_api.Controllers
{
    public abstract class AdminControllerBase : ControllerBase
    {
        protected readonly UserManager<User> UserManager;

        protected AdminControllerBase(
            UserManager<User> userManager
        )
        {
            UserManager = userManager;
        }

        /// <summary>Validates the admin access role.</summary>
        /// <param name="user">The <see langword="User"/> to verify</param>
        /// <returns><see langword="bool"/> whether the role is admin access.</returns>
        protected static bool ValidateAdminAccess(User user)
        {
            return Constants.Functions.HelperMethods.ValidateAccess(user, Constants.Strings.JwtClaims.AdminAccess);
        }

        /// <summary>Validates the user access role.</summary>
        /// <param name="user">The <see langword="User"/> to verify</param>
        /// <returns><see langword="bool"/> whether the role is user access.</returns>
        protected static bool ValidateUserAccess(User user)
        {
            return Constants.Functions.HelperMethods.ValidateAccess(user, Constants.Strings.JwtClaims.ApiAccess);
        }

        /// <summary>Validates the unvalidated access role.</summary>
        /// <param name="user">The <see langword="User"/> to verify</param>
        /// <returns><see langword="bool"/> whether the role is unvalidated access.</returns>
        protected static bool ValidateUnvalidatedAccess(User user)
        {
            return Constants.Functions.HelperMethods.ValidateAccess(user,
                Constants.Strings.JwtClaims.UnvalidatedAccess);
        }

        /// <summary>
        /// Validates an admin given a user id
        /// </summary>
        /// <param name="user">Given <see langword="User"/> user to validate.</param>
        /// 
        /// <returns><see langword="null"/> if successful, else <see langword="IActionResult"/></returns>
        protected IActionResult ValidateAdmin(User user)
        {
            if (user == null || !ValidateAdminAccess(user))
                return Unauthorized();

            return null;
        }
    }
}