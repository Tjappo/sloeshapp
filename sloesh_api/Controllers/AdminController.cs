﻿using System.Threading.Tasks;
using sloesh_api.CustomErrors;
using sloesh_api.Dtos.User.Admin;
using sloesh_api.Helpers;
using sloesh_api.Models.User;
using sloesh_api.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using sloesh_api.Dtos.Admin;

namespace sloesh_api.Controllers
{
    [Authorize(Policy = Constants.Strings.JwtPolicies.AdminUser)]
    [ApiController]
    [Route("api/[controller]")]
    public class AdminController : AdminControllerBase
    {
        private readonly ICustomerFetchService _customerFetchService;
        private readonly ICustomerUpdateService _customerUpdateService;
        private readonly IAdminFetchService _adminFetchService;
        private readonly IAdminUpdateService _adminUpdateService;

        public AdminController(
            ICustomerFetchService customerFetchService,
            ICustomerUpdateService customerUpdateService,
            IAdminFetchService adminFetchService,
            IAdminUpdateService adminUpdateService,
            UserManager<User> userManager
        ) : base(userManager)
        {
            _customerFetchService = customerFetchService;
            _customerUpdateService = customerUpdateService;
            _adminFetchService = adminFetchService;
            _adminUpdateService = adminUpdateService;
        }

        #region Customers

        /// <summary>
        /// Updates the customer
        /// </summary>
        ///
        /// <remarks>PUT api/admin/UpdateUser</remarks>
        /// 
        /// <param name="customerAdminUpdateDto"><see langword="UserAdminUpdateDto"/> containing required properties to update the user</param>
        /// <returns><see langword="Ok"/> if successful, otherwise a <see langword="BadRequest"/></returns>
        [HttpPut(nameof(UpdateUser))]
        public async Task<IActionResult> UpdateUser([FromBody] CustomerAdminUpdateDto customerAdminUpdateDto)
        {
            if (ValidateAdmin(await _adminFetchService.FindByIdWithClaimsAsync(customerAdminUpdateDto.AdminId)) is
                IActionResult validationError)
                return validationError;

            var userToEdit = await _customerFetchService.FindByIdWithRelationsAsync(customerAdminUpdateDto.UserId);

            if (userToEdit == null || !(ValidateUserAccess(userToEdit) || ValidateUnvalidatedAccess(userToEdit)))
                return new UserNotFoundObjectResult();

            if (await _customerUpdateService.UpdateCustomerAdmin(userToEdit, customerAdminUpdateDto) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));

            return Ok("User Updated");
        }

        #endregion

        #region Admin

        /// <summary>
        /// Creates an admin
        /// </summary>
        ///
        /// <remarks>POST api/admin/CreateAdmin</remarks>
        /// 
        /// <param name="createAdminDto"><see langword="CreateAdminDto"/> containing required properties to create the admin</param>
        /// <returns><see langword="Ok"/> if successful, otherwise a <see langword="BadRequest"/></returns>
        [HttpPost(nameof(CreateAdmin))]
        public async Task<IActionResult> CreateAdmin([FromBody] CreateAdminDto createAdminDto)
        {
            var adminToEdit = await _adminFetchService.FindByIdWithClaimsAsync(createAdminDto.UserId);

            if (adminToEdit == null || !ValidateAdminAccess(adminToEdit))
                return Unauthorized();
            
            if (await UserManager.FindByNameAsync(createAdminDto.UserName) != null)
                return new UserAlreadyExistsObjectResult();

            if (await _adminUpdateService.CreateAdmin(createAdminDto) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));

            return Ok();
        }

        /// <summary>
        /// Updates the admin
        /// </summary>
        ///
        /// <remarks>PUT api/admin/CreateAdmin</remarks>
        /// 
        /// <param name="updateAdminDto"><see langword="UpdateAdminDto"/> containing required properties to update the admin</param>
        /// <returns><see langword="Ok"/> if successful, otherwise a <see langword="BadRequest"/></returns>
        [HttpPut(nameof(UpdateAdmin))]
        public async Task<IActionResult> UpdateAdmin([FromBody] UpdateAdminDto updateAdminDto)
        {
            var adminToEdit = await _adminFetchService.FindByIdWithClaimsAsync(updateAdminDto.AdminId);

            if (adminToEdit == null || !ValidateAdminAccess(adminToEdit))
                return Unauthorized();
            
            adminToEdit = await _adminFetchService.FindByIdWithClaimsAsync(updateAdminDto.UserId);

            if (adminToEdit == null || !ValidateAdminAccess(adminToEdit))
                return Unauthorized();

            if (await _adminUpdateService.UpdateAdmin(adminToEdit, updateAdminDto) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));

            return Ok("Admin updated");
        }

        #endregion
    }
}