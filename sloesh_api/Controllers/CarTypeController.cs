﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using sloesh_api.Helpers;
using sloesh_api.Models.User;
using sloesh_api.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using sloesh_api.CustomErrors;
using sloesh_api.Dtos.Car;
using sloesh_api.Dtos.User.Identity;
using sloesh_api.Models.Car;
using sloesh_api.Models.ViewModels.Car;

namespace sloesh_api.Controllers
{
    [Authorize(Policy = Constants.Strings.JwtPolicies.AdminUser)]
    [ApiController]
    [Route("api/[controller]")]
    public class CarTypeController : AdminControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICarTypeService _carTypeService;
        private readonly IAdminFetchService _adminFetchService;

        public CarTypeController(
            IAdminFetchService adminFetchService,
            ICarTypeService carTypeService,
            UserManager<User> userManager,
            IMapper mapper
        ) : base(userManager)
        {
            _adminFetchService = adminFetchService;
            _carTypeService = carTypeService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all car types
        /// </summary>
        ///
        /// <remarks>GET api/carType/GetCarTypes</remarks>
        /// 
        /// <param name="userIdDto"><see langword="UserIdDto"/> containing the user id.</param>
        /// <returns><see langword="List"/> of <see langword="CarTypeViewModel"/>.</returns>
        [HttpGet(nameof(GetCarTypes))]
        public async Task<IActionResult> GetCarTypes([FromQuery] UserIdDto userIdDto)
        {
            var carTypes = await _carTypeService.GetAllCarTypes();
            
            var carTypeViewModels = _mapper.Map<List<CarType>, List<CarTypeViewModel>>(carTypes);
            
            return Ok(carTypeViewModels);
        }

        /// <summary>
        /// Creates an advertisement
        /// </summary>
        ///
        /// <remarks>POST api/carType/CreateCarType</remarks>
        /// 
        /// <param name="createCarTypeDto"><see langword="CreateCarTypeDto"/> containing the parameters to create
        /// the car type</param>
        /// <returns><see langword="Ok"/> if successful, otherwise a <see langword="BadRequest"/></returns>
        [HttpPost(nameof(CreateCarType))]
        public async Task<IActionResult> CreateCarType([FromBody] CreateCarTypeDto createCarTypeDto)
        {
            var admin = await _adminFetchService.FindByIdWithClaimsAsync(createCarTypeDto.UserId);

            if (admin == null || !ValidateAdminAccess(admin))
                return Unauthorized();
            
            var carType = new CarType
            {
                Name = createCarTypeDto.Name,
                Description = createCarTypeDto.Description,
                CarTypeCars = new List<CarTypeCar>()
            };
            
            if (await _carTypeService.AddCarType(carType) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));

            return Ok();
        }

        /// <summary>
        /// Updates the advertisement
        /// </summary>
        ///
        /// <remarks>PUT api/carType/UpdateCarType</remarks>
        /// 
        /// <param name="updateCarTypeDto"><see langword="UpdateCarTypeDto"/> containing the parameters to update
        /// the car type</param>
        /// <returns>
        ///     <see langword="Ok"/> if succeeded,
        ///     <see langword="NotFound"/> if not exists,
        ///     <see langword="BadRequest"/> if failed during updating
        /// </returns>
        [HttpPut(nameof(UpdateCarType))]
        public async Task<IActionResult> UpdateCarType([FromBody] UpdateCarTypeDto updateCarTypeDto)
        {
            var admin = await _adminFetchService.FindByIdWithClaimsAsync(updateCarTypeDto.UserId);

            if (admin == null || !ValidateAdminAccess(admin))
                return Unauthorized();

            var carType = await _carTypeService.GetCarTypeById(updateCarTypeDto.Id);

            if (carType == null)
                return NotFound();
            
            if (await _carTypeService.UpdateCarType(carType, updateCarTypeDto) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));
            
            return Ok();
        }

        /// <summary>
        /// Deletes the advertisement
        /// </summary>
        ///
        /// <remarks>DELETE api/carType/DeleteCarType</remarks>
        /// 
        /// <param name="carTypeDto"><see langword="CarTypeDto"/> containing the parameters to delete the car type.</param>
        /// <returns>
        ///     <see langword="Ok"/> if succeeded,
        ///     <see langword="NotFound"/> if not exists,
        ///     <see langword="BadRequest"/> if failed during deletion
        /// </returns>
        [HttpDelete(nameof(DeleteCarType))]
        public async Task<IActionResult> DeleteCarType([FromBody] CarTypeDto carTypeDto)
        {
            var admin = await _adminFetchService.FindByIdWithClaimsAsync(carTypeDto.UserId);

            if (admin == null || !ValidateAdminAccess(admin))
                return Unauthorized();

            var carType = await _carTypeService.GetCarTypeById(carTypeDto.Id);

            if (carType == null)
                return NotFound();
            
            if (await _carTypeService.DeleteCarType(carType) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));
            
            return Ok();
        }
    }
}