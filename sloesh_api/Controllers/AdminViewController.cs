﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using sloesh_api.CustomErrors;
using sloesh_api.Dtos.User.Identity;
using sloesh_api.Dtos.User.Admin;
using sloesh_api.Helpers;
using sloesh_api.Models.User;
using sloesh_api.Models.ViewModels.Identity.Admin;
using sloesh_api.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using sloesh_api.Models.User.Customer;

namespace sloesh_api.Controllers
{
    [Authorize(Policy = Constants.Strings.JwtPolicies.AdminUser)]
    [ApiController]
    [Route("api/[controller]")]
    public class AdminViewController : AdminControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICustomerFetchService _customerFetchService;
        private readonly IAdminFetchService _adminFetchService;

        public AdminViewController(
            ICustomerFetchService customerFetchService,
            IAdminFetchService adminFetchService,
            UserManager<User> userManager,
            IMapper mapper
        ) : base(userManager)
        {
            _customerFetchService = customerFetchService;
            _adminFetchService = adminFetchService;
            _mapper = mapper;
        }

        #region Customers

        /// <summary>Gets all users with the api access role.</summary>
        ///
        /// <remarks>GET api/adminView/GetUsers</remarks>
        ///
        /// <param name="userIdDto">The userId <see langword="UserIdDto"/> of the admin</param>
        ///
        /// <returns>An <see langword="Ok"/> with the <see langword="CustomerAdminViewModel"/> if successful, else a <see langword="BadRequest"/></returns>
        [HttpGet(nameof(GetUsers))]
        public async Task<IActionResult> GetUsers([FromQuery] UserIdDto userIdDto)
        {
            if (ValidateAdmin(await _adminFetchService.FindByIdWithClaimsAsync(userIdDto.UserId)) is IActionResult
                validationError)
                return validationError;

            var customerUsers = await _customerFetchService.GetAllCustomers();
            var customers = _mapper.Map<ICollection<Customer>, List<CustomerAdminViewModel>>(customerUsers);

            return Ok(customers);
        }

        #endregion

        #region Admins

        /// <summary>Gets all users with the admin role.</summary>
        ///
        /// <remarks>POST api/adminView/GetAdmins</remarks>
        ///
        /// <param name="userIdDto">The userId <see langword="UserIdDto"/> of the admin</param>
        ///
        /// <returns>An <see langword="Ok"/> with the <see langword="UserViewModel"/> if successful, else a <see langword="BadRequest"/></returns>
        [HttpGet(nameof(GetAdmins))]
        public async Task<IActionResult> GetAdmins([FromQuery] UserIdDto userIdDto)
        {
            if (ValidateAdmin(await _adminFetchService.FindByIdWithClaimsAsync(userIdDto.UserId)) is IActionResult
                validationError)
                return validationError;

            var adminUsers = await _adminFetchService.GetAllAdmins();
            var admins = _mapper.Map<List<Admin>, List<AdminViewModel>>(adminUsers);

            return Ok(admins);
        }

        #endregion
    }
}