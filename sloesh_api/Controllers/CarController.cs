﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using AutoMapper;
using Castle.Core.Internal;
using sloesh_api.CustomErrors;
using sloesh_api.Helpers;
using sloesh_api.Dtos.User.Identity;
using sloesh_api.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore.Internal;
using sloesh_api.Dtos.Advertisement;
using sloesh_api.Dtos.Car;
using sloesh_api.Models.Advertisements;
using sloesh_api.Models.Car;
using sloesh_api.Models.ViewModels.Advertisement;
using sloesh_api.Models.ViewModels.Car;
using sloesh_api.Models.ViewModels.Identity.Customer;

namespace sloesh_api.Controllers
{
    [Authorize(Policy = Constants.Strings.JwtPolicies.ApiUser)]
    [ApiController]
    [Route("api/[controller]")]
    public class CarController : ControllerBase
    {
        private readonly ICarService _carService;
        private readonly IAdvertisementService _advertisementService;
        private readonly ICustomerFetchService _customerFetchService;
        private readonly ICustomerUpdateService _customerUpdateService;
        private readonly IMapper _mapper;

        public CarController(
            ICarService carService,
            IAdvertisementService advertisementService,
            ICustomerFetchService customerFetchService,
            ICustomerUpdateService customerUpdateService,
            IMapper mapper)
        {
            _carService = carService;
            _advertisementService = advertisementService;
            _customerFetchService = customerFetchService;
            _customerUpdateService = customerUpdateService;
            _mapper = mapper;
        }

        #region UsedByAdmin

        [HttpGet(nameof(GetUnassignedCars))]
        public async Task<IActionResult> GetUnassignedCars(UserIdDto userIdDto)
        {
            var cars = await _carService.GetUnassignedCars();

            return Ok();
        }

//        [HttpPost(nameof(AssignCarTypesToCar))]
//        public async Task<IActionResult> AssignCarTypesToCar(AssignCarTypesDto assignCarTypesDto)
//        {
//            return Ok();
//        }

        #endregion

        #region UsedByCustomer

        /// <summary>
        /// Gets the cars of a user
        /// </summary>
        ///
        /// <remarks>GET api/car/GetCars</remarks>
        /// 
        /// <param name="userIdDto"><see langword="UserIdDto"/> containing the user id</param>
        /// <returns><see langword="Ok"/> if succeeded, else a <see langword="UserNotFoundObjectResult"/></returns>
        [HttpGet(nameof(GetCars))]
        public async Task<IActionResult> GetCars([FromQuery] UserIdDto userIdDto)
        {
            var customer = await _customerFetchService.FindByIdWithRelationsAsync(userIdDto.UserId);

            if (customer == null)
                return new UserNotFoundObjectResult();

            var cars = _mapper.Map<ICollection<Car>, List<CarViewModel>>(customer.Cars);

            return Ok(cars);
        }

        /// <summary>
        /// Gets the supported cars of a user for an advertisement
        /// </summary>
        ///
        /// <remarks>GET api/car/GetSupportedCars</remarks>
        /// 
        /// <param name="advertisementIdDto"><see langword="AdvertisementIdDto"/> containing the user and advertisement id.</param>
        /// <returns><see langword="Ok"/> if succeeded, else if user not found, a <see langword="UserNotFoundObjectResult"/>,
        /// else if advertisement not found, a <see langword="NotFound"/></returns>
        [HttpGet(nameof(GetSupportedCars))]
        public async Task<IActionResult> GetSupportedCars([FromQuery] AdvertisementIdDto advertisementIdDto)
        {
            var customer =
                await _customerFetchService.FindByIdWithPersonalCarsAdvertisementsAsync(advertisementIdDto.UserId);
            var advertisement = await _advertisementService.FindByIdWithRelationsAsync(advertisementIdDto.Id);

            if (customer == null)
                return new UserNotFoundObjectResult();
            if (advertisement == null)
                return NotFound();

            var filteredCars = FilterCars(customer.Cars, advertisement.CarTypeAdvertisements);

            var subscribeAdvertisementViewModel = new SubscribeAdvertisementViewModel
            {
                CustomerInformation = _mapper.Map<CustomerViewModel>(customer),
                Cars = _mapper.Map<ICollection<Car>, List<CarViewModel>>(filteredCars)
            };


            return Ok(subscribeAdvertisementViewModel);
        }

        /// <summary>
        /// Adds a car
        /// </summary>
        ///
        /// <remarks>POST api/car/AddCar</remarks>
        /// 
        /// <param name="addCarDto"><see langword="AddCarDto"/> containing the parameters to add a car</param>
        /// <returns><see langword="Ok"/> if succeeded, else a <see langword="UserNotFoundObjectResult"/></returns>
        [HttpPost(nameof(AddCar))]
        public async Task<IActionResult> AddCar([FromBody] AddCarDto addCarDto)
        {
            var customer = await _customerFetchService.FindByIdWithRelationsAsync(addCarDto.UserId);

            if (customer == null)
                return new UserNotFoundObjectResult();

            var car = new Car
            {
                Customer = customer,
                LicensePlate = addCarDto.LicensePlate,
                CarAdvertisements = new List<CustomerAdvertisement>(),
                CarTypeCars = new List<CarTypeCar>()
            };

            if (await _customerUpdateService.AddCar(customer, car) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));

            var cars = _mapper.Map<ICollection<Car>, List<CarViewModel>>(customer.Cars);

            return Ok(cars);
        }

//        /// <summary>
//        /// Updates a car
//        /// </summary>
//        /// 
//        /// <remarks>PUT api/customer/UpdateCar</remarks>
//        /// 
//        /// <param name="updateCarDto"><see langword="UpdateCarDto"/> containing the parameters to update a car</param>
//        /// <returns>
//        ///     <see langword="Ok"/> if succeeded,
//        ///     <see langword="UserNotFoundObjectResult"/> if user not found,
//        ///     <see langword="NotFound"/> if car not found,
//        ///     <see langword="BadRequest"/> if another error occurs.
//        /// </returns>
//        [HttpPut(nameof(UpdateCar))]
//        public async Task<IActionResult> UpdateCar([FromBody] UpdateCarDto updateCarDto)
//        {
//            var customer = await _customerFetchService.FindByIdWithRelationsAsync(updateCarDto.UserId);
//
//            if (customer == null)
//                return new UserNotFoundObjectResult();
//
//            var car = customer.Cars.Find(c => string.Equals(c.LicensePlate, updateCarDto.LicensePlate));
//
//            if (car == null)
//                return NotFound();
//
//            if (await _customerUpdateService.UpdateCar(car, updateCarDto) is Error error)
//                return BadRequest(Errors.AddErrorToModelState(error, ModelState));
//
//            var cars = _mapper.Map<ICollection<Car>, List<CarViewModel>>(customer.Cars);
//            
//            return Ok(cars);
//        }

        /// <summary>
        /// Deletes a car
        /// </summary>
        ///
        /// <remarks>DELETE api/car/DeleteCar</remarks>
        /// 
        /// <param name="deleteCarDto"><see langword="DeleteCarDto"/> containing the parameters to delete a car</param>
        /// <returns>
        ///     <see langword="Ok"/> if succeeded,
        ///     <see langword="UserNotFoundObjectResult"/> if user not found,
        ///     <see langword="NotFound"/> if car not found,
        ///     <see langword="BadRequest"/> if another error occurs.
        /// </returns>
        [HttpDelete(nameof(DeleteCar))]
        public async Task<IActionResult> DeleteCar([FromBody] DeleteCarDto deleteCarDto)
        {
            var customer = await _customerFetchService.FindByIdWithRelationsAsync(deleteCarDto.UserId);

            if (customer == null)
                return new UserNotFoundObjectResult();

            var car = customer.Cars.Find(c => string.Equals(c.LicensePlate, deleteCarDto.LicensePlate));

            if (car == null)
                return NotFound();

            if (await _customerUpdateService.DeleteCar(car) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));

            return Ok("Car Deleted");
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Filters a list of cars given a list of car types that the advertisement supports
        /// </summary>
        /// <param name="cars"><see langword="List"/> of <see langword="Car"/> to filter.</param>
        /// <param name="carTypeAdvertisements"><see langword="IReadOnlyCollection"/> of <see langword="CarTypeAdvertisement"/> to filter it by.</param>
        /// <returns><see langword="List"/> of filtered <see langword="Car"/>.</returns>
        private static List<Car> FilterCars(List<Car> cars,
            IReadOnlyCollection<CarTypeAdvertisement> carTypeAdvertisements)
        {
            for (var i = cars.Count - 1; i >= 0; --i)
            {
                if (cars[i].CarTypeCars.IsNullOrEmpty() || !carTypeAdvertisements.Any(cta =>
                        cars[i].CarTypeCars.Any(ctc => ctc.CarTypeId == cta.CarTypeId)))
                {
                    cars.RemoveAt(i);
                }
            }

            return cars;
        }

        #endregion
    }
}