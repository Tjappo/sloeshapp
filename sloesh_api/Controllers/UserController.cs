﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.Threading.Tasks;
using Castle.Core.Internal;
using sloesh_api.CustomErrors;
using sloesh_api.Helpers;
using sloesh_api.Dtos.User.Identity;
using sloesh_api.Services.Interfaces;
using sloesh_api.Models.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using sloesh_api.Models.Advertisements;
using sloesh_api.Models.Car;
using sloesh_api.Models.User.Customer;

namespace sloesh_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ICustomerUpdateService _customerUpdateService;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public UserController(
            ICustomerUpdateService customerUpdateService,
            UserManager<User> userManager,
            IMapper mapper)
        {
            _customerUpdateService = customerUpdateService;
            _userManager = userManager;
            _mapper = mapper;
        }

        /// <summary>Registers the user.</summary>
        ///
        /// <remarks>POST api/user/register</remarks>
        ///
        /// <param name="registerCustomerDto">The user information <see langword="RegisterCustomerDto"/> from the request body.</param>
        ///
        /// <returns>An <see langword="OkObject"/> with the <see langword="string"/> 'Account created' if successful, else a <see langword="BadRequestObjectResult"/></returns>
        [HttpPost(nameof(Register))]
        public async Task<IActionResult> Register([FromBody] RegisterCustomerDto registerCustomerDto)
        {
            // Map Dto to model
            var user = MapToCustomer(registerCustomerDto);

            // Check Username already exists
            if (await _userManager.FindByNameAsync(user.UserName) != null)
                return new UserAlreadyExistsObjectResult();

            if (await _customerUpdateService.RegisterCustomer(user, registerCustomerDto.Password,
                Constants.Strings.JwtClaims.UnvalidatedAccess, Url, Request.Scheme) is Error error)
                return new BadRequestObjectResult(Errors.AddErrorToModelState(error, ModelState));

            return Ok("User registered");
        }

        /// <summary>Confirms the mail of the user.</summary>
        ///
        /// <remarks>GET api/user/ConfirmEmail</remarks>
        ///
        /// <param name="confirmEmailDto">The user id and token <see langword="ConfirmEmailDto"/></param>
        ///
        /// <returns>An <see langword="Ok"/> if successful, else a <see langword="BadRequestObjectResult"/></returns>
        [HttpGet(nameof(ConfirmEmail))]
        public async Task<IActionResult> ConfirmEmail([FromQuery] ConfirmEmailDto confirmEmailDto)
        {
            var user = await _userManager.FindByIdAsync(confirmEmailDto.UserId);

            if (user == null)
                return new UserNotFoundObjectResult();

            var result = await _userManager.ConfirmEmailAsync(user, confirmEmailDto.Token);

            if (!result.Succeeded)
                return new BadRequestObjectResult(result);

            if (await ChangeClaim(user, Constants.Strings.JwtClaims.UnvalidatedAccess,
                Constants.Strings.JwtClaims.ApiAccess) is Error error)
                return  new BadRequestObjectResult(error);
            
            return Ok("Email has been confirmed!");
        }

        /// <summary>Resets the password of the user.</summary>
        ///
        /// <remarks>PUT api/user/ResetPassword</remarks>
        ///
        /// <param name="resetPasswordDto">The required information to change the password <see langword="ResetPasswordDto"/></param>
        ///
        /// <returns>An <see langword="Ok"/> if successful, else a <see langword="BadRequestObjectResult"/></returns>
        [HttpPut(nameof(ResetPassword))]
        public async Task<IActionResult> ResetPassword(ResetPasswordDto resetPasswordDto)
        {
            var user = await _userManager.FindByIdAsync(resetPasswordDto.UserId);

            if (user == null)
                return new UserNotFoundObjectResult();

            var result = await _userManager.ResetPasswordAsync(user, resetPasswordDto.Token, resetPasswordDto.Password);

            if (result.Succeeded)
                return Ok("Password has been reset!");

            return new BadRequestObjectResult(result);
        }

        /// <summary>Sends an email to the user to reset its password.</summary>
        ///
        /// <remarks>POST api/user/ForgotPassword</remarks>
        ///
        /// <param name="userNameDto">The <see langword="UserNameDto"/></param>
        ///
        /// <returns>An <see langword="Ok"/> if successful, else a <see langword="BadRequestObjectResult"/></returns>
        [HttpPost(nameof(ForgotPassword))]
        public async Task<IActionResult> ForgotPassword([FromBody] UserNameDto userNameDto)
        {
            var user = await _userManager.FindByNameAsync(userNameDto.UserName);

            if (user == null)
                return new UserNotFoundObjectResult();

            await _customerUpdateService.SendForgotPasswordEmail(user, Url, Request.Scheme);

            return Ok("Email to reset password has been sent!");
        }

        /// <summary>Sends an email to the user to reset its password.</summary>
        ///
        /// <remarks>POST api/user/resetPassword</remarks>
        ///
        /// <param name="userIdDto">The <see langword="string"/> user id</param>
        ///
        /// <returns>An <see langword="Ok"/> if successful, else a <see langword="BadRequestObjectResult"/></returns>
        [HttpGet(nameof(ResetPassword))]
        public async Task<IActionResult> ResetPassword([FromQuery] UserIdDto userIdDto)
        {
            var user = await _userManager.FindByIdAsync(userIdDto.UserId);

            if (user == null)
                return new UserNotFoundObjectResult();

            await _customerUpdateService.SendForgotPasswordEmail(user, Url, Request.Scheme);

            return Ok("Email to reset password has been sent!");
        }

        /// <summary>Updates the email of the user.</summary>
        ///
        /// <remarks>PUT api/user/updateEmail</remarks>
        ///
        /// <param name="updateEmailDto">The <see langword="UpdateEmailDto"/> containing the fields to update</param>
        ///
        /// <returns>An <see langword="Ok"/> if successful, else a <see langword="BadRequest"/></returns>
        [Authorize(Policy = Constants.Strings.JwtPolicies.ApiUser)]
        [HttpPut(nameof(UpdateEmail))]
        public async Task<IActionResult> UpdateEmail([FromBody] UpdateEmailDto updateEmailDto)
        {
            var user = await _userManager.FindByIdAsync(updateEmailDto.UserId);

            if (user == null)
                return new UserNotFoundObjectResult();

            if (await _customerUpdateService.UpdateEmail(user, updateEmailDto, Url, Request.Scheme) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));

            return Ok("Email updated!");
        }

        /// <summary>Changes the email of the user.</summary>
        ///
        /// <remarks>GET api/user/changeEmailToken</remarks>
        ///
        /// <param name="changeEmailDto">The <see langword="ChangeUserDto"/> containing required fields to update the email</param>
        ///
        /// <returns>An <see langword="Ok"/> if successful, else a <see langword="BadRequest"/></returns>
        [HttpGet(nameof(ChangeEmailToken))]
        public async Task<IActionResult> ChangeEmailToken([FromQuery] ChangeEmailDto changeEmailDto)
        {
            var user = await _userManager.FindByNameAsync(changeEmailDto.OldEmail);

            if (user == null)
                return new UserNotFoundObjectResult();

            if (await _customerUpdateService.ChangeEmail(user, changeEmailDto) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));

            return Ok("Email has been changed");
        }
        
        #region Helpers

        /// <summary>
        /// Maps the dto to a customer
        /// </summary>
        /// <param name="registerCustomerDto"><see langword="RegisterCustomerDto"/> containing the user parameters</param>
        /// <returns><see langword="Customer"/> mapped entity</returns>
        private Customer MapToCustomer(RegisterCustomerDto registerCustomerDto)
        {
            // Map Dto to model
            var customer = _mapper.Map<Customer>(registerCustomerDto);
            customer.AccountInfo.CreatedAt = DateTime.Now;
            customer.Cars = new List<Car>();
            foreach (var licensePlate in registerCustomerDto.LicensePlates)
            {
                if (!licensePlate.IsNullOrEmpty())
                {
                    customer.Cars.Add(new Car
                    {
                        LicensePlate = licensePlate,
                        CarAdvertisements = new Collection<CustomerAdvertisement>(),
                        Customer = customer
                    });
                }
            }
            return customer;
        }
        
        /// <summary>
        /// Changes the claim of a user
        /// </summary>
        /// <param name="user"><see langword="User"/> to change the claim of</param>
        /// <param name="oldClaim"><see langword="string"/> to change it from</param>
        /// <param name="newClaim"><see langword="string"/> to change it to</param>
        /// <returns><see langword="Error"/> if any, else <see langword="null"/></returns>
        private async Task<Error> ChangeClaim(User user, string oldClaim, string newClaim)
        {
            var result = await _userManager.AddClaimAsync(user,
                new Claim(Constants.Strings.JwtClaimIdentifiers.Roles, newClaim));

            if (!result.Succeeded)
                return new CustomIdentityError();
           
            result = await _userManager.RemoveClaimAsync(user,
                new Claim(Constants.Strings.JwtClaimIdentifiers.Roles, oldClaim));
            
            return !result.Succeeded ? new CustomIdentityError() : null;
        }
        
        #endregion
    }
}