﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using AutoMapper;
using sloesh_api.CustomErrors;
using sloesh_api.Helpers;
using sloesh_api.Dtos.User.Identity;
using sloesh_api.Models.ViewModels.Identity.Customer;
using sloesh_api.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using sloesh_api.Dtos.Car;
using sloesh_api.Models.Car;
using sloesh_api.Models.User.Customer;
using sloesh_api.Models.ViewModels.Car;

namespace sloesh_api.Controllers
{
    [Authorize(Policy = Constants.Strings.JwtPolicies.ApiUser)]
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerFetchService _customerFetchService;
        private readonly ICustomerUpdateService _customerUpdateService;
        private readonly IMapper _mapper;

        public CustomerController(
            ICustomerFetchService customerFetchService,
            ICustomerUpdateService customerUpdateService,
            IMapper mapper)
        {
            _customerFetchService = customerFetchService;
            _customerUpdateService = customerUpdateService;
            _mapper = mapper;
        }

        #region Customers            

        /// <summary>Gets the viewable user info from a userId.</summary>
        ///
        /// <remarks>GET api/customer/get</remarks>
        ///
        /// <param name="userIdDto">The userId <see langword="UserIdDto"/></param>
        ///
        /// <returns>An <see langword="Ok"/> with the <see langword="CustomerViewModel"/> if successful, else a <see langword="BadRequest"/></returns>
        [HttpGet("get")]
        public async Task<IActionResult> GetById([FromQuery] UserIdDto userIdDto)
        {
            var customer = await _customerFetchService.FindByIdWithRelationsAsync(userIdDto.UserId);

            if (customer == null)
                return new UserNotFoundObjectResult();

            var customerViewModel = _mapper.Map<CustomerViewModel>(customer);

            return Ok(customerViewModel);
        }

        /// <summary>
        /// Updates the user
        /// </summary>
        ///
        /// <remarks>PUT api/customer/UpdateCustomer</remarks>
        /// 
        /// <param name="customerUpdateDto"><see langword="UserUpdateDto"/> containing required properties to update the user</param>
        /// <returns><see langword="Ok"/> if successful, otherwise a <see langword="BadRequest"/></returns>
        [HttpPut(nameof(UpdateCustomer))]
        public async Task<IActionResult> UpdateCustomer([FromBody] CustomerUpdateDto customerUpdateDto)
        {
            var customer = await _customerFetchService.FindByNameWithRelationsAsync(customerUpdateDto.Email);

            if (customer == null)
                return new UserNotFoundObjectResult();

            if (await _customerUpdateService.UpdateCustomer(customer, customerUpdateDto) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));

            return Ok("Customer Updated");
        }

        #endregion

        #region Cars

        /// <summary>
        /// Gets the cars of a user
        /// </summary>
        ///
        /// <remarks>GET api/customer/GetLicensePlates</remarks>
        /// 
        /// <param name="userIdDto"><see langword="UserIdDto"/> containing the user id</param>
        /// <returns><see langword="Ok"/> if succeeded, else a <see langword="UserNotFoundObjectResult"/></returns>
        [HttpGet(nameof(GetCars))]
        public async Task<IActionResult> GetCars([FromQuery] UserIdDto userIdDto)
        {
            var customer = await _customerFetchService.FindByIdWithRelationsAsync(userIdDto.UserId);

            if (customer == null)
                return new UserNotFoundObjectResult();
            
            var cars = _mapper.Map<ICollection<Car>, List<CarViewModel>>(customer.Cars);

            return Ok(cars);
        }

        /// <summary>
        /// Adds a car
        /// </summary>
        ///
        /// <remarks>POST api/customer/AddCar</remarks>
        /// 
        /// <param name="addCarDto"><see langword="AddCarDto"/> containing the parameters to add a car</param>
        /// <returns><see langword="Ok"/> if succeeded, else a <see langword="UserNotFoundObjectResult"/></returns>
        [HttpPost(nameof(AddCar))]
        public async Task<IActionResult> AddCar([FromBody] AddCarDto addCarDto)
        {
            var customer = await _customerFetchService.FindByIdWithRelationsAsync(addCarDto.UserId);

            if (customer == null)
                return new UserNotFoundObjectResult();

            var car = new Car
            {
                Customer = customer,
                LicensePlate =  addCarDto.LicensePlate
            };

            if (await _customerUpdateService.AddCar(customer, car) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));

            var cars = _mapper.Map<ICollection<Car>, List<CarViewModel>>(customer.Cars);
            
            return Ok(cars);
        }

//        /// <summary>
//        /// Updates a car
//        /// </summary>
//        /// 
//        /// <remarks>PUT api/customer/UpdateCar</remarks>
//        /// 
//        /// <param name="updateCarDto"><see langword="UpdateCarDto"/> containing the parameters to update a car</param>
//        /// <returns>
//        ///     <see langword="Ok"/> if succeeded,
//        ///     <see langword="UserNotFoundObjectResult"/> if user not found,
//        ///     <see langword="NotFound"/> if car not found,
//        ///     <see langword="BadRequest"/> if another error occurs.
//        /// </returns>
//        [HttpPut(nameof(UpdateCar))]
//        public async Task<IActionResult> UpdateCar([FromBody] UpdateCarDto updateCarDto)
//        {
//            var customer = await _customerFetchService.FindByIdWithRelationsAsync(updateCarDto.UserId);
//
//            if (customer == null)
//                return new UserNotFoundObjectResult();
//
//            var car = customer.Cars.Find(c => string.Equals(c.LicensePlate, updateCarDto.LicensePlate));
//
//            if (car == null)
//                return NotFound();
//
//            if (await _customerUpdateService.UpdateCar(car, updateCarDto) is Error error)
//                return BadRequest(Errors.AddErrorToModelState(error, ModelState));
//
//            var cars = _mapper.Map<ICollection<Car>, List<CarViewModel>>(customer.Cars);
//            
//            return Ok(cars);
//        }

        /// <summary>
        /// Deletes a car
        /// </summary>
        ///
        /// <remarks>DELETE api/customer/DeleteCar</remarks>
        /// 
        /// <param name="deleteCarDto"><see langword="DeleteCarDto"/> containing the parameters to delete a car</param>
        /// <returns>
        ///     <see langword="Ok"/> if succeeded,
        ///     <see langword="UserNotFoundObjectResult"/> if user not found,
        ///     <see langword="NotFound"/> if car not found,
        ///     <see langword="BadRequest"/> if another error occurs.
        /// </returns>
        [HttpDelete(nameof(DeleteCar))]
        public async Task<IActionResult> DeleteCar([FromBody] DeleteCarDto deleteCarDto)
        {
            var customer = await _customerFetchService.FindByIdWithRelationsAsync(deleteCarDto.UserId);

            if (customer == null)
                return new UserNotFoundObjectResult();

            var car = customer.Cars.Find(c => string.Equals(c.LicensePlate, deleteCarDto.LicensePlate));

            if (car == null)
                return NotFound();

            if (await _customerUpdateService.DeleteCar(car) is Error error)
                return BadRequest(Errors.AddErrorToModelState(error, ModelState));

            return Ok("Car Deleted");
        }

        #endregion
    }
}