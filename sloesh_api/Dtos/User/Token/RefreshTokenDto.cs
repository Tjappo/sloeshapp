using System.ComponentModel.DataAnnotations;
using sloesh_api.Dtos.User.Identity;

namespace sloesh_api.Dtos.User.Token
{
    public class RefreshTokenDto : UserIdDto
    {
        [Required] public string RefreshToken { get; set; }
    }
}