﻿using System.ComponentModel.DataAnnotations;
using sloesh_api.Dtos.User.Identity;

namespace sloesh_api.Dtos.User.Admin
{
    public class UserAdminEditDto : UserIdDto
    {
        [Required] [EmailAddress] public string UserName { get; set; }
    }
}