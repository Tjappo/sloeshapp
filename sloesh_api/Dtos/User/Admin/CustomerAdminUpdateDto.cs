﻿using System.ComponentModel.DataAnnotations;
using sloesh_api.Dtos.User.Identity;
using sloesh_api.Models.Enums.AccountInfo;

namespace sloesh_api.Dtos.User.Admin
{
    public class CustomerAdminUpdateDto : CustomerUpdateDto
    {
        [Required] public string AdminId { get; set; }
        [Required] public string UserId { get; set; }

        [Required]
        [Range(typeof(bool), "false", "true")]
        public bool EmailConfirmed { get; set; }

        [Required]
        [Range(typeof(bool), "false", "true")]
        public bool IsLockedOut { get; set; }
    }
}