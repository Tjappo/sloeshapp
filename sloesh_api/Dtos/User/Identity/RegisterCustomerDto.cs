﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using sloesh_api.Dtos.Car;
using sloesh_api.Models.Enums.AccountInfo;

namespace sloesh_api.Dtos.User.Identity
{
    public class RegisterCustomerDto
    {
        [Required] [StringLength(100)] public string FirstName { get; set; }
        [Required] [StringLength(100)] public string LastName { get; set; }

        [Required] public string Address { get; set; }
        [Required] public string Zipcode { get; set; }

        [Required] [EmailAddress] public string Email { get; set; }

        [Required] public string Iban { get; set; }

        [Required] public List<string> LicensePlates { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare(nameof(Password), ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}