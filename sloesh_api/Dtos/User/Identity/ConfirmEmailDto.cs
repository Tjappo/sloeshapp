﻿using System.ComponentModel.DataAnnotations;

namespace sloesh_api.Dtos.User.Identity
{
    public class ConfirmEmailDto : UserIdDto
    {
        [Required] public string Token { get; set; }
    }
}