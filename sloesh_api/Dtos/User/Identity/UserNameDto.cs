﻿using System.ComponentModel.DataAnnotations;

namespace sloesh_api.Dtos.User.Identity
{
    public class UserNameDto
    {
        [Required] [EmailAddress] public string UserName { get; set; }
    }
}