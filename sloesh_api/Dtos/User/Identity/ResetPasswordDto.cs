﻿using System.ComponentModel.DataAnnotations;

namespace sloesh_api.Dtos.User.Identity
{
    public class ResetPasswordDto : UserIdDto
    {
        [Required] public string Token { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare(nameof(Password), ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}