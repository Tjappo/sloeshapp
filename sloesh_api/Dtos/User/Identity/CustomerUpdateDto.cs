using System.ComponentModel.DataAnnotations;
using sloesh_api.Models.Enums.AccountInfo;

namespace sloesh_api.Dtos.User.Identity
{
    public class CustomerUpdateDto
    {
        [Required] [StringLength(100)] public string FirstName { get; set; }
        [Required] [StringLength(100)] public string LastName { get; set; }
        [Required] [EmailAddress] public string Email { get; set; }

        [Required] public string Address { get; set; }
        [Required] public string Zipcode { get; set; }

        [Required] public string IBAN { get; set; }
    }
}