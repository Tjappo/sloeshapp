﻿using System.ComponentModel.DataAnnotations;

namespace sloesh_api.Dtos.User.Identity
{
    public class UserIdDto
    {
        [Required] public string UserId { get; set; }
    }
}