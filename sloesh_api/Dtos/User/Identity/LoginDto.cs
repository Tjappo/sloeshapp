﻿using System.ComponentModel.DataAnnotations;

namespace sloesh_api.Dtos.User.Identity
{
    public class LoginDto : UserNameDto
    {
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}