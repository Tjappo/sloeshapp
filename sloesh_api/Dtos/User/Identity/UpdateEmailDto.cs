﻿using System.ComponentModel.DataAnnotations;

namespace sloesh_api.Dtos.User.Identity
{
    public class UpdateEmailDto : UserIdDto
    {
        [Required] [EmailAddress] public string Email { get; set; }
    }
}