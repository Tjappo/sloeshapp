﻿using System.ComponentModel.DataAnnotations;

namespace sloesh_api.Dtos.User.Identity
{
    public class ChangeEmailDto
    {
        [Required] public string Token { get; set; }
        [Required] [EmailAddress] public string OldEmail { get; set; }
        [Required] [EmailAddress] public string NewEmail { get; set; }
    }
}