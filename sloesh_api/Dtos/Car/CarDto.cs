using System.ComponentModel.DataAnnotations;

namespace sloesh_api.Dtos.Car
{
    public class CarDto
    {
        [Required] public string LicensePlate { get; set; }
    }
}