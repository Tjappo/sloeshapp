using System.ComponentModel.DataAnnotations;
using sloesh_api.Dtos.User.Identity;

namespace sloesh_api.Dtos.Car
{
    public class DeleteCarDto : UserIdDto
    {
        [Required] public string LicensePlate { get; set; }
    }
}