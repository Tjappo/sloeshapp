using System.ComponentModel.DataAnnotations;

namespace sloesh_api.Dtos.Car
{
    public class UpdateCarTypeDto : CarTypeDto
    {
        [Required] public string Name { get; set; }

        [Required] public string Description { get; set; }
    }
}