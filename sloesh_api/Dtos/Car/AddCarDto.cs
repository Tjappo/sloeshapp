using System.ComponentModel.DataAnnotations;

namespace sloesh_api.Dtos.Car
{
    public class AddCarDto : CarDto
    {
        [Required] public string UserId { get; set; }
    }
}