using System.ComponentModel.DataAnnotations;
using sloesh_api.Dtos.User.Identity;

namespace sloesh_api.Dtos.Car
{
    public class CreateCarTypeDto : UserIdDto
    {
        [Required] public string Name { get; set; }
        
        [Required] public string Description { get; set; }
    }
}