using System.ComponentModel.DataAnnotations;
using sloesh_api.Dtos.User.Identity;

namespace sloesh_api.Dtos.Car
{
    public class CarTypeDto : UserIdDto
    {
        [Required] public int Id { get; set; }
    }
}