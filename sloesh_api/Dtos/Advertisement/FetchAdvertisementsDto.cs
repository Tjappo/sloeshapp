using System.ComponentModel.DataAnnotations;

namespace sloesh_api.Dtos.Advertisement
{
    public class FetchAdvertisementsDto
    {
        [Required] public string ZipCode { get; set; }
        [Required] public string LicensePlate { get; set; }
    }
}