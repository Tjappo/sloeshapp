using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using sloesh_api.Dtos.User.Identity;
using sloesh_api.Helpers;

namespace sloesh_api.Dtos.Advertisement
{
    public class AdvertisementDto : UserIdDto
    {
        [Required]
        [RegularExpression(@"^[A-Z]+[a-zA-Z\s]+[a-zA-Z]*$")]
        public string Title { get; set; }

        [Required] public string Logo { get; set; }
        
        [Required] public string Image { get; set; }

        [Required]
        [Range(Constants.Numbers.Advertisement.MinimumCompensation,
            Constants.Numbers.Advertisement.MaximumCompensation)]
        public float Compensation { get; set; }

        [Required]
        [Range(Constants.Numbers.Advertisement.MinimumDuration, Constants.Numbers.Advertisement.MaximumDuration)]
        public int Duration { get; set; }
        
        [Required]
        public List<int> CarTypes { get; set; }

        [Required] public DateTime Start { get; set; }

        [Required] public DateTime End { get; set; }
    }
}