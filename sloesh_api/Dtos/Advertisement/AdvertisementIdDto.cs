using System.ComponentModel.DataAnnotations;
using sloesh_api.Dtos.User.Identity;

namespace sloesh_api.Dtos.Advertisement
{
    public class AdvertisementIdDto : UserIdDto
    {
        [Required] public int Id { get; set; }
    }
}