using System.ComponentModel.DataAnnotations;

namespace sloesh_api.Dtos.Advertisement
{
    public class AdvertisementUpdateDto : AdvertisementDto
    {
        [Required] public int Id;
    }
}