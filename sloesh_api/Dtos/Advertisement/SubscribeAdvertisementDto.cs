using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using sloesh_api.Dtos.User.Identity;

namespace sloesh_api.Dtos.Advertisement
{
    public class SubscribeAdvertisementDto : UserIdDto
    {
        [Required] public int AdvertisementId { get; set; }
        [Required] public List<string> Cars { get; set; }
        [Required] public bool UseHomeAddress { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
    }
}