﻿using System.ComponentModel.DataAnnotations;
using sloesh_api.Dtos.User.Identity;

namespace sloesh_api.Dtos.Admin
{
    public abstract class AdminDto : UserIdDto
    {
        [Required]
        [RegularExpression(@"^[A-Z]+[a-zA-Z\s]+[a-zA-Z]*$")]
        public string DisplayName { get; set; }

        [Required] [EmailAddress] public string UserName { get; set; }

        [Required]
        [Range(typeof(bool), "false", "true")]
        public bool IsLockedOut { get; set; }
    }
}