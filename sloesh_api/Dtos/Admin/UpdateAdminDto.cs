﻿using System.ComponentModel.DataAnnotations;

namespace sloesh_api.Dtos.Admin
{
    public class UpdateAdminDto : AdminDto
    {
        [Required] public string AdminId { get; set; }
    }
}