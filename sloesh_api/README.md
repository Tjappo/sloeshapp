# Sloesh API

This repo contains the API of Sloesh.

---

# Table of Contents

* [Features](#features)
* [Prerequisites](#prerequisites)
* [Installation - Getting Started!](#installation)

# Features

- **ASP.NET Core 2.2**
  - Web API

# Prerequisites:
 * dotnet core 2.2

# Installation / Getting Started:
 * Clone this repo
 * At the repo's root directory run `dotnet restore`
 * Run the application (`dotnet run`)

 or

 * Run the application in VSCode or Visual Studio 2017 (Hit `F5`)
 * Browse to [http://localhost:8000](http://localhost:8000)


----

# Found a Bug? Want to Contribute?

Nothing's ever perfect, but please let me know by creating an issue (make sure there isn't an existing one about it already), and we'll try and work out a fix for it! If you have any good ideas, or want to contribute, feel free to either make an Issue with the Proposal, or just make a PR from your Fork.

----
