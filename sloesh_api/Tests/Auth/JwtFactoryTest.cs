using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using sloesh_api.Auth;
using sloesh_api.Helpers;
using sloesh_api.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Xunit;

namespace sloesh_api.Tests.Auth
{
    public class JwtFactoryTest
    {
        private IJwtFactory _jwtFactory;

        private const string _secret =
            "THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING";

        private const string _jwtIssuer = "jwtIssuer";
        private const string _jwtAudience = "jwtAudience";
        private ClaimsIdentity _claimsIdentity;
        private const string _id = "id";
        private const string _userName = "userName";
        private const string _roles = "roles";

        public JwtFactoryTest()
        {
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_secret));
            var jwtIssuerOptions = new OptionsWrapper<JwtIssuerOptions>(new JwtIssuerOptions
            {
                Issuer = _jwtIssuer,
                Audience = _jwtAudience,
                SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
            });
            _jwtFactory = new JwtFactory(jwtIssuerOptions);
            _claimsIdentity = _jwtFactory.GenerateClaimsIdentity(_userName, _id, _roles);
        }

        [Fact]
        public void GenerateClaimsIdentityTest()
        {
            Assert.Equal(_id,
                _claimsIdentity.Claims.First(x => string.Equals(x.Type, Constants.Strings.JwtClaimIdentifiers.Id))
                    .Value);
            Assert.Equal(_roles,
                _claimsIdentity.Claims.First(x => string.Equals(x.Type, Constants.Strings.JwtClaimIdentifiers.Roles))
                    .Value);
            Assert.Equal(_userName, _claimsIdentity.Name);
        }

        [Fact]
        public async void GenerateEncodedTokenTest()
        {
            var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            var token = await _jwtFactory.GenerateEncodedToken(_userName, _claimsIdentity);
            Assert.True(jwtSecurityTokenHandler.CanReadToken(token));
            var tokenDecoded = jwtSecurityTokenHandler.ReadJwtToken(token);
            Assert.Equal(tokenDecoded.Issuer, _jwtIssuer);
            Assert.NotNull(tokenDecoded.Audiences.FirstOrDefault(x => x.Equals(_jwtAudience)));
            Assert.Equal(_id,
                tokenDecoded.Claims.First(x => string.Equals(x.Type, Constants.Strings.JwtClaimIdentifiers.Id)).Value);
            Assert.Equal(_roles,
                tokenDecoded.Claims.First(x => string.Equals(x.Type, Constants.Strings.JwtClaimIdentifiers.Roles))
                    .Value);
        }
    }
}