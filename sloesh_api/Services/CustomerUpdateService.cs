﻿using System;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using sloesh_api.Database;
using sloesh_api.Dtos.User.Identity;
using sloesh_api.CustomErrors;
using sloesh_api.Dtos.User.Admin;
using sloesh_api.Helpers;
using sloesh_api.Models.User;
using sloesh_api.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using sloesh_api.Models.Car;
using sloesh_api.Models.User.Customer;

namespace sloesh_api.Services
{
    public class CustomerUpdateService : ServiceBase, ICustomerUpdateService
    {
        private readonly UserManager<User> _userManager;
        private readonly IEmailSenderService _emailSenderService;
        private readonly ServerUrlOptions _serverUrlOptions;

        public CustomerUpdateService(
            IEmailSenderService emailSenderService,
            UserManager<User> userManager,
            ApplicationDbContext context,
            IOptions<ServerUrlOptions> serverUrlOptionsAccessor
        ) : base(context)
        {
            _userManager = userManager;
            _emailSenderService = emailSenderService;
            _serverUrlOptions = serverUrlOptionsAccessor.Value;
        }

        /// <summary>Registers the user.</summary>
        ///
        /// <param name="user">The <see langword="Customer"/> to register.</param>
        /// <param name="password">The <see langword="string"/> password to use.</param>
        /// <param name="roles">The <see langword="string"/> roles to assign.</param>
        /// <param name="urlHelper">The <see langword="IUrlHelper"/> to use.</param>
        /// <param name="requestScheme">The <see langword="string"/> requestScheme to use.</param>
        ///
        /// <returns>An <see langword="IdentityResult"/> if successful, else <see langword="null"/></returns>
        public async Task<Error> RegisterCustomer(Customer user, string password, string roles, IUrlHelper urlHelper,
            string requestScheme)
        {
            // Create User
            var result = await _userManager.CreateAsync(user, password);

            if (!result.Succeeded)
                return new CustomIdentityError();

            await _userManager.AddClaimAsync(user,
                new Claim(Constants.Strings.JwtClaimIdentifiers.Roles, roles));

            if (await SendEmailConfirmEmail(user, urlHelper, requestScheme) is Error error)
                return error;

            return await PersistChanges();
        }

        /// <summary>Sends the forgot password email to the email of the user.</summary>
        ///
        /// <param name="user">The <see langword="User"/> to send the email to.</param>
        /// <param name="urlHelper">The <see langword="IUrlHelper"/> to use.</param>
        /// <param name="requestScheme">The <see langword="string"/> requestScheme to use.</param>
        /// <returns><see langword="Error"/> if applicable, else <see langword="null"/></returns>
        public async Task<Error> SendForgotPasswordEmail(User user, IUrlHelper urlHelper, string requestScheme)
        {
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);

            var callbackUrl = $"{_serverUrlOptions.FrontEndUrl}resetPassword?userId={user.Id}&token={token}";

            return await _emailSenderService.SendEmailAsync(user.UserName, "Reset Password",
                $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");
        }


        /// <summary>Updates the user.</summary>
        ///
        /// <param name="user">The <see langword="User"/> to update.</param>
        /// <param name="updateEmailDto">The <see langword="UpdateEmailDto"/> to update to.</param>
        /// <param name="urlHelper">The <see langword="IUrlHelper"/> to use.</param>
        /// <param name="requestScheme">The <see langword="string"/> requestScheme to use.</param>
        ///
        /// <returns><see langword="Error"/> if applicable, else <see langword="null"/></returns>
        public async Task<Error> UpdateEmail(User user, UpdateEmailDto updateEmailDto, IUrlHelper urlHelper,
            string requestScheme)
        {
            if (user.Email == updateEmailDto.Email) return null;

            if (await _userManager.FindByNameAsync(updateEmailDto.Email) != null)
                return new CustomIdentityError("Email already exists");

            return await SendChangeEmailConfirmEmail(user, updateEmailDto.Email, urlHelper, requestScheme);
        }

        /// <summary>Changes the mail of the user.</summary>
        ///
        /// <param name="user">The <see langword="User"/> to update.</param>
        /// <param name="changeEmailDto">The <see langword="ChangeEmailDto"/> to update to.</param>
        ///
        /// <returns>The <see langword="IdentityResult"/> if successful, else <see langword="null"/></returns>
        public async Task<Error> ChangeEmail(User user, ChangeEmailDto changeEmailDto)
        {
            var result = await _userManager.ChangeEmailAsync(user, changeEmailDto.NewEmail, changeEmailDto.Token);

            if (!result.Succeeded) return new CustomIdentityError();

            user.UserName = user.Email;
            user.NormalizedUserName = user.NormalizedEmail;

            return await PersistChanges();
        }

        /// <summary>
        /// Updates the user
        /// </summary>
        /// <remarks>
        /// Used by an admin
        /// </remarks>
        /// <param name="customer">The <see langword="Customer"/> to edit</param>
        /// <param name="customerAdminUpdateDto">The <see langword="UserAdminUpdateDto"/> containing the parameters to edit it to</param>
        /// <returns><see langword="Error"/> if fails, else <see langword="null"/></returns>
        public async Task<Error> UpdateCustomerAdmin(Customer customer, CustomerAdminUpdateDto customerAdminUpdateDto)
        {
            customer = UpdateCustomerInfoAdmin(customer, customerAdminUpdateDto);

            return await PersistChanges();
        }

        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="customer">The <see langword="Customer"/> to edit</param>
        /// <param name="customerUpdateDto">The <see langword="CustomerUpdateDto"/> containing the parameters to edit it to</param>
        /// <returns><see langword="Error"/> if fails, else <see langword="null"/></returns>
        public async Task<Error> UpdateCustomer(Customer customer, CustomerUpdateDto customerUpdateDto)
        {
            customer = UpdateCustomerInfo(customer, customerUpdateDto);

            return await PersistChanges();
        }

        #region Cars

        /// <summary>
        /// Adds a car
        /// </summary>
        /// <param name="customer"><see langword="Customer"/> to add the car to</param>
        /// <param name="car"><see langword="Car"/> to add</param>
        /// <returns><see langword="Error"/> if fails, else <see langword="null"/></returns>
        public async Task<Error> AddCar(Customer customer, Car car)
        {
            customer.Cars.Add(car);
            return await PersistChanges();
        }

//        /// <summary>
//        /// Updates a car
//        /// </summary>
//        /// <param name="car"><see langword="Car"/> to update</param>
//        /// <param name="updateCarDto">The <see langword="UpdateCarDto"/> containing the parameters to edit it to</param>
//        /// <returns><see langword="Error"/> if fails, else <see langword="null"/></returns>
//        public async Task<Error> UpdateCar(Car car, UpdateCarDto updateCarDto)
//        {
//            car.LicensePlate = updateCarDto.NewLicensePlate;
//            return await PersistChanges();
//        }

        /// <summary>
        /// Deletes a car
        /// </summary>
        /// <param name="car"><see langword="Car"/> to delete</param>
        /// <returns><see langword="Error"/> if fails, else <see langword="null"/></returns>
        public async Task<Error> DeleteCar(Car car)
        {
            Context.Cars.Remove(car);
            return await PersistChanges();
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Updates the user info
        /// </summary>
        /// <param name="userToEdit"><see langword="Customer"/> to edit</param>
        /// <param name="customerUpdateDto"><see langword="CustomerUpdateDto"/> fields to update to</param>
        /// <returns><see langword="Customer"/> updated user</returns>
        private static Customer UpdateCustomerInfo(Customer userToEdit, CustomerUpdateDto customerUpdateDto)
        {
            userToEdit.AccountInfo.FirstName = customerUpdateDto.FirstName;
            userToEdit.AccountInfo.LastName = customerUpdateDto.LastName;
            userToEdit.AccountInfo.IBAN = customerUpdateDto.IBAN;

            userToEdit.AddressInfo.Address = customerUpdateDto.Address;
            userToEdit.AddressInfo.Zipcode = customerUpdateDto.Zipcode;

            return userToEdit;
        }

        /// <summary>
        /// Updates the user info
        /// </summary>
        /// <remarks>
        /// Used by an admin
        /// </remarks>
        /// <param name="userToEdit"><see langword="Customer"/> to edit</param>
        /// <param name="customerAdminUpdateDto"><see langword="CustomerAdminUpdateDto"/> fields to update to</param>
        /// <returns><see langword="Customer"/> updated user</returns>
        private static Customer UpdateCustomerInfoAdmin(Customer userToEdit,
            CustomerAdminUpdateDto customerAdminUpdateDto)
        {
            userToEdit = UpdateCustomerInfo(userToEdit, customerAdminUpdateDto);

            userToEdit.Email = userToEdit.UserName = customerAdminUpdateDto.Email;
            userToEdit.NormalizedEmail = userToEdit.NormalizedUserName = customerAdminUpdateDto.Email;
            userToEdit.EmailConfirmed = customerAdminUpdateDto.EmailConfirmed;

            userToEdit.LockoutEnd = (customerAdminUpdateDto.IsLockedOut)
                ? DateTimeOffset.Now.AddYears(Constants.Numbers.LockOut.LockoutYears)
                : DateTimeOffset.Now;

            return userToEdit;
        }

        /// <summary>Sends the email to confirm the email of the user.</summary>
        ///
        /// <param name="user">The <see langword="User"/> to send the email to.</param>
        /// <param name="urlHelper">The <see langword="IUrlHelper"/> to use.</param>
        /// <param name="requestScheme">The <see langword="string"/> requestScheme to use.</param>
        /// <returns><see langword="Error"/> if applicable, else <see langword="null"/></returns>
        private async Task<Error> SendEmailConfirmEmail(User user, IUrlHelper urlHelper, string requestScheme)
        {
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = urlHelper.Action(
                "ConfirmEmail",
                "User",
                new {userId = user.Id, token},
                requestScheme);

            return await _emailSenderService.SendEmailAsync(user.Email, "Confirm your email",
                $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");
        }

        /// <summary>Sends the email to confirm the change of email of the user.</summary>
        ///
        /// <param name="user">The <see langword="User"/> to send the email to.</param>
        /// <param name="newEmail">The <see langword="string"/> new email of the user.</param>
        /// <param name="urlHelper">The <see langword="IUrlHelper"/> to use.</param>
        /// <param name="requestScheme">The <see langword="string"/> requestScheme to use.</param>
        ///
        /// <returns><see langword="Error"/> if applicable, else <see langword="null"/></returns>
        private async Task<Error> SendChangeEmailConfirmEmail(User user, string newEmail, IUrlHelper urlHelper,
            string requestScheme)
        {
            var token = await _userManager.GenerateChangeEmailTokenAsync(user, newEmail);
            var callbackUrl = urlHelper.Action(
                "ChangeEmailToken",
                "User",
                new {token, oldEmail = user.UserName, newEmail},
                requestScheme);

            return await _emailSenderService.SendEmailAsync(user.Email, "Confirm your new email",
                $"Please confirm your new email by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");
        }

        #endregion
    }
}