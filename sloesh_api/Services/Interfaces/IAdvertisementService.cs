using System.Collections.Generic;
using System.Threading.Tasks;
using sloesh_api.CustomErrors;
using sloesh_api.Dtos.Advertisement;
using sloesh_api.Models.Advertisements;
using sloesh_api.Models.Car;
using sloesh_api.Models.User;
using sloesh_api.Models.User.Customer;

namespace sloesh_api.Services.Interfaces
{
    public interface IAdvertisementService
    {
        Task<Advertisement> FindByIdAsync(int id);
        Task<List<Advertisement>> GetAllAdvertisements();
        Task<List<Advertisement>> GetNewestAvailableAdvertisements();
        Task<List<Advertisement>> GetAllAvailableAdvertisements();
        Task<Advertisement> FindByIdWithRelationsAsync(int id);
        Task<Error> CreateAdvertisement(Admin admin, Advertisement advertisement);
        Task<Error> UpdateAdvertisement(Admin admin, Advertisement advertisement, AdvertisementDto advertisementDto);
        Task<Error> SubscribeAdvertisement(Customer customer, Car car, Advertisement advertisement);
    }
}