﻿using System.Collections.Generic;
using System.Threading.Tasks;
using sloesh_api.Models.User;

namespace sloesh_api.Services.Interfaces
{
    public interface IAdminFetchService
    {
        Task<Admin> FindByIdWithClaimsAsync(string userId);
        Task<Admin> FindByNameWithClaimsAsync(string userName);
        Task<List<Admin>> GetAllAdmins();
    }
}