﻿using System.Threading.Tasks;
using sloesh_api.CustomErrors;
using sloesh_api.Dtos.Admin;
using sloesh_api.Models.User;

namespace sloesh_api.Services.Interfaces
{
    public interface IAdminUpdateService
    {
        Task<Error> UpdateAdmin(Admin adminToEdit, UpdateAdminDto updateAdminDto);
        Task<Error> CreateAdmin(CreateAdminDto createAdminDto);
    }
}