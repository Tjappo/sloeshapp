﻿using System.Threading.Tasks;
using sloesh_api.Dtos.User.Identity;
using sloesh_api.CustomErrors;
using sloesh_api.Dtos.User.Admin;
using sloesh_api.Models.User;
using Microsoft.AspNetCore.Mvc;
using sloesh_api.Dtos.Car;
using sloesh_api.Models.Car;
using sloesh_api.Models.User.Customer;

namespace sloesh_api.Services.Interfaces
{
    public interface ICustomerUpdateService
    {
        Task<Error> RegisterCustomer(Customer customer, string password, string roles, IUrlHelper urlHelper,
            string requestScheme);
        Task<Error> SendForgotPasswordEmail(User user, IUrlHelper urlHelper, string requestScheme);
        Task<Error> UpdateEmail(User user, UpdateEmailDto updateEmailDto, IUrlHelper urlHelper, string requestScheme);
        Task<Error> ChangeEmail(User user, ChangeEmailDto changeEmailDto);
        Task<Error> UpdateCustomer(Customer customer, CustomerUpdateDto customerUpdateDto);
        Task<Error> UpdateCustomerAdmin(Customer customer, CustomerAdminUpdateDto customerAdminUpdateDto);
        Task<Error> AddCar(Customer customer, Car car);
//        Task<Error> UpdateCar(Car car, UpdateCarDto updateCarDto);
        Task<Error> DeleteCar(Car car);
    }
}