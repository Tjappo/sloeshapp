using System.Collections.Generic;
using System.Threading.Tasks;
using sloesh_api.CustomErrors;
using sloesh_api.Dtos.Car;
using sloesh_api.Models.Car;

namespace sloesh_api.Services.Interfaces
{
    public interface ICarTypeService
    {
        Task<List<CarType>> GetAllCarTypes();
        Task<CarType> GetCarTypeById(int carTypeId);
        Task<Error> AddCarType(CarType carType);
        Task<Error> UpdateCarType(CarType carType, UpdateCarTypeDto updateCarTypeDto);
        Task<Error> DeleteCarType(CarType carType);
    }
}