﻿using System.Threading.Tasks;
using sloesh_api.CustomErrors;

namespace sloesh_api.Services.Interfaces
{
    public interface IEmailSenderService
    {
        Task<Error> SendEmailAsync(string email, string subject, string message);
    }
}