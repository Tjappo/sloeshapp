﻿using System.Collections.Generic;
using System.Threading.Tasks;
using sloesh_api.CustomErrors;
using sloesh_api.Models.Car;
using sloesh_api.Models.User.Customer;

namespace sloesh_api.Services.Interfaces
{
    public interface ICarService
    {
        Task<List<Car>> GetUnassignedCars();
        Task<Error> AddCar(Customer customer, Car car);
//        Task<Error> UpdateCar(Car car, UpdateCarDto updateCarDto);
        Task<Error> DeleteCar(Car car);
    }
}