﻿using System.Collections.Generic;
using System.Threading.Tasks;
using sloesh_api.Models.User;
using sloesh_api.Models.User.Customer;
using sloesh_api.Models.ViewModels;

namespace sloesh_api.Services.Interfaces
{
    public interface ICustomerFetchService
    {
        Task<Customer> FindByIdWithRelationsAsync(string userId);
        Task<Customer> FindByIdWithPersonalCarsAdvertisementsAsync(string userId);
        Task<Customer> FindByIdWithCarsAdvertisementsAsync(string userId);
        Task<Customer> FindByIdWithCarsAsync(string userId);
        Task<Customer> FindByIdWithClaimsAsync(string userId);
        Task<Customer> FindByNameWithClaimsAsync(string userName);
        Task<Customer> FindByNameWithRelationsAsync(string userName);
        Task<List<Customer>> GetAllCustomers();
    }
}