﻿using System.Net;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;
using sloesh_api.CustomErrors;
using sloesh_api.Helpers;
using sloesh_api.Services.Interfaces;

namespace sloesh_api.Services
{
    public class EmailSenderService : IEmailSenderService
    {
        public EmailSenderService(IOptions<AuthMessageSenderOptions> optionsAccessor)
        {
            Options = optionsAccessor.Value;
        }

        public AuthMessageSenderOptions Options { get; } //set only via Secret Manager

        public async Task<Error> SendEmailAsync(string email, string subject, string message)
        {
            return await Execute(Options, subject, message, email);
        }

        private static async Task<Error> Execute(AuthMessageSenderOptions options, string subject, string message,
            string email)
        {
            var client = new SendGridClient(options.ApiKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(options.FromEmail, options.FromName),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = message
            };
            msg.AddTo(new EmailAddress(email));

            // Disable click tracking.
            // See https://sendgrid.com/docs/User_Guide/Settings/tracking.html
            msg.TrackingSettings = new TrackingSettings
            {
                ClickTracking = new ClickTracking {Enable = false}
            };

            var response = await client.SendEmailAsync(msg);

            if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Accepted)
                return new MailError();

            return null;
        }
    }
}