﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using sloesh_api.Database;
using sloesh_api.CustomErrors;
using sloesh_api.Services.Interfaces;
using sloesh_api.Models.Car;
using sloesh_api.Models.User.Customer;

namespace sloesh_api.Services
{
    public class CarService : ServiceBase, ICarService
    {
        public CarService(
            ApplicationDbContext context
        ) : base(context)
        {
        }

        /// <summary>
        /// Gets all the cars that have not yet any car types assigned to
        /// </summary>
        /// <returns><see langword="List"/> of <see langword="Car"/> that have no car types assigned.</returns>
        public Task<List<Car>> GetUnassignedCars()
        {
            return Context.Cars
                .Include(c => c.CarTypeCars)
                .Where(c => c.CarTypeCars == null)
                .ToListAsync();
        }

        /// <summary>
        /// Adds a car
        /// </summary>
        /// <param name="customer"><see langword="Customer"/> to add the car to</param>
        /// <param name="car"><see langword="Car"/> to add</param>
        /// <returns><see langword="Error"/> if fails, else <see langword="null"/></returns>
        public async Task<Error> AddCar(Customer customer, Car car)
        {
            customer.Cars.Add(car);
            return await PersistChanges();
        }

//        /// <summary>
//        /// Updates a car
//        /// </summary>
//        /// <param name="car"><see langword="Car"/> to update</param>
//        /// <param name="updateCarDto">The <see langword="UpdateCarDto"/> containing the parameters to edit it to</param>
//        /// <returns><see langword="Error"/> if fails, else <see langword="null"/></returns>
//        public async Task<Error> UpdateCar(Car car, UpdateCarDto updateCarDto)
//        {
//            car.LicensePlate = updateCarDto.NewLicensePlate;
//            return await PersistChanges();
//        }

        /// <summary>
        /// Deletes a car
        /// </summary>
        /// <param name="car"><see langword="Car"/> to delete</param>
        /// <returns><see langword="Error"/> if fails, else <see langword="null"/></returns>
        public async Task<Error> DeleteCar(Car car)
        {
            Context.Cars.Remove(car);
            return await PersistChanges();
        }
    }
}