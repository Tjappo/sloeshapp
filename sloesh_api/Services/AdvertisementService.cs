using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using sloesh_api.CustomErrors;
using sloesh_api.Database;
using sloesh_api.Dtos.Advertisement;
using sloesh_api.Helpers;
using sloesh_api.Models.Advertisements;
using sloesh_api.Models.Car;
using sloesh_api.Models.User;
using sloesh_api.Models.User.Customer;
using sloesh_api.Services.Interfaces;
using Action = sloesh_api.Models.Enums.Advertisement.Action;

namespace sloesh_api.Services
{
    public class AdvertisementService : ServiceBase, IAdvertisementService
    {
        public AdvertisementService(
            ApplicationDbContext context
        ) : base(context)
        {
        }

        /// <summary>Finds and returns an advertisement, if any, who has the specified <paramref name="id"/>.</summary>
        ///
        /// <param name="id">The ID to search for.</param>
        ///
        /// <returns>The <see cref="T:System.Threading.Tasks.Task" /> that represents the asynchronous operation,
        /// containing the advertisement matching the specified <paramref name="id"/> if it exists.</returns>
        public Task<Advertisement> FindByIdAsync(int id)
        {
            return Context.Advertisements
                .Include(a => a.CarTypeAdvertisements)
                .FirstOrDefaultAsync(advertisement => advertisement.Id == id);
        }

        /// <summary>Gets all advertisements.</summary>
        ///
        /// <returns>A task containing a list of <see langword="Advertisement"/>.</returns>
        public Task<List<Advertisement>> GetAllAdvertisements()
        {
            return Context.Advertisements
                .ToListAsync();
        }
        
        /// <summary>Gets newest available advertisements.</summary>
        ///
        /// <remarks>
        /// Have an availability of at least one week
        /// Also includes the relation <see langword="CarType"/>
        /// Maximum of 4 results
        /// </remarks>
        ///
        /// <returns>A task containing a list of <see langword="Advertisement"/>.</returns>
        public Task<List<Advertisement>> GetNewestAvailableAdvertisements()
        {
            var endDate = DateTime.Today.AddDays(Constants.Numbers.Days.Weeks);
            return Context.Advertisements
                .Where(a => a.End >= endDate)
                .Include(a => a.CarTypeAdvertisements)
                .ThenInclude(cta => cta.CarType)
                .Include(a => a.History)
                .OrderByDescending(a => a.History.Last().CreatedAt)
                .Take(4)
                .ToListAsync();
        }

        /// <summary>Gets all available advertisements.</summary>
        ///
        /// <remarks>
        /// Have an availability of at least one week
        /// Also includes the relation <see langword="CarType"/>
        /// </remarks>
        ///
        /// <returns>A task containing a list of <see langword="Advertisement"/>.</returns>
        public Task<List<Advertisement>> GetAllAvailableAdvertisements()
        {
            var endDate = DateTime.Today.AddDays(Constants.Numbers.Days.Weeks);
            return Context.Advertisements
                .Where(a => a.End >= endDate)
                .Include(a => a.CarTypeAdvertisements)
                .ThenInclude(cta => cta.CarType)
                .ToListAsync();
        }

        /// <summary>Finds and returns an advertisement, if any, who has the specified <paramref name="id"/>.</summary>
        ///
        /// <param name="id">The ID to search for.</param>
        ///
        /// <returns>The <see cref="T:System.Threading.Tasks.Task" /> that represents the asynchronous operation,
        /// containing the advertisement matching the specified <paramref name="id"/> if it exists.</returns>
        public Task<Advertisement> FindByIdWithRelationsAsync(int id)
        {
            return Context.Advertisements
                .Include(a => a.CarAdvertisements)
                .Include(a => a.History)
                .ThenInclude(h => h.Admin)
                .Include(a => a.CarTypeAdvertisements)
                .ThenInclude(cta => cta.CarType)
                .FirstOrDefaultAsync(advertisement => advertisement.Id == id);
        }

        /// <summary>
        /// Creates an advertisement
        /// </summary>
        /// <param name="admin"><see langword="Admin"/> who created the advertisement</param>
        /// <param name="advertisement"><see langword="Advertisement"/> to add to the database</param>
        /// <returns><see langword="Error"/> if any, else <see langword="null"/></returns>
        public async Task<Error> CreateAdvertisement(Admin admin, Advertisement advertisement)
        {
            Context.Advertisements.Add(advertisement);
            await PersistChanges();
            advertisement.History.Add(new HistoryAction
            {
                AdminId = admin.Id,
                AdvertisementId = advertisement.Id,
                Action = Action.Create,
                CreatedAt = DateTime.Now
            });
            return await PersistChanges();
        }

        /// <summary>
        /// Updates an advertisement
        /// </summary>
        /// <param name="admin"><see langword="Admin"/> who created the advertisement</param>
        /// <param name="advertisement"><see langword="Advertisement"/> to update</param>
        /// <param name="advertisementDto"><see langword="AdvertisementDto"/> containing the parameters to update</param>
        /// <returns><see langword="Error"/> if any, else <see langword="null"/></returns>
        public Task<Error> UpdateAdvertisement(Admin admin, Advertisement advertisement,
            AdvertisementDto advertisementDto)
        {
            advertisement.History.Add(new HistoryAction
            {
                AdminId = admin.Id,
                AdvertisementId = advertisement.Id,
                Action = Action.Update,
                CreatedAt = DateTime.Now
            });
            advertisement.Title = advertisementDto.Title;
            advertisement.Compensation = advertisementDto.Compensation;
            advertisement.Duration = advertisementDto.Duration;
            advertisement.End = advertisementDto.End;
            advertisement.Start = advertisementDto.Start;
            advertisement.Logo = advertisementDto.Logo;
            advertisement.Image = advertisementDto.Image;
            return PersistChanges();
        }

        /// <summary>
        /// Subscribes a customer to an advertisement
        /// </summary>
        /// <param name="customer"><see langword="Customer"/> that is subscribing</param>
        /// <param name="car"><see langword="Car"/> that is subscribing</param>
        /// <param name="advertisement"><see langword="Advertisement"/> to subscribe to</param>
        /// <returns><see langword="Error"/> if any, else <see langword="null"/></returns>
        public Task<Error> SubscribeAdvertisement(Customer customer, Car car, Advertisement advertisement)
        {
            var today = DateTime.Now;
            var endDate = today.AddDays(Constants.Numbers.Days.Weeks * advertisement.Duration);
            endDate = (advertisement.End < endDate) ? advertisement.End : endDate;

            customer.Advertisements.Add(new CustomerAdvertisement
            {
                CarId = car.Id,
                StartDate = DateTime.Now,
                EndDate = endDate,
                AdvertisementId = advertisement.Id
            });
            return PersistChanges();
        }
    }
}