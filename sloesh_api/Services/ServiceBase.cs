using System;
using System.Threading.Tasks;
using sloesh_api.CustomErrors;
using sloesh_api.Database;

namespace sloesh_api.Services
{
    public class ServiceBase
    {
        protected readonly ApplicationDbContext Context;

        public ServiceBase(
            ApplicationDbContext context
        )
        {
            Context = context;
        }

        /// <summary>
        /// Persists the changes in the database
        /// </summary>
        /// <returns><see langword="Error"/> if fails, else <see langword="null"/></returns>
        protected async Task<Error> PersistChanges()
        {
            try
            {
                // Add to database
                await Context.SaveChangesAsync();
                return null;
            }
            catch (Exception)
            {
                return new DatabaseError();
            }
        }
    }
}