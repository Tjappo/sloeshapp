using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using sloesh_api.Database;
using sloesh_api.Helpers;
using sloesh_api.Models.User;
using sloesh_api.Services.Interfaces;

namespace sloesh_api.Services
{
    public class AdminFetchService : IAdminFetchService
    {
        private readonly ApplicationDbContext _context;

        public AdminFetchService(
            ApplicationDbContext context
        )
        {
            _context = context;
        }

        /// <summary>Finds and returns an admin, if any, who has the specified <paramref name="userName"/>.</summary>
        ///
        /// <param name="userName">The userName to search for.</param>
        ///
        /// <returns>The <see cref="T:System.Threading.Tasks.Task" /> that represents the asynchronous operation,
        /// containing the admin matching the specified <paramref name="userName" /> if it exists,
        /// including the collection of <see langword="IdentityClaim"/> the admin has.</returns>
        public Task<Admin> FindByNameWithClaimsAsync(string userName)
        {
            return _context.Admins
                .Include(user => user.Claims)
                .FirstOrDefaultAsync(user => string.Equals(user.UserName, userName));
        }

        /// <summary>Finds and returns a user, if any, who has the specified <paramref name="userId"/>.</summary>
        ///
        /// <param name="userId">The user ID to search for.</param>
        ///
        /// <returns>The <see cref="T:System.Threading.Tasks.Task" /> that represents the asynchronous operation,
        /// containing the admin matching the specified <paramref name="userId" /> if it exists,
        /// including the collection of <see langword="IdentityClaim"/> the admin has.</returns>
        public Task<Admin> FindByIdWithClaimsAsync(string userId)
        {
            return _context.Admins
                .Include(user => user.Claims)
                .FirstOrDefaultAsync(user => string.Equals(user.Id, userId));
        }

        /// <summary>Gets all admins.</summary>
        ///
        /// <returns>A task containing a <see langword="List"/> of <see langword="Admin"/>.</returns>
        public async Task<List<Admin>> GetAllAdmins()
        {
            return await _context.Admins
                .ToListAsync();
        }
    }
}