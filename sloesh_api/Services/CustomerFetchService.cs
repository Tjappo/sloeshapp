﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using sloesh_api.Database;
using sloesh_api.Helpers;
using sloesh_api.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using sloesh_api.Models.User.Customer;

namespace sloesh_api.Services
{
    public class CustomerFetchService : ICustomerFetchService
    {
        private readonly ApplicationDbContext _context;

        public CustomerFetchService(
            ApplicationDbContext context
        )
        {
            _context = context;
        }

        /// <summary>Finds and returns a user, if any, who has the specified <paramref name="userId" />.</summary>
        ///
        /// <remarks>Including the <see langword="AddressInfo"/>, <see langword="AccountInfo"/>, <see langword="Car"/>
        /// and <see langword="Claim"/> relations</remarks>
        ///
        /// <param name="userId">The <see langword="string"/> userId to get the data from.</param>
        ///
        /// <returns>The <see cref="T:System.Threading.Tasks.Task" /> that represents the asynchronous operation,
        /// containing the user matching the specified <paramref name="userId" /> if it exists,
        /// including the collection of <see langword="IdentityClaim"/> the user has.</returns>
        public Task<Customer> FindByIdWithRelationsAsync(string userId)
        {
            return _context.Customers
                .Include(user => user.Claims)
                .Include(user => user.AccountInfo)
                .Include(user => user.AddressInfo)
                .Include(user => user.Cars)
                .FirstOrDefaultAsync(user => string.Equals(user.Id, userId));
        }

        /// <summary>Finds and returns a user, if any, who has the specified <paramref name="userId" />.</summary>
        ///
        /// <remarks>Including the <see langword="Car"/> and <see langword="CarAdvertisement"/> relations.</remarks>
        ///
        /// <param name="userId">The <see langword="string"/> userId to get the data from.</param>
        ///
        /// <returns>The <see cref="T:System.Threading.Tasks.Task" /> that represents the asynchronous operation,
        /// containing the user matching the specified <paramref name="userId" /> if it exists,
        /// including the collection of <see langword="IdentityClaim"/> the user has.</returns>
        public Task<Customer> FindByIdWithCarsAdvertisementsAsync(string userId)
        {
            return _context.Customers
                .Include(user => user.Cars)
                .ThenInclude(c => c.CarTypeCars)
                .Include(user => user.Advertisements)
                .FirstOrDefaultAsync(user => string.Equals(user.Id, userId));
        }
        
        /// <summary>Finds and returns a user, if any, who has the specified <paramref name="userId" />.</summary>
        ///
        /// <remarks>Including the <see langword="AccountInfo"/>, <see langword="AddressInfo"/>, <see langword="Car"/>,
        /// and <see langword="CarAdvertisement"/> relations.</remarks>
        ///
        /// <param name="userId">The <see langword="string"/> userId to get the data from.</param>
        ///
        /// <returns>The <see cref="T:System.Threading.Tasks.Task" /> that represents the asynchronous operation,
        /// containing the user matching the specified <paramref name="userId" /> if it exists,
        /// including the collection of <see langword="IdentityClaim"/> the user has.</returns>
        public Task<Customer> FindByIdWithPersonalCarsAdvertisementsAsync(string userId)
        {
            return _context.Customers
                .Include(user => user.AccountInfo)
                .Include(user => user.AddressInfo)
                .Include(user => user.Cars)
                .ThenInclude(c => c.CarTypeCars)
                .Include(user => user.Advertisements)
                .FirstOrDefaultAsync(user => string.Equals(user.Id, userId));
        }
        
        /// <summary>Finds and returns a user, if any, who has the specified <paramref name="userId" />.</summary>
        ///
        /// <remarks>Including the <see langword="Car"/> relation.</remarks>
        ///
        /// <param name="userId">The <see langword="string"/> userId to get the data from.</param>
        ///
        /// <returns>The <see cref="T:System.Threading.Tasks.Task" /> that represents the asynchronous operation,
        /// containing the user matching the specified <paramref name="userId" /> if it exists,
        /// including the collection of <see langword="IdentityClaim"/> the user has.</returns>
        public Task<Customer> FindByIdWithCarsAsync(string userId)
        {
            return _context.Customers
                .Include(user => user.Cars)
                .FirstOrDefaultAsync(user => string.Equals(user.Id, userId));
        }

        /// <summary>Finds and returns a user, if any, who has the specified <paramref name="userId" />.</summary>
        ///
        /// <param name="userId">The user ID to search for.</param>
        ///
        /// <returns>The <see cref="T:System.Threading.Tasks.Task" /> that represents the asynchronous operation,
        /// containing the user matching the specified <paramref name="userId" /> if it exists,
        /// including the collection of <see langword="IdentityClaim"/> the user has.</returns>
        public Task<Customer> FindByIdWithClaimsAsync(string userId)
        {
            return _context.Customers
                .Include(user => user.Claims)
                .FirstOrDefaultAsync(user => string.Equals(user.Id, userId));
        }

        /// <summary>Finds and returns a user, if any, who has the specified <paramref name="userName" />.</summary>
        ///
        /// <param name="userName">The userName to search for.</param>
        ///
        /// <returns>The <see cref="T:System.Threading.Tasks.Task" /> that represents the asynchronous operation,
        /// containing the user matching the specified <paramref name="userName" /> if it exists,
        /// including the collection of <see langword="IdentityClaim"/> the user has.</returns>
        public Task<Customer> FindByNameWithClaimsAsync(string userName)
        {
            return _context.Customers
                .Include(user => user.Claims)
                .FirstOrDefaultAsync(user => string.Equals(user.UserName, userName));
        }

        /// <summary>Finds and returns a user, if any, who has the specified <paramref name="userName" />.</summary>
        ///
        /// <remarks>Including the <see langword="AddressInfo"/>, <see langword="AccountInfo"/>, <see langword="Car"/>
        /// and <see langword="Claim"/> relations</remarks>
        ///
        /// <param name="userName">The userName to search for.</param>
        ///
        /// <returns>The <see cref="T:System.Threading.Tasks.Task" /> that represents the asynchronous operation,
        /// containing the user matching the specified <paramref name="userName" /> if it exists,
        /// including the collection of <see langword="IdentityClaim"/> the user has.</returns>
        public Task<Customer> FindByNameWithRelationsAsync(string userName)
        {
            return _context.Customers
                .Include(user => user.Claims)
                .Include(user => user.AccountInfo)
                .Include(user => user.AddressInfo)
                .Include(user => user.Cars)
                .FirstOrDefaultAsync(user => string.Equals(user.UserName, userName));
        }

        /// <summary>Gets all users with the api access claim.</summary>
        ///
        /// <returns>A task containing a list of <see langword="User"/> with the api access claim.</returns>
        public async Task<List<Customer>> GetAllCustomers()
        {
            return await _context.Customers
                .Include(user => user.Claims)
                .Include(user => user.AccountInfo)
                .Include(user => user.AddressInfo)
                .Include(user => user.Cars)
                .Where(user =>
                    user.Claims.Any(claim =>
                        string.Equals(claim.ClaimType, Constants.Strings.JwtClaimIdentifiers.Roles) &&
                        (string.Equals(claim.ClaimValue, Constants.Strings.JwtClaims.ApiAccess) ||
                         string.Equals(claim.ClaimValue, Constants.Strings.JwtClaims.UnvalidatedAccess))
                    )
                )
                .ToListAsync();
        }
    }
}