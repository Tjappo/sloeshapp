﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using sloesh_api.CustomErrors;
using sloesh_api.Database;
using sloesh_api.Dtos.Admin;
using sloesh_api.Helpers;
using sloesh_api.Models.User;
using sloesh_api.Services.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace sloesh_api.Services
{
    public class AdminUpdateService : ServiceBase, IAdminUpdateService
    {
        private readonly UserManager<User> _userManager;

        public AdminUpdateService(
            ApplicationDbContext context,
            UserManager<User> userManager
        ) : base(context)
        {
            _userManager = userManager;
        }

        /// <summary>
        /// Updates the given admin with the given parameters
        /// </summary>
        /// <param name="adminToEdit"><see langword="Admin"/> admin to edit.</param>
        /// <param name="updateAdminDto"><see langword="UpdateAdminDto"/> updated parameters.</param>
        /// <returns><see langword="Error"/> if fails, else <see langword="null"/></returns>
        public async Task<Error> UpdateAdmin(Admin adminToEdit, UpdateAdminDto updateAdminDto)
        {
            adminToEdit.DisplayName = updateAdminDto.DisplayName;
            adminToEdit.UserName = updateAdminDto.UserName;
            adminToEdit.Email = updateAdminDto.UserName;
            adminToEdit.LockoutEnd = (updateAdminDto.IsLockedOut)
                ? DateTimeOffset.Now.AddYears(Constants.Numbers.LockOut.LockoutYears)
                : DateTimeOffset.Now;

            return await PersistChanges();
        }

        /// <summary>
        /// Creates a new admin
        /// </summary>
        /// <param name="createAdminDto"><see langword="CreateAdminDto"/> containing parameters to create the new admin with</param>
        /// <returns>An <see langword="ArrayList"/> of <see langword="Error"/> if fails, else <see langword="null"/></returns>
        public async Task<Error> CreateAdmin(CreateAdminDto createAdminDto)
        {
            var admin = CreateAdminObject(createAdminDto.UserName, createAdminDto.DisplayName);

            var result = await _userManager.CreateAsync(admin, createAdminDto.Password);

            if (!result.Succeeded)
                return new CustomIdentityError();

            await _userManager.AddClaimAsync(admin,
                new Claim(Constants.Strings.JwtClaimIdentifiers.Roles, Constants.Strings.JwtClaims.AdminAccess));

            return await PersistChanges();
        }

        #region Helpers

        /// <summary>
        /// Creates a new admin
        /// </summary>
        /// <param name="userName"><see langword="string"/> user name to use</param>
        /// <param name="displayName"><see langword="string"/> display name to use</param>
        /// <returns>Created <see langword="Admin"/></returns>
        private static Admin CreateAdminObject(string userName, string displayName)
        {
            return new Admin
            {
                DisplayName = displayName,
                UserName = userName,
                Email = userName,
                EmailConfirmed = true
            };
        }

        #endregion
    }
}