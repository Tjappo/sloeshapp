using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using sloesh_api.CustomErrors;
using sloesh_api.Database;
using sloesh_api.Dtos.Car;
using sloesh_api.Models.Car;
using sloesh_api.Services.Interfaces;

namespace sloesh_api.Services
{
    public class CarTypeService : ServiceBase, ICarTypeService
    {
        public CarTypeService(ApplicationDbContext context) : base(context)
        {
        }

        /// <summary>Gets all car types.</summary>
        ///
        /// <returns>A task containing a <see langword="List"/> of <see langword="CarType"/>.</returns>
        public async Task<List<CarType>> GetAllCarTypes()
        {
            return await Context.CarTypes.ToListAsync();
        }

        /// <summary>
        /// Get car type by id
        /// </summary>
        /// <param name="carTypeId"><see langword="int"/> id of the car type</param>
        /// 
        /// <returns>The <see cref="T:System.Threading.Tasks.Task" /> that represents the asynchronous operation,
        /// containing the <see langword="CarType"/> matching the specified <paramref name="carTypeId"/> if it exists.
        /// </returns>
        public async Task<CarType> GetCarTypeById(int carTypeId)
        {
            return await Context.CarTypes.FirstOrDefaultAsync(ct => ct.Id == carTypeId);
        }

        /// <summary>
        /// Adds a car type
        /// </summary>
        /// <param name="carType"><see langword="CarType"/> to add</param>
        /// <returns><see langword="Error"/> if fails, else <see langword="null"/></returns>
        public async Task<Error> AddCarType(CarType carType)
        {
            Context.CarTypes.Add(carType);
            return await PersistChanges();
        }

        /// <summary>
        /// Updates a car type
        /// </summary>
        /// <param name="carType"><see langword="CarType"/> to update</param>
        /// <param name="updateCarTypeDto">The <see langword="UpdateCarTypeDto"/> containing the parameters to edit it to</param>
        /// <returns><see langword="Error"/> if fails, else <see langword="null"/></returns>
        public async Task<Error> UpdateCarType(CarType carType, UpdateCarTypeDto updateCarTypeDto)
        {
            carType.Name = updateCarTypeDto.Name;
            carType.Description = updateCarTypeDto.Description;
            return await PersistChanges();
        }

        /// <summary>
        /// Deletes a car type
        /// </summary>
        /// <param name="carType"><see langword="CarType"/> to delete</param>
        /// <returns><see langword="Error"/> if fails, else <see langword="null"/></returns>
        public async Task<Error> DeleteCarType(CarType carType)
        {
            Context.CarTypes.Remove(carType);
            return await PersistChanges();
        }
    }
}