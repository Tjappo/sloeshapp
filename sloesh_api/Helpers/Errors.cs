﻿using System.Collections;
using System.Collections.Generic;
using sloesh_api.CustomErrors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace sloesh_api.Helpers
{
    public static class Errors
    {
        /// <summary>Updates the ModelStateDictionary, given the IdentityResult errors</summary>
        ///
        /// <param name="errors">The <see langword="IdentityResult"/> errors to add.</param>
        /// <param name="modelState">The current <see langword="ModelStateDictionary"/>.</param>
        ///
        /// <returns>The updated <see langword="ModelStateDictionary"/></returns>
        public static ModelStateDictionary AddErrorsToModelState(IEnumerable errors,
            ModelStateDictionary modelState)
        {
            foreach (Error e in errors)
            {
                modelState.TryAddModelError(e.Code, e.Description);
            }

            return modelState;
        }

        /// <summary>Updates the ModelStateDictionary, given a custom code and descriptions</summary>
        ///
        /// <param name="code">The error code <see langword="string"/>.</param>
        /// <param name="descriptions">The error descriptions <see langword="IEnumerable"/>.</param>
        /// <param name="modelState">The current <see langword="ModelStateDictionary"/>.</param>
        ///
        /// <returns>The updated <see langword="ModelStateDictionary"/></returns>
        public static ModelStateDictionary AddErrorsToModelState(string code, IEnumerable<string> descriptions,
            ModelStateDictionary modelState)
        {
            foreach (var description in descriptions)
            {
                modelState.TryAddModelError(code, description);
            }

            return modelState;
        }

        /// <summary>Updates the ModelStateDictionary, given a custom code and decription</summary>
        ///
        /// <param name="code">The error code <see langword="string"/>.</param>
        /// <param name="description">The error description <see langword="string"/>.</param>
        /// <param name="modelState">The current <see langword="ModelStateDictionary"/>.</param>
        ///
        /// <returns>The updated <see langword="ModelStateDictionary"/></returns>
        public static ModelStateDictionary AddErrorToModelState(string code, string description,
            ModelStateDictionary modelState)
        {
            modelState.TryAddModelError(code, description);
            return modelState;
        }

        /// <summary>Updates the ModelStateDictionary, given an error</summary>
        ///
        /// <param name="error">The <see langword="Error"/>.</param>
        /// <param name="modelState">The current <see langword="ModelStateDictionary"/>.</param>
        ///
        /// <returns>The updated <see langword="ModelStateDictionary"/></returns>
        public static ModelStateDictionary AddErrorToModelState(Error error, ModelStateDictionary modelState)
        {
            modelState.TryAddModelError(error.Code, error.Description);
            return modelState;
        }

        /// <summary>Converts the list of <see langword="IdentityError"/> to a list of <see langname="Error"/></summary>
        ///
        /// <param name="errorsToConvert">The list of <see langword="IdentityError"/> to convert.</param>
        ///
        /// <returns>The converted <see langname="ArrayList"/> of <see langword="Error"/></returns>
        public static ArrayList ConvertIdentityErrorsToCustomErrors(IEnumerable<IdentityError> errorsToConvert)
        {
            var errors = new ArrayList();
            foreach (var error in errorsToConvert)
            {
                errors.Add(new Error(error));
            }

            return errors;
        }
    }
}