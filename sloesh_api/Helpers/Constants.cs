﻿using System.Linq;
using sloesh_api.Models.User;

namespace sloesh_api.Helpers
{
    public static class Constants
    {
        public static class Numbers
        {
            public static class LockOut
            {
                public const int LockoutYears = 200;
            }

            public static class Days
            {
                public const int Weeks = 7;
            }

            public static class Advertisement
            {
                public const int MinimumDuration = 1;
                public const int MaximumDuration = 4;

                public const float MinimumCompensation = 0.00f;
                public const float MaximumCompensation = float.MaxValue;
            }
        }

        public static class Strings
        {
            public static class JwtClaimIdentifiers
            {
                public const string Roles = "roles", Id = "id";
            }

            public static class JwtClaims
            {
                public const string UnvalidatedAccess = "unvalidated_access";
                public const string ApiAccess = "api_access";
                public const string AdminAccess = "admin_access";
            }

            public static class JwtPolicies
            {
                public const string UnvalidatedUser = "UnvalidatedUser";
                public const string ApiUser = "ApiUser";
                public const string AdminUser = "AdminUser";
            }

            public static class Errors
            {
                public const string IdentityError = "IdentityError";
                public const string MailError = "MailError";
                public const string DatabaseError = "DatabaseError";
                public const string UserNotFoundDescription = "User not found";
                public const string UserAlreadyExistsDescription = "User already exists";
                public const string ObjectAlreadyExistsDescription = "Object already exists";
                public const string ObjectNotFoundDescription = "Object not found";
                public const string InvalidAccessAttempts = "invalid_access_attempts";
                public const string SomethingWentWrong = "Something went wrong, please try again later";
                public const string InvalidTwoFactorCode = "Invalid Two Factor Authentication code";
            }
        }

        public static class Functions
        {
            public static class HelperMethods
            {
                public static bool ValidateAccess(User user, string claimValue)
                {
                    var userClaimValue =
                        user.Claims.First(claim => string.Equals(claim.ClaimType, Strings.JwtClaimIdentifiers.Roles))
                            .ClaimValue;
                    return string.Equals(userClaimValue, claimValue);
                }
            }
        }
    }
}