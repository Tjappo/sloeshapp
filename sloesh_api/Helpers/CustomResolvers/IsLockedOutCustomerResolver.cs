﻿using System;
using AutoMapper;
using sloesh_api.Models.User.Customer;
using sloesh_api.Models.ViewModels.Identity.Admin;

namespace sloesh_api.Helpers.CustomResolvers
{
    public class IsLockedOutCustomerResolver : IValueResolver<Customer, CustomerAdminViewModel, bool>
    {
        public bool Resolve(Customer source, CustomerAdminViewModel destination, bool destMember,
            ResolutionContext context)
        {
            if (!source.LockoutEnabled || source.LockoutEnd == null)
                return false;

            return source.LockoutEnd > DateTime.UtcNow;
        }
    }
}