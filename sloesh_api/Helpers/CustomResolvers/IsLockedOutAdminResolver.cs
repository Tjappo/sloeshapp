﻿using System;
using AutoMapper;
using sloesh_api.Models.User;
using sloesh_api.Models.ViewModels.Identity.Admin;

namespace sloesh_api.Helpers.CustomResolvers
{
    public class IsLockedOutAdminResolver : IValueResolver<Admin, AdminViewModel, bool>
    {
        public bool Resolve(Admin source, AdminViewModel destination, bool destMember,
            ResolutionContext context)
        {
            if (!source.LockoutEnabled || source.LockoutEnd == null)
                return false;

            return source.LockoutEnd > DateTime.UtcNow;
        }
    }
}