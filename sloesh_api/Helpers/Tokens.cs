﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using sloesh_api.Auth;
using sloesh_api.Models;
using Newtonsoft.Json;

namespace sloesh_api.Helpers
{
    public class Tokens
    {
        /// <summary>Generate a jwt token</summary>
        ///
        /// <param name="identity">The <see langword="ClaimsIdentity"/> to use.</param>
        /// <param name="jwtFactory">The <see langword="JwtFactory"/> to use.</param>
        /// <param name="userName">The username <see langword="string"/>.</param>
        /// <param name="displayName">The displayName <see langword="string"/>.</param>
        /// <param name="jwtOptions">The options of the jwt token <see langword="JwtIssuerOptions"/>.</param>
        /// <param name="serializerSettings">The <see langword="JsonSerializerSettings"/>.</param>
        ///
        /// <returns>The json object containing the generated jwt token</returns>
        public static async Task<string> GenerateJwt(ClaimsIdentity identity, IJwtFactory jwtFactory, string userName,
            string displayName, JwtIssuerOptions jwtOptions, JsonSerializerSettings serializerSettings)
        {
            var response = new
            {
                id = identity.Claims.Single(c => c.Type == "id").Value,
                displayName,
                roles = identity.Claims.Single(c => c.Type == Constants.Strings.JwtClaimIdentifiers.Roles).Value,
                auth_token = await jwtFactory.GenerateEncodedToken(userName, identity),
                expires_in = (int) jwtOptions.ValidFor.TotalSeconds
            };

            return JsonConvert.SerializeObject(response, serializerSettings);
        }
    }
}