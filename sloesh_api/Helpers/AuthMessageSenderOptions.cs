﻿namespace sloesh_api.Helpers
{
    public class AuthMessageSenderOptions
    {
        public string ApiKey { get; set; }
        public string FromEmail { get; set; }
        public string FromName { get; set; }
    }
}