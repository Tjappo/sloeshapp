﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using AutoMapper;
using sloesh_api.Dtos.Advertisement;
using sloesh_api.Dtos.User.Identity;
using sloesh_api.Helpers.CustomResolvers;
using sloesh_api.Models.Advertisements;
using sloesh_api.Models.Car;
using sloesh_api.Models.User;
using sloesh_api.Models.User.Customer;
using sloesh_api.Models.ViewModels.Advertisement;
using sloesh_api.Models.ViewModels.Car;
using sloesh_api.Models.ViewModels.Identity.Admin;
using sloesh_api.Models.ViewModels.Identity.Customer;

namespace sloesh_api.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Admin, AdminViewModel>()
                .ForMember(dest => dest.UserId, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.IsLockedOut, opts => opts.ResolveUsing<IsLockedOutAdminResolver>());

            CreateMap<HistoryAction, HistoryActionViewModel>()
                .ForMember(dest => dest.DisplayName, opts => opts.MapFrom(src => src.Admin.DisplayName));

            CreateMap<CustomerAdvertisement, CustomerAdvertisementViewModel>()
                .ForMember(dest => dest.UserName, opts => opts.MapFrom(src => src.Customer.UserName));

            CreateMap<Customer, CustomerViewModel>()
                .ForMember(dest => dest.FirstName, opts => opts.MapFrom(src => src.AccountInfo.FirstName))
                .ForMember(dest => dest.LastName, opts => opts.MapFrom(src => src.AccountInfo.LastName))
                .ForMember(dest => dest.Address, opts => opts.MapFrom(src => src.AddressInfo.Address))
                .ForMember(dest => dest.Zipcode, opts => opts.MapFrom(src => src.AddressInfo.Zipcode))
                .ForMember(dest => dest.Email, opts => opts.MapFrom(src => src.Email))
                .ForMember(dest => dest.IBAN, opts => opts.MapFrom(src => src.AccountInfo.IBAN))
                .ForMember(dest => dest.ActiveSince, opts => opts.MapFrom(src => src.CreatedAt));

            CreateMap<Customer, CustomerAdminViewModel>()
                .IncludeBase<Customer, CustomerViewModel>()
                .ForMember(dest => dest.Cars, opts => opts.MapFrom(src => src.Cars))
                .ForMember(dest => dest.UserId, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.IsLockedOut, opts => opts.ResolveUsing<IsLockedOutCustomerResolver>());

            CreateMap<CarType, CarTypeViewModel>();

            CreateMap<Car, CarViewModel>();

            CreateMap<RegisterCustomerDto, Customer>()
                .ForMember(dest => dest.UserName, opts => opts.MapFrom(src => src.Email))
                .ForMember(dest => dest.AccountInfo,
                    opts => opts.MapFrom(
                        src => new AccountInfo
                        {
                            FirstName = src.FirstName,
                            LastName = src.LastName,
                            IBAN = src.Iban
                        })
                )
                .ForMember(dest => dest.AddressInfo,
                    opts => opts.MapFrom(src => new AddressInfo
                    {
                        Address = src.Address,
                        Zipcode = src.Zipcode
                    })
                )
                .ForMember(dest => dest.Advertisements, opts => opts.MapFrom(src => new List<CustomerAdvertisement>()));

            CreateMap<AdvertisementDto, Advertisement>()
                .ForMember(dest => dest.Title, opts => opts.MapFrom(src => src.Title))
                .ForMember(dest => dest.Image, opts => opts.MapFrom(src => src.Image))
                .ForMember(dest => dest.Compensation, opts => opts.MapFrom(src => src.Compensation))
                .ForMember(dest => dest.Duration, opts => opts.MapFrom(src => src.Duration))
                .ForMember(dest => dest.Start, opts => opts.MapFrom(src => src.Start))
                .ForMember(dest => dest.End, opts => opts.MapFrom(src => src.End))
                .ForMember(dest => dest.CarAdvertisements,
                    opts => opts.MapFrom(src => new Collection<CustomerAdvertisement>()))
                .ForMember(dest => dest.History, opts => opts.MapFrom(src => new Collection<HistoryAction>()))
                .ForMember(dest => dest.CarTypeAdvertisements, opts => opts.MapFrom(src => new Collection<CarTypeAdvertisement>()));

            CreateMap<Advertisement, AdvertisementViewModel>()
                .ForMember(dest => dest.CarTypes,
                    opts => opts.MapFrom(src => src.CarTypeAdvertisements.Select(cta => cta.CarType)));

            CreateMap<Advertisement, AdvertisementDetailViewModel>()
                .IncludeBase<Advertisement, AdvertisementViewModel>()
                .ForMember(dest => dest.SubscribedCars, opts => opts.MapFrom(src => src.CarAdvertisements.Count));
        }
    }
}