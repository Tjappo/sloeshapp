﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore;
using Microsoft.Extensions.Logging;

namespace sloesh_api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateBuildWebHostBuilder(args)
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();
                    logging.AddDebug();
                })
                .Build()
                .Run();
        }

        public static IWebHostBuilder CreateBuildWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
//                .UseKestrel(options => options.Listen(IPAddress.Any, 443, listenOptions =>
//                    listenOptions.UseHttps("certificate.pfx")))
                .UseUrls("http://*:8000/")
                .ConfigureKestrel((context, options) =>
                {
                    // Set properties and call methods on options
                });
    }
}