﻿using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using sloesh_api.Auth;
using sloesh_api.Database;
using sloesh_api.Extensions;
using sloesh_api.Helpers;
using sloesh_api.Models;
using sloesh_api.Models.User;
using sloesh_api.Services;
using sloesh_api.Services.Interfaces;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace sloesh_api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            var appSettings = appSettingsSection.Get<AppSettings>();

            // ===== Add our DbContext ========
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(
                    Configuration.GetConnectionString("DefaultConnection")));


            // ===== Create tables ======
//      dbContext.Database.EnsureCreated();

            // ===== Register Jwt Factory =======
            services.AddSingleton<IJwtFactory, JwtFactory>();

            // ===== Register Http Context Accessor =======
            services.TryAddTransient<IHttpContextAccessor, HttpContextAccessor>();

            // ===== Configure Jwt Issuer Options ========
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(appSettings.Secret));
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = appSettings.JwtIssuer;
                options.Audience = appSettings.JwtAudience;
                options.SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            });

            // ===== Configure Token validation parameters ========
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = appSettings.JwtIssuer,

                ValidateAudience = true,
                ValidAudience = appSettings.JwtAudience,

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = key,

                RequireExpirationTime = false,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };

            // ===== Add Authentication ========
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(configureOptions =>
            {
                configureOptions.ClaimsIssuer = appSettings.JwtIssuer;
                configureOptions.TokenValidationParameters = tokenValidationParameters;
                configureOptions.SaveToken = true;
                configureOptions.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() != typeof(SecurityTokenExpiredException))
                            return Task.CompletedTask;
                        
                        context.Response.Headers.Add("Access-Control-Expose-Headers", "Token-Expired");
                        context.Response.Headers.Add("Token-Expired", "true");

                        return Task.CompletedTask;
                    }
                };
            });

            // ===== Add Api User Claim Policy ========
            services.AddAuthorization(options =>
            {
                options.AddPolicy(Constants.Strings.JwtPolicies.UnvalidatedUser,
                    policy => policy.RequireRole(
                        Constants.Strings.JwtClaims.UnvalidatedAccess,
                        Constants.Strings.JwtClaims.ApiAccess,
                        Constants.Strings.JwtClaims.AdminAccess));
                options.AddPolicy(Constants.Strings.JwtPolicies.ApiUser,
                    policy => policy.RequireRole(
                        Constants.Strings.JwtClaims.ApiAccess,
                        Constants.Strings.JwtClaims.AdminAccess));
                options.AddPolicy(Constants.Strings.JwtPolicies.AdminUser,
                    policy => policy.RequireRole(
                        Constants.Strings.JwtClaims.AdminAccess));
            });

            // ===== Add Identity ========
            var builder = services.AddIdentityCore<User>(options =>
            {
                // ===== Configure Identity Options ========
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequiredLength = 6;
                options.SignIn.RequireConfirmedEmail = true;
                options.Lockout.DefaultLockoutTimeSpan =
                    TimeSpan.FromDays(365 * Constants.Numbers.LockOut.LockoutYears); // indefinitely
                options.Lockout.MaxFailedAccessAttempts = 3;
                options.Lockout.AllowedForNewUsers = true;
            });
            builder = new IdentityBuilder(builder.UserType, typeof(IdentityRole), builder.Services);
            builder.AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();

            // ===== Initialise Dependencies ========
            services.AddAutoMapper();
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                })
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());

            // ===== Initialise mail sender ========
            var sendGridConfigurationSection = Configuration.GetSection("SendGridSettings");
            services.AddSingleton<IEmailSenderService, EmailSenderService>();
            services.Configure<AuthMessageSenderOptions>(sendGridConfigurationSection);

            // ===== Initialise Server urls ========
            var serverUrls = Configuration.GetSection("ServerUrls");
            services.Configure<ServerUrlOptions>(serverUrls);

            // ===== Configure services ======
            // configure DI for application services
            services.AddScoped<IAdvertisementService, AdvertisementService>();
            services.AddScoped<IAdminFetchService, AdminFetchService>();
            services.AddScoped<IAdminUpdateService, AdminUpdateService>();
            services.AddScoped<ICustomerUpdateService, CustomerUpdateService>();
            services.AddScoped<ICustomerFetchService, CustomerFetchService>();
            services.AddScoped<IEmailSenderService, EmailSenderService>();
            services.AddScoped<ICarTypeService, CarTypeService>();
            services.AddScoped<ICarService, CarService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(
                    builder =>
                    {
                        builder.Run(
                            async context =>
                            {
                                context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                                context.Response.Headers.Add("Access-Control-Allow-Origin", "*");

                                var error = context.Features.Get<IExceptionHandlerFeature>();
                                if (error != null)
                                {
                                    context.Response.AddApplicationError(error.Error.Message);
                                    await context.Response.WriteAsync(error.Error.Message).ConfigureAwait(false);
                                }
                            });
                    });
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}