# To Do list

List of features that still have to be implemented

---

# Backend

- **General**
  - Use postal code and license to fetch advertisements
  - Car types
  - Subscribing to an advertisement, also requires:
    - start date
    - address
- **Improve identity**
  - Resend confirmation email (for registration)
- **Bugs**

---

# Frontend

- **General**
    - Change email by customer

- **Views**
