﻿using Microsoft.AspNetCore.Http;

namespace sloesh_api.Extensions
{
    public static class ResponseExtensions
    {
        public static void AddApplicationError(this HttpResponse response, string message)
        {
            response.Headers.Add("Application-Error", message);
            // CORS
            response.Headers.Add("Access-Control-Expose-Headers", "Application-Error, Token-Expired");
        }
    }
}